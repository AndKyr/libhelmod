
include femocs/share/makefile.femocs
F90=gfortran

CFLAGS= -ffree-line-length-none -O3 -Ifemocs/GETELEC/mod -Imod -Ifemocs/lib/ -Jmod \
		  -Warray-bounds -fcheck=all

LIBNAME=libhelmod.a
TEMPLIB=libtemp.a
LIBGETELEC = femocs/GETELEC/lib/libgetelec.a
LIBSLATEC = femocs/GETELEC/lib/libslatec.a
LIBEMISSION = libemission.a
LIBFEMOCS = femocs/lib/libfemocs.a
MODFEMOCS = femocs/lib/libfemocs.mod

LDFLAGS= $(patsubst -L%, -Lfemocs/%, $(FEMOCS_LIBPATH)) $(FEMOCS_LIB) 

AR=ar rcs
LINKLIBS = ar -rcT

OBJECTS = obj/random.o obj/evaporation.o obj/multigrid.o obj/qforces.o \
	obj/bspline.o obj/heating.o obj/helmod.o


all: $(LIBNAME)

release: calc_temperature.exe
	
$(TEMPLIB): $(OBJECTS)
	$(AR) $(TEMPLIB) $(OBJECTS)

$(LIBEMISSION): $(LIBGETELEC) $(LIBSLATEC)	 
	$(LINKLIBS) $(LIBEMISSION) $(LIBGETELEC) $(LIBSLATEC)	 
	
$(LIBNAME): $(TEMPLIB) $(LIBEMISSION) $(LIBFEMOCS)
	$(LINKLIBS) $(LIBNAME) $(TEMPLIB) $(LIBEMISSION)
	
%.exe: obj/%.o $(LIBNAME) $(LIBFEMOCS)
	$(F90) -o $@ $< -L. $(LDFLAGS) -lhelmod 

obj/%.o: src/%.f90 $(MODFEMOCS)
	@mkdir -p obj mod
	$(F90) -c -o $@ $< $(CFLAGS)
	
GETELEC/modules/obj/%.o : femocs/GETELEC/modules/%.f90
	$(F90) $(CFLAGS) -JGETELEC/mod -c $< -o $@

clean:
	rm -f *.a obj/* mod/*.mod *.o 
    
