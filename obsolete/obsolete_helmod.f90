subroutine current_effects(Efield, x1, grid_spacing, atoms, xnp, atype, xq)
    
    !> @brief Adjust atom velocities and force to account for resistive heating and electromigration
    !>
    !> This subroutine calculates the expected temperature distribution in a field emitter by solving the heat equation analytically.
    !> See the master's thesis of S. Parviainen for the most details. Some details also in Parviainen 2011
    !> (http://dx.doi.org/10.1016/j.commatsci.2011.02.010)
    !> Additionally an extra electron wind force in the z-direction can be applied to surface atoms to simulate electromigration. 
    !> When testing electromigration effects you can use @p elmigrat > 1 to see what kind of effects that has.
    !> @p atype is just used to determine which atoms are fixed (and should, therefore, not be included in temperature calculations)
    !> A negative value means fixed.
    !>
    !> @todo Split into smaller parts
    !> @todo Should not use hard-coded values for Cu
    
    use jouleheat, only: solve_heat_equation
    implicit none
    real(dp), intent(in) :: Efield(:, :, :, :) !< The electric field (V/Å)
    integer, intent(in) :: atoms(:, :, 1:) !< Atoms placed on a grid
    integer, intent(in) :: atype(:) !< Atom types
    real(dp), intent(in) :: xq(:) !< Charge on atoms (e) and forces (eV/Å)
    real(dp), intent(in) :: grid_spacing(3) !< Grid spacing (Å)

    real(dp), intent(inout) :: xnp(:) !< Force acting on atoms. Out: force adjusted for electromigration (parcas units)
    real(dp), intent(inout) :: x1(:) !< Atom velocities [vx1, vy1, vz1, vx2, ...]. Out: adjusted for temperature (parcas units)

    integer :: tipheight ! height of protrusion (gridpoints)
    integer :: colcount ! Number of columns in the protrusion
    real(dp), pointer :: tipcols(:, :) ! First index column number
                                        ! Second index: 1 = x, 2 = y
                                        ! 3 = height, 4 = current density
    integer :: tipd(500) ! Protrusion diameter (in gridpoints) by beight
    integer :: natintip ! Atoms in protrusion
    real(dp) :: temp(1:500), temp2(1:500), J_avg(1:500)
    real(dp) :: Tat_max, T_tip, T_Tip2, T_verytip, T_verytip2
    real(dp) :: I_tot ! Total field emission current
    integer, parameter :: solvesteps = 50000000 ! Number of steps when solving heat equation
    real(dp) :: T_boundary ! Boundary temperature for protrusion (substrate temperature)
    real(dp) :: delta ! Helper for scaling velocities

    integer :: k ! loop index

    real(dp), parameter :: tau = 10000 ! Time constant for heat equation temperature control
    real(dp), parameter :: Z_eff = 42d0 ! Effective charge for electromigration

    integer :: myatoms

    myatoms = size(x1)/3

    tipheight = size(atoms, 3) ! A guess for the height (might not be true due to evaporated
              ! atoms since highest_atom_height_above_surf was calculated

    print *, "DBG: tipheight in current_effects",tipheight
    colcount = tipcolcount(Efield(:, :, :, 4), tipheight) ! Count columns in tip

    if (pars%debug) print *, "Columns in tip: ", colcount

    if (colcount /= 0) then
      allocate(tipcols(colcount, 4)) ! Allocate memory for columns
      call tip(Efield(:, :, :, 4), tipcols, colcount, tipd, tipheight)

      if (pars%debug) print *, "Tip height: ", tipheight*grid_spacing(3), tipheight

      delta = globs%deltat_fs/globs%timeunit(1)

      ! Calculate temperature of tip
      call tiptemp(tipcols, x1, atoms, delta, natintip, T_tip, Tat_max)

      if (pars%debug) print *, "T_tip=", T_tip, "(", natintip, "atoms)"

      ! Calculate heat distribution and current densities
      call get_temperature_distribution(temp(1:tipheight), atoms, x1, delta)

      temp2 = temp

      ! A better distribution might be obtained by first smoothing the data a bit
      temp(1) = 0.5*temp2(1) + 0.5*temp2(2)
      temp(tipheight) = 0.5*temp2(tipheight) + 0.5*temp2(tipheight - 1)
      do k = 2, tipheight - 1
        temp(k) = 0.25*temp2(k - 1) + 0.5*temp2(k) + 0.25*temp2(k + 1)
      end do

      ! Calculate current density in each column
      call current_densities(Efield(:, :, :, 4), tipcols, T_tip)

      I_tot = sum(tipcols(:, 4))*grid_spacing(1)*grid_spacing(2) ! A
      print *, "I_tot = ", I_tot, "A"

      if (pars%debug) print *, "tipd top", tipd(tipheight - 2:tipheight)
      T_verytip = temp(tipheight)
      J_avg = I_tot/(tipd*grid_spacing(1)*grid_spacing(2))*1d20 ! A/m^2
      J_avg(tipheight) = (J_avg(tipheight) + J_avg(tipheight - 1))/2 ! Remove effect of "one atom sticking out"
        print *, "J_avg top", J_avg(tipheight - 2:tipheight) * 1.d-18
      if (pars%debug) print *, "J_avg max", maxval(J_avg(:tipheight))
      if (pars%debug) print *, "J_avg avg", sum(J_avg(:tipheight))/tipheight
      
      print *, 'heattop', 17e-9*pars%finite_size_effect * J_avg(tipheight)**2 * 1.d-27

      temp2 = temp

      T_boundary = 300d0 ! Default boundary temperature. Is changed to actual temperature later.

      ! Calculate temperature of atoms below surface to get boundary temperature
      T_boundary = calculate_avg_temperature(atoms, x1, delta, atype)
      if(T_boundary < 0) T_boundary = 300d0 ! Default temperature

      if(pars%debug) print *, "Boundary temperature is ", T_boundary

      call solve_heat_equation(temp(1:tipheight), J_avg(1:tipheight), T_boundary, &
        grid_spacing(3)*1d-10, 1d-15, pars%finite_size_effect, solvesteps)

      print *, "Final tempeature: ", temp(tipheight), temp(tipheight/2), temp(1)

      T_verytip2 = temp(tipheight)

      call scale_velocities(atoms, x1, temp(1:tipheight), tau, delta, J_avg, Z_eff, xnp,&
        xq)
    end if ! colcount /= 0

    call tiptemp(tipcols, x1, atoms, delta, natintip, T_tip2, Tat_max)
    if (pars%debug) print *, "New T_tip = ", T_tip2, "(", natintip, "atoms)"

    if(.false.) then
      print *, "Temperature got too high", T_tip2
      stop
    end if

    deallocate(tipcols)
end subroutine current_effects

subroutine scale_velocities(atoms, x1, temp, tau, delta, J_avg, Z_eff, xnp, xq)

    !> @brief Scale valocities according to the given temperature distribution and 
    !> apply electromigration force
    !>
    !> Electromigration relies on resititity returned by resistivity(). 
    !> This in turn takes values from rho_table.h, so be sure to have
    !> the correct values for materials.
    !>
    !> @todo Consider how to split electromigration out of this subroutine.
    !>  Problem: would require looping over atoms twice!

    use jouleheat, only: resistivity

    integer, intent(in)     :: atoms(:, :, :) !< Atoms on grid
    real(dp), intent(inout) :: x1(:) !< Atom velocities (parcas units)
    real(dp), intent(in)    :: temp(:) 
    !< Temperature distribution as function of height (K)
    real(dp), intent(in)    :: tau !< Time constant for temperature control (fs)
    real(dp), intent(in)    :: delta !< Timestep (parcas units)
    real(dp), intent(in)    :: J_avg(:) !< Current density as a function of height (A/Å^2)
    real(dp), intent(in)    :: Z_eff !< Effective charge (e)
    real(dp), intent(inout) :: xnp(:) !< Forces acting on atoms (parcas units)
    real(dp), intent(in)    :: xq(:) !< Charges on atoms (e) and forces (eV/Å)

    real(dp) :: Ek, Ek2, E_internal, F_elmig, lambdas(size(temp))

    integer :: i, j, k, nelmigrated, m4, m


    real(dp) :: elmig_avg ! Average force due to electromigration
    real(dp) :: elmig_max ! Maximum  force due to electromigration

    real(dp), parameter :: kBeV = 8.61734215d-5

    real(dp)    :: sumlambdas ! sum of lambdas to print the average over tip
    integer     :: ilambdas ! counter of lambdas calcated for tip

    if(size(temp) /= size(atoms,3)) then
      print *, "size(temp) /= size(atoms,3)", size(temp), size(atoms, 3)
      stop
    end if

    nelmigrated = 0
    elmig_avg = 0
    elmig_max = 0
    ! Set new atom velocities

    sumlambdas = 0.d0
    ilambdas = 0

    ! todo Check order of loop indices
    do i = 1, size(atoms, 1); do j = 1, size(atoms, 2)
    do k = 3, size(temp) ! Also applies to top surface layer. Is that correct?
      m = atoms(i, j, k)
      if (m>0) then
        m4 = (m - 1)*4
        m = (m - 1)*3
        Ek = (x1(m + 1)**2*globs%box(1)**2 + &
          x1(m + 2)**2*globs%box(2)**2 + &
          x1(m + 3)**2*globs%box(3)**2)/(delta**2)
        Ek = 0.5*Ek
        Ek2 = 1.5d0 * kBeV * temp(k) ! Energy corresponding to temperature
        lambdas(k) = sqrt(1.d0 + globs%deltat_fs * (Ek / Ek2 - 1.d0) / tau )    
        if(Ek>0) then
          ! Set new velocity
          x1(m + 1) = x1(m + 1)*lambdas(k)
          x1(m + 2) = x1(m + 2)*lambdas(k)
          x1(m + 3) = x1(m + 3)*lambdas(k)
        end if
        sumlambdas = sumlambdas + lambdas(k)
        ilambdas = ilambdas + 1

        ! Electromigration
        if(pars%elmigrat> 0) then   
          if(abs(xq(m4 + 1)) > 0d0) then !Only apply to surface atoms (all charged atoms)
            E_internal = J_avg(k) * resistivity(temp(k)) * pars%finite_size_effect
            F_elmig = E_internal*Z_eff*pars%elmigrat/1d10 ! In ev/Å
            elmig_max = max(elmig_max, F_elmig)
            elmig_avg = elmig_avg + F_elmig
            xnp(m + 3) = xnp(m + 3) + F_elmig/globs%box(3)
            nelmigrated = nelmigrated + 1
          end if
        end if
      end if
    end do
    end do; end do

    print '(A15,ES13.5)', '<lambas> =', sumlambdas / ilambdas

    elmig_avg = elmig_avg / nelmigrated

    if(pars%debug) print *, "Applied elmigration to ", nelmigrated, "atoms (out of ", size(x1)/3, "total) average and max force ", &
      elmig_avg, elmig_max, "(eV/Å)"

end subroutine scale_velocities






pure real(dp) function calculate_avg_temperature(atoms, x1, delta, atype)

    !> @brief Calculate the average temperature of atoms in a part of the grid
    !>
    !> Fixed atoms are not included in this caluclation

    integer, intent(in) :: atoms(:, :, 1:) !< Atoms on the grid
    real(dp), intent(in) :: x1(:) !< Atom velocities (parcas units)
    integer, intent(in) :: atype(:) !< Atom types
    real(dp), intent(in) :: delta !< Timestep (parcas units)

    real(dp) :: Ek
    integer :: n, m
    integer :: i, j, k

    real(dp), parameter :: kBeV = 8.61734215d-5

    Ek = 0
    n = 0
    do i = 1, size(atoms, 1); do j = 1 , size(atoms, 2)
    do k = 1 , size(atoms, 3)
      m = atoms(i, j, k)
      if(m > 0) then
        if(atype(m) < 0) cycle ! Ignore fixed atoms
        n= n + 1
        m = (m - 1)*3
        ! Kinetic energy is missing a factor 0.5, but it's compensated when 
        !converting to temperature
        Ek = Ek + (x1(m + 1)**2*globs%box(1)**2 + &
          x1(m + 2)**2*globs%box(2)**2 + &
          x1(m + 3)**2*globs%box(3)**2)/(delta**2)
      end if
    end do
    end do; end do
    if(Ek > 0) then 
      calculate_avg_temperature = Ek/(3.0*n*kBeV)
    else
      calculate_avg_temperature = -1d0
    end if

end function

subroutine tiptemp(tipcols, x1, atoms, delta, natintip, T_tip, Tat_max)
    !> @brief Calculate temperature of the tip
    real(dp), intent(in) :: tipcols(:, :) !< Coordinates and current of columns in tip
    real(dp), intent(in) :: x1(:) !< Atomic velocities (parcas internal units)
    integer, intent(in) :: atoms(:, :, 1:) !< Atoms on the grid
    real(dp), intent(in) :: delta !< Timestep in MD units

    integer, intent(out) :: natintip !< Total number of atoms in the tip
    real(dp), intent(out) :: T_tip !< Average temperature of the tip
    real(dp), intent(out) :: Tat_max !< "Temperature" of atom with highest energy

    real(dp) :: T

    real(dp) :: Ek_tot, Ek
    integer :: x, y, h, i, j, k

    real(dp), parameter :: kBeV = 8.61734215d-5

    Ek_tot = 0
    natintip = 0
    Tat_max = 0

    do i = 1, size(tipcols, 1)
      x = int(tipcols(i, 1))
      y = int(tipcols(i, 2))
      h = int(tipcols(i, 3))
      do k = 1, h
        j = atoms(x, y, k)
        if(j>0) then
          j = (j - 1)*3

          Ek = ((x1(j + 1)*globs%box(1))**2 + &
               (x1(j + 2)*globs%box(2))**2 + &
               (x1(j + 3)*globs%box(3))**2)/delta**2
          T = Ek/(3.0*kBeV)
          if (T>Tat_max) Tat_max = T
          Ek_tot = Ek_tot + Ek
          natintip = natintip + 1
        end if
      end do
    end do
    T_tip = Ek_tot/(3.0*natintip*kBeV) ! Missing a factor 2, but it's OK, because
                                       ! Ek_tot is missing a factor 0.5
end subroutine




!> @brief Calculate current densities for all columns
subroutine current_densities(Efield, tipcols, T)
    use emission, only: gtf, fowler_nordheim2
    implicit none
    real(dp), intent(in) :: Efield(:, :, :) !< Electric field (V/Å)
    real(dp), intent(in) :: T !< Temperature of tip of tip (K)
    real(dp), intent(inout) :: tipcols(:, :) !< Coordinate and current of columns in tip

    integer :: h
    integer :: i, x, y, dx, dy, z
    real(dp) :: Eij, Eij_max
    real(dp) :: J,J_max

    integer, parameter :: emission_model = EMISSION_GTF

    !call print_phi(phi_glob,grid_glob)
    print *, 'calling current density'

    Eij_max = 0
    J_max = 0
    do i = 1, size(tipcols, 1)
      x = tipcols(i, 1)
      y = tipcols(i, 2)
      h = tipcols(i, 3)

      ! Include contribution from sides of protrusion
      ! This gives a better agreement with FEM simulations which properly accounts for 
      !current "leaking" out from the sides.
      ! BUG: emission_model is not used here
      J = 0
      ! Loop over all surrounding columns
      do dx = -1, 1
      do dy = -1, 1
        if(dx == 0 .and. dy == 0) cycle ! Don't include the actual column, only those around it
        do z = 1, h
          J = J + gtf(Efield(x + dx, y + dy, z)*1d10, T)
        end do
      end do
      end do

      Eij = Efield(x, y, h + 1)*1d10 ! V/Å -> V/m
      Eij_max = max(Eij_max,Eij)
      ! BUG: Should use temp at apex?
      if(emission_model == EMISSION_FN) then
        J = J + fowler_nordheim2(Eij, T)
      elseif(emission_model == EMISSION_GTF) then
        J = J + gtf(Eij, T)
        if(J<0) then
                print *, "Got negative current. Try with a lower field.", J, Eij, T
                stop
        end if
      else
        print *, "Unknown emission model. Should be 1 for FN, 2 for GTF"
      end if

      J_max = max(J_max,J)
      tipcols(i, 4) = J/1d20 ! Convert to A/Å**2

      if(pars%debug) then
      if (x == size(Efield, 1)/2 .and. y == size(Efield, 2)/2) then
              print *, "J_middle", J, h
      end if
      end if

    end do
end subroutine current_densities





!> @brief Get positions of all the columns in the tip
!>
!> Determines the position and height of all the columns in the protrusion in the system
subroutine tip(Efield, tipcols, colcount, tipd, tipheight)
    implicit none
    integer, intent(in) :: colcount !< Number of columns in tip
    real(dp), intent(in) :: Efield(:, :, :) !< Electric field (V/Å)
    integer, intent(in) :: tipheight !< Height of tip (gridpoints)

    real(dp), intent(out) :: tipcols(colcount, 4) !< Array with coordinate and (not yet calculated) current of columns
    integer, intent(out) :: tipd(:) !< Width of tip as a function of height from the surface (gridpoints)

    integer :: i, j, k, ti
    integer, parameter :: threshold = 5
    integer :: ncols

    tipcols = -1
    tipd = 0


    if(size(tipd) < tipheight) then
      print *, "Increase size of tipd"
      stop
    end if

    ! Find width of tip
    do j = 1, size(Efield, 2)
    do i = 1, size(Efield, 1)
    do k = tipheight + 5, 1, -1
      if(abs(Efield(i, j, k)) < 1d-30) then
        tipd(k) = tipd(k) + 1
      end if
    end do
    end do
    end do

    ! Set tip heights
    ncols = 0
    ti = 1
    do j = 1, size(Efield, 2)
    do i = 1, size(Efield, 1)
    ! TODO: Check that there is at least 10 points above! BUG!
    do k = tipheight + 10, 1, -1
      if(abs(Efield(i, j, k)) < 1d-30) then
        if(k > threshold) then
          ncols = ncols + 1
          tipcols(ti, 1) = i
          tipcols(ti, 2) = j
          tipcols(ti, 3) = k
          if(tipcols(ti, 3) > tipheight) then
            print *, "tipcol higher than tip", k, tipheight ! Shouldn't happen
            stop
          end if
          ti = ti + 1
        end if
        exit
      end if
    end do
    end do
    end do

    !if (pars%debug) print *, "tipd top real", tipd(tipheight-3:tipheight + 1)
    ! Smoothen tip diameter
    tipd(2:tipheight) = nint((tipd(1:tipheight - 1) + tipd(2:tipheight))/2.0)

    !tipheight = tipheight - 1

    if (ncols /= size(tipcols, 1)) then ! Shouldn't ever happen
      print *, "ncols /= colcount", ncols, size(tipcols, 1)
      stop
    end if
end subroutine tip



    ! This is actually quite expensive just to try to estimate the total charge, and it's not used for anything, so let's skip it
    ! In principle it could probably be removed completely from the code
    if(.false.) then
    do ia = 1, myatoms
      i4 = ia*4 - 4
      Q_sum = Q_sum + xq(i4 + 1)

      if(abs(xq(i4 + 1)) > 0.0d0) N_at = N_at + 1;

      ! Calculate forces on atoms
      xq1 = xq(i4 + 1) * E_n(i4 + 1)
      xq2 = xq(i4 + 1) * E_n(i4 + 2)
      xq3 = xq(i4 + 1) * E_n(i4 + 3) 

      E_n(i4 + 4) = sqrt( E_n(i4 + 1) * E_n(i4 + 1) + E_n(i4 + 2) * E_n(i4 + 2) + E_n(i4 + 3) * E_n(i4 + 3) )

      if(abs(xq(i4 + 1)) < abs(qmn) .and. abs(xq(i4 + 1)) > 0.0d0) then
        qmn = xq(i4 + 1)
        at_qmn = ia
      endif

      if(abs(xq(i4 + 1)) > abs(qmx)) then
        qmx = xq(i4 + 1)
        at_qmx = ia
      endif
    enddo

    Q_tot_real = 0d0
    do k = 1, size(philimit, 3)
    do j = 1, size(philimit, 2)
    do i = 1, size(philimit, 1)
      if(philimit(i, j, k) /= FREE_SPACE) cycle
      iq0(1) = i; iq0(2) = j; iq0(3) = k
      do i_c = 1, 3
        i_c1 = i_c + 1; if(i_c1 > 3) i_c1 = i_c1 - 3
        i_c2 = i_c + 2; if(i_c2 > 3) i_c2 = i_c2 - 3
        iq1(1) =i; iq1(2) = j; iq1(3)= k
        iq1(i_c) = iq0(i_c) + 1

        iq2(1) = i; iq2(2) = j; iq2(3) = k
        iq2(i_c) = iq0(i_c) - 1
        if(pbc(i_c) == 1 .and. iq1(i_c) < 1) iq1(i_c) = iq1(i_c) + size(philimit, i_c)
        if(pbc(i_c) == 1 .and. iq1(i_c)  > size(philimit, i_c)) iq1(i_c) = iq1(i_c) - size(philimit, i_c)

        if(pbc(i_c) == 1 .and. iq2(i_c) < 1) iq2(i_c) = iq2(i_c) + size(philimit, i_c)
        if(pbc(i_c) == 1 .and. iq2(i_c)  > size(philimit, i_c)) iq2(i_c) = iq2(i_c) - size(philimit, i_c)
        if(iq1(i_c) > size(philimit, 3)) cycle
        if(iq2(i_c) > size(philimit, 3)) cycle

        if(philimit(iq1(1), iq1(2), iq1(3)) == DIRICHLET_BOUNDARY &
                .or. philimit(iq2(1), iq2(2), iq2(3)) == DIRICHLET_BOUNDARY) then
          Q_tot_real = Q_tot_real + Efield(i, j, k, i_c)*i0(i_c)*eps0*grid_spacing(i_c1)*grid_spacing(i_c2)
        endif
      enddo


      add_3 = 0; i_ad2 =0;i_ad3 = 0; j_ad2= 0; j_ad3 = 0; k_ad2 = 0; k_ad3 = 0;
      add_2(1) = 0; add_2(2) = 0; add_2(3) = 0;
      do ii = -1, 1; do jj = -1, 1; do kk = -1, 1
        if(abs(ii) + abs(jj) + abs(kk) == 0) cycle  ! there is no atom in the GP with the field
        i_ii = i + ii;     j_jj = j + jj;       k_kk = k + kk

        if(pbc(1) == 1 .and. i_ii < 1) i_ii = i_ii + size(philimit, 1)
        if(pbc(1) == 1 .and. i_ii > size(philimit, 1)) i_ii = i_ii - size(philimit, 1)

        if(pbc(2) == 1 .and. j_jj < 1) j_jj = j_jj + size(philimit, 2)
        if(pbc(2) == 1 .and. j_jj > size(philimit, 2)) j_jj = j_jj - size(philimit, 2)     


        if(k_kk < 1 .or. k_kk > size(philimit, 3)) cycle
        if(abs(ii) + abs(jj) + abs(kk) == 2 .and. philimit(i_ii, j_jj, k_kk) == DIRICHLET_BOUNDARY) then
          if(ii == 0) add_2(1) = add_2(1) + 1
          if(jj == 0) add_2(2) = add_2(2) + 1
          if(kk == 0) add_2(3) = add_2(3) + 1
          i_ad2 = ii; j_ad2 = jj; k_ad2 = kk;
        endif

        if(abs(ii) + abs(jj) + abs(kk) == 3 .and. philimit(i_ii, j_jj, k_kk) == DIRICHLET_BOUNDARY) then
          add_3 = add_3 + 1
          i_ad3 = ii; j_ad3 = jj; k_ad3 = kk;
        endif

      enddo; enddo; enddo

      if(add_2(1) == 1 .or. add_2(2) == 1 .or. add_2(3) == 1 .or. add_3 == 1) then
        do i_c = 1, 3
          i_c1 = i_c + 1; if(i_c1 > 3) i_c1 = i_c1 - 3
          i_c2 = i_c + 2; if(i_c2 > 3) i_c2 = i_c2 - 3
          Q_tot_real =Q_tot_real + Efield(i, j, k, i_c)*i0(i_c)*eps0*grid_spacing(i_c1)*grid_spacing(i_c2)
        enddo
      endif
    enddo; enddo; enddo;
    if (pars%debug) print *, "Q_tot_real = ", Q_tot_real, "Q_sum =", Q_sum, "N_at =", N_at
    end if






subroutine charge_and_field(els, fem, xq, atype, atoms)
                            
    !> @brief Compute electric field and assign charges to atoms
    !>
    !> This function should be used when you already have atoms assigned to a grid, 
    !> and also have the boundary conditions in theappropriate form. In case you only
    !> have atom coordinates, you should use chage_and_field_from_atoms() instead.
    !>
    !> The solution of the electrostatic potential is remembered, and used as the 
    !> initial  guess for the next call. The value of the previously used applied 
    !> field is also remembered. If it is different thant the current one, the ratio
    !> is used to scale the initial guess.
    !>
    !> @p Efield has the following format: the first three indices indicate the 
    !> coordinate on the grid, and the 4th index has the direction (Ex,Ey,Ez,|E|).
    !> E.g the magnitude of the field at point 3,4,5 (grid coordinates) is given by
    !> Efield(3, 4, 5, 4). The units are V/Å.
    !> 
    !> @p xq has the following format: it is a one dimensional array in a smiliar
    !> style as parcas x0. First element contains total charge in unit charges, 
    !> followed by the Lorentz force (eV/Å). The force components are used in Qforces 
    !> to calculate the total force on atoms.
    
    type(Electrostatics), intent(inout) :: els
    !< Electrostatics data structure to be modified 
    type(femocs), intent(inout)         :: fem  
    !< Finite elements object to be created-modified.                           
    
    real(dp), intent(inout) :: xq(:) !< Charge on atoms (e) and forces (eV/Å)
    integer, intent(in)     :: atype(:) !< Types of atoms
    integer, intent(in)     :: atoms(:, :, :) !< Atom coordinates on a grid

    integer, allocatable, save  :: philimit_sq(:, :, :)  
    ! Boundary conditions on a square grid
    real(dp), allocatable, save :: Efield_sq(:, :, :, :)              
    ! Electrostatic field on a square grid (intermediate variable used only here.)
    
    integer                 :: highest_atom_height_above_surf, k
    integer                 :: smaller_size(3)              
    ! Size of volume to solve when not solving the full system
    integer                 :: smaller_origin(3)
    ! Origin of the volume when not solving the full system
    logical, save           :: firsttime = .true.

    ! Make grid square to avoid numeric effects
    ! This is important for e.g. 110 surfaces, because otherwise different grid
    ! spacings are used in different directions, leading to numerically different
    ! results, even though otherwise the system is symmetric
    
    if (pars%debug) print *, 'Entering charge_and_field'
    
    els%grid_spacing_sq = [max(els%grid_spacing(1), els%grid_spacing(2)), &
                        max(els%grid_spacing(1), els%grid_spacing(2)), &
                        els%grid_spacing(3)] ! NOTE: Only square in xy-plane
                        
    call limgridsq(els%philimit, els%grid_spacing, philimit_sq, els%grid_spacing_sq) 
    ! Get a square grid for philim

    if(firsttime) then
        allocate(els%phi_sq(size(philimit_sq, 1), size(philimit_sq, 2), &
                                                            size(els%Efield, 3)))
        allocate(Efield_sq(size(philimit_sq, 1), size(philimit_sq, 2), &
                                                            size(els%Efield, 3), 4))
        Efield_sq = 0

        if(pars%debug) then
            print *, "phisize: ", shape(els%phi_sq)
            print *, "Setting initial gradient guess for electric field"
        end if

        els%phi_sq(:, :, 1:2) = 0 ! assume the linear increase of phi with the gradient
        do k = 3, size(els%phi_sq, 3)
            els%phi_sq(:, :, k) = - (els%current_applied_field * (k - 2) * &
                                els%grid_spacing_sq(3)) 
            ! assume the linear increase of phi with the gradient
        end do
        firsttime = .false.
    end if

    ! Only need to solve field if surface shape or applied field has changed
    !> @todo Move scale_potential call into solve_field
    call scale_potential(els%phi_sq, els%current_applied_field, els%prev_applied_field)

    call solve_field(els%current_applied_field, &
            philimit_sq(:, :, els%zbounds(1):els%zbounds(1) + globs%iabove - 1), &
            els%phi_sq, Efield_sq, els%grid_spacing_sq, els%full_solve, &
            smaller_size, smaller_origin)

    call egridunsq(Efield_sq, els%grid_spacing_sq, els%Efield, els%grid_spacing) 
    ! Get the electric field back to the original grid

    if(pars%debug) print *, "Mid-point e-field", els%Efield(size(els%Efield, 1)/2, &
                        size(els%Efield, 2)/2, highest_atom_height_above_surf + 1, 4) 
                        ! Bug, not necessarily height at mid point

    ! Calculate the induced surface charge
    !> @todo Make it so that charges only looks at the subvolume 
    !> where anything interesting has happened. Charges elsewhere should be the same.
    call charges(atoms, els%Efield, els%grid_spacing, xq, &
        els%philimit(:, :, els%zbounds(1):els%zbounds(1) + globs%iabove - 1), atype)

end subroutine charge_and_field



function tipcolcount(Efield, tipheight) result(ncols)

    !> @brief Count number of columns in the tip
    !> 
    !> Calculates height based on where the electric field >0, i.e assumes 
    !> E = 0 inside the tip
    real(dp), intent(in) :: Efield(:, :, :) !< Electric field (V/Å)

    integer, intent(inout) :: tipheight 
    !< Height of tip. In: Height below which to look, out: actual height

    integer :: ncols
    integer :: tmp

    integer :: i, j, k
    integer, parameter :: threshold = 5

    if (pars%debug) print *, "DBG: tipheight", tipheight
    tmp = tipheight
    tipheight = -1

    ncols = 0
    do j = 1, size(Efield, 2)
    do i = 1, size(Efield, 1)
    do k = tmp + 10, 1, -1
      if(abs(Efield(i, j, k)) < 1d-30) then
        if(k > threshold) then
          ncols = ncols + 1
          tipheight = max(tipheight, k)
          exit
        end if
      end if
    end do
    end do
    end do

end function tipcolcount





!!> @brief Calculates resistivity
!function resistivity(T) result(rho)
    
!    real(dp), intent(in)    :: T !temperature
!    real(dp)                :: rho
!    integer :: Ti
!    include "rho_table.h"

!    Ti = nint(T)/10

!    if (Ti < 5 .or. Ti > 119) then
!        if (pars%debug > 2) print *, &
!            "WARNING: Temperature out of range in resisitivity calculation. T =", T

!        Ti = min(Ti,119) ! cap up to 119
!        Ti = max(Ti,5)   ! bottom down to 5
!    end if
!    ! Resistivity is an exponential function, so use log to make it linear, 
!    !do normal interpolation, and then make it exponential again
!    rho = exp( (log(rho_table(Ti+1)) - log(rho_table(Ti))) / (log(Ti*10d0+10)&
!                - log(Ti*10d0)) * (log(T) - log(Ti*10d0)) + log(rho_table(Ti)) )

!    rho = rho*10.d0 ! Convert to units Ohm*nm
!end function resistivity


    in setup_boundary_conditions: 
        if(atoms(i_cnt, j_cnt, k_cnt) /= 0) then
            ia2 = atoms(i_cnt, j_cnt, k_cnt)
            if(.false.) then ! Could be useful for debugging
          print *, "Error: multiple atoms at ", i_cnt, j_cnt, k_cnt, atoms(i_cnt, j_cnt, k_cnt), ia
          centre = [(dble(i_cnt)/size(atoms, 1) - 0.5), (dble(j_cnt)/size(atoms, 2) - 0.5), (dble(k_cnt)/size(atoms, 3) - 0.5)]
          print *, "centre", centre
          print *, ia, x0(ia*3 - 2), x0(ia*3 - 1), x0(ia*3)
          print *, ia, x0(ia*3 - 2), x0(ia*3 - 1), x0(ia*3)
          print *, ia, x0(ia*3 - 2) - centre(1), x0(ia*3 - 1) - centre(2), x0(ia*3 - 0) - centre(3)
          print *, ia, (x0(ia*3 - 2) + 0.5)*size(atoms, 1), (x0(ia*3 - 1) + 0.5)*size(atoms, 2), &
                  (x0(ia*3 - 0) + 0.5)*size(atoms, 3)
          print *, ia2, x0(ia2*3 - 2), x0(ia2*3 - 1), x0(ia2*3)
          print *, ia2, x0(ia2*3 - 2), x0(ia2*3 - 1), x0(ia2*3)
          print *, ia2, x0(ia2*3 - 2) - centre(1), x0(ia2*3 - 1) - centre(2), x0(ia2*3 - 0) - centre(3)
          print *, ia2, (x0(ia2*3 - 2) + 0.5)*size(atoms, 1), (x0(ia2*3 - 1) + 0.5)*size(atoms, 2), &
                  (x0(ia2*3 - 0) + 0.5)*size(atoms, 3)
          print *, (x0(ia2*3 - 2) - x0(ia*3 - 2)), (x0(ia2*3 - 1) - x0(ia*3 - 1)), (x0(ia2*3 - 0) - x0(ia*3 - 0))
        end if

    
    
    
    
!> @brief Calculate the charges on individual atoms
subroutine charges(atoms, Efield, grid_spacing, xq, philimit, atype)

    !> Compute the charges on individual atoms based on a modified version of Gauss' 
    !> law. The first column of @p xq received the charge (unit charges) and the 
    !> three following columns get the force in x- y- and z-directions
    
    integer, intent(in)     :: atype(:) !< Atom types

    real(dp), intent(in)    :: Efield(:, :, :, :) !< Electric field (V/Å)
    real(dp), intent(in)    :: grid_spacing(3) !< Grid spacing (Å)
    integer, intent(in)     :: atoms(:, :, 1:) !< Atoms on a grid
    integer, intent(in)     :: philimit(:, :, 1:) !< Boundary conditions for solver
    real(dp), intent(out)   :: xq(:) !< Charge on atoms (e) and force (eV/Å)

    real(dp)    :: frac(4, 3), sum_fr(3)
    integer     :: i, j, k, ii, jj, kk, i_ii, j_jj, k_kk, i4, myatoms, i_A(3), i_E(3)
    integer     :: ic, sum_i0,  i0(3), i_c, i_c1, i_c2
    integer     :: nul(3), neighb(3, 3), neigh(3, 3), neighb_st(3) = [1, 4, 4]
    
    real(dp), allocatable, save :: E_n(:)
    logical, save :: firsttime = .true.
    real(dp), parameter :: eps0 = 5.5268d-03, frac0(3) = [0.5d0, 0.25d0, 0.25d0]  
    ! eps0 = 8.854e-12 C/V/m = 8.854e-22 C/V/Å = 5.5268e-03 e/V/Å

    if (pars%debug > 0) print *, 'Entering charges.'

    myatoms = size(xq)/4

    if(firsttime) allocate(E_n((myatoms + 50)*4))
    firsttime = .false.

    ! Check if the number of atoms has changed and update the allocation of E_n.
    if(size(E_n) < (myatoms + 50)*4) then
        deallocate(E_n)
        allocate(E_n((myatoms + 50)*4))
    endif

    xq(1:4*myatoms) = 0
    E_n(1:4*myatoms) = 0

    ! @todo check the limits of loops here, after im* stuff was removed
    ! Loop once more over all the grid points above the surface
    ! Aim: collect the information of what charge the atom can receive 
    !from the particular GP
    do k = 1, size(philimit, 3)  !loop over all grid points
    do j = 1, size(philimit, 2)
    do i = 1, size(philimit, 1)
        if(philimit(i, j, k) /= FREE_SPACE) cycle ! GPs with no field are not allowed
        neighb = 0
        ! Loop over neighbouring points
        do kk = -1, 1
            k_kk = k + kk
            if(k_kk < 1 .or. k_kk > size(philimit, 3)) cycle
            do jj = -1, 1
                j_jj = j + jj;       
                if(pbc(2) == 1 .and. j_jj < 1) j_jj = j_jj + size(philimit, 2)
                if(pbc(2) == 1 .and. j_jj > size(philimit, 2)) &
                    j_jj = j_jj - size(philimit, 2)
                do ii = -1, 1
                    
                    !A.K. Skip the current point (only neighbors)
                    if(abs(ii) + abs(jj) +  abs(kk) == 0) cycle  
                    
                    ! there is no atom in the GP with the field

                    i_ii = i + ii !AK: the current neighbor point   
                    ! Handle periodic boundaries
                    if(pbc(1) == 1 .and. i_ii < 1) i_ii = i_ii + size(philimit, 1)
                    if(pbc(1) == 1 .and. i_ii > size(philimit, 1)) &
                        i_ii = i_ii - size(philimit, 1)

                    if(atoms(i_ii, j_jj, k_kk) < 1 ) cycle   
                    ! If the GP does not contain an atom
                    if(atype(atoms(i_ii, j_jj, k_kk)) == pars%evpatype) cycle 
                    ! Ignore evaporated atoms

                    i0(1) = -ii; i0(2) = -jj; i0(3) = -kk;
                    i_c = 0
                    do ic = 1, 3
                        i_c = i_c + abs(i0(ic))
                    enddo
                    
                    
                    !AK: Following if clause is added to check the field for Femocs
                    if (all([Efield(i, j ,k, 4)<1.d-30, k > 10, i>1, &
                            i<size(Efield,1), j>1, j<size(Efield,2)])) then
                        print *, 'Warning: charges gets zero field at point', &
                                i, j, k, 'Efield = ', Efield(i,j,k,:)
                        !globs%gridprint = .true.
                    endif
                    
                    do ic = 1, 3
                        if(sign(1d0, Efield(i, j, k, ic)) /= sign(1d0, 1d0*i0(ic))) &
                            neighb(i_c, ic) = neighb(i_c, ic) + 1 
                    enddo
                enddo
            enddo
        enddo

        do ic = 1, 3
            do i_c = 1, 3
                neigh(i_c, ic) = neighb(i_c, ic)
                if(neigh(i_c, ic) > neighb_st(i_c)) neigh(i_c, ic) = neighb_st(i_c)
            enddo
            sum_fr(ic) = 0.0; nul(ic) = 0;
            do i_c =1, 3
                i_c1 = i_c + 1; if(i_c1 > 3) i_c1 = i_c1 - 3
                i_c2 = i_c + 2; if(i_c2 > 3) i_c2 = i_c2 - 3
                frac(i_c, ic) = frac0(i_c)*neigh(i_c, ic)/neighb_st(i_c) +     &
                    frac0(i_c1)*(1 - neigh(i_c1, ic)*1d0/neighb_st(i_c1))*0.5d0 +   &
                    frac0(i_c2)*(1 - neigh(i_c2, ic)*1d0/neighb_st(i_c2))*0.5d0
                          
                if(neigh(i_c, ic) < 1) nul(ic) = nul(ic) + 1
                sum_fr(ic) = sum_fr(ic) + frac(i_c, ic)
            enddo

            select case (nul(ic))
                case (1)
                    do i_c =1, 3
                        i_c1 = i_c + 1; if(i_c1 > 3) i_c1 = i_c1 - 3
                        i_c2 = i_c + 2; if(i_c2 > 3) i_c2 = i_c2 - 3
                        if(neighb(i_c, ic) > 0d0) cycle
                        frac(i_c1, ic) = frac(i_c1, ic) + frac(i_c, ic)*0.5d0
                        frac(i_c2, ic) = frac(i_c2, ic) + frac(i_c, ic)*0.5d0
                        frac(i_c, ic) = 0d0
                    enddo
                case(2)
                    do i_c = 1, 3
                        if(neighb(i_c, ic) > 0d0) then
                            i_c1 = i_c + 1; if(i_c1 > 3) i_c1 = i_c1 - 3
                            i_c2 = i_c + 2; if(i_c2 > 3) i_c2 = i_c2 - 3
                            frac(i_c, ic) = frac(i_c, ic) + frac(i_c1, ic) + &
                                            frac(i_c2, ic)
                            frac(i_c1, ic) = 0d0
                            frac(i_c2, ic) = 0d0
                            exit
                        endif
                    enddo
                case(3)
                    cycle
            end select
        enddo

        do ic = 1, 3
            sum_fr(ic) = frac(1, ic) + frac(2, ic) + frac(3, ic)
            if(sum_fr(ic) > 1d0 .or. sum_fr(ic) < 1d0 - 0.00001) then
                print *, "in direction", ic
                print *, sum_fr(ic), nul(ic), (frac(i_c, ic), i_c = 1, 3), &
                        (neighb(i_c, ic), i_c = 1, 3)
                print *, &
                    "check the fractions, the charge is going to be overestimated!"
                stop
            endif
        enddo

        do kk = -1, 1
            k_kk = k + kk
            do jj = -1, 1
                j_jj = j + jj
                if(pbc(2) == 1 .and. j_jj < 1) j_jj = j_jj + size(philimit, 2)
                if(pbc(2) == 1 .and. j_jj > size(philimit, 2)) &
                    j_jj = j_jj - size(philimit, 2)
                do ii = -1, 1
                    if(abs(ii) + abs(jj) + abs(kk) == 0) cycle  
                    ! there is no atom in the GP with the field
                    i_ii = i + ii
                    if(pbc(1) == 1 .and. i_ii < 1) i_ii = i_ii + size(philimit, 1)
                    if(pbc(1) == 1 .and. i_ii > size(philimit, 1)) &
                        i_ii = i_ii - size(philimit, 1)
                    if(k_kk < 1 .or. k_kk > size(philimit, 3)) cycle
                    if(atoms(i_ii, j_jj, k_kk) < 1) cycle
                    if(atype(atoms(i_ii, j_jj, k_kk)) == pars%evpatype) cycle
                    
                    i4 = atoms(i_ii, j_jj, k_kk) * 4 - 4
                    i0(1) = -ii; i0(2) = -jj; i0(3) = -kk 
                    ! Now the direction is from GP to atom, but must be opposite
                    i_E(1) = i; i_E(2) = j; i_E(3) = k 
                    ! Now the direction is from GP to atom, but must be opposite
                    i_A(1) = i_ii; i_A(2) = j_jj; i_A(3) = k_kk
                    do i_c = 1, 3
                        i_c1 = i_c + 1;  if(i_c1>3) i_c1= i_c1 - 3
                        i_c2 = i_c + 2; if(i_c2>3) i_c2= i_c2 - 3
                        i_A(i_c) = i_E(i_c)
                        if(philimit(i_A(1), i_A(2), i_A(3)) == DIRICHLET_BOUNDARY &
                            .or. i0(i_c) == 0) cycle

                        sum_i0 = 0
                        do ic = 1, 3
                            sum_i0 = sum_i0 + abs(i0(ic))
                        enddo
                        if(neighb(sum_i0, i_c) == 0) cycle
                    
                        E_n(i4 + i_c) = E_n(i4 + i_c) + &
                            Efield(i, j, k, i_c) * &
                            frac(sum_i0, i_c)/neighb(sum_i0, i_c)

                        ! Calculate induced charge
                        xq(i4 + 1) = xq(i4 + 1) + Efield(i, j, k, i_c) * i0(i_c) * &
                            frac(sum_i0, i_c)/neighb(sum_i0, i_c) *   &
                            eps0 * grid_spacing(i_c1) * grid_spacing(i_c2)
                                
                        ! Calculate the force on atoms
                        xq(i4 + 1 + i_c) = xq(i4 + 1 + i_c) + Efield(i, j, k, i_c) *&
                            Efield(i, j, k, i_c) * i0(i_c) * frac(sum_i0, i_c) / &
                            neighb(sum_i0, i_c)*eps0*grid_spacing(i_c1) * &
                            grid_spacing(i_c2)
                    enddo
                enddo
            enddo
        enddo
    enddo; enddo; enddo; 
    
    if (pars%debug > 0) print *, 'exiting charges'

end subroutine charges



!> @brief Get the temperature distribution in a tip
subroutine get_temperature_distribution(temperature, atoms, x1, delta)

    !> Returns the temperature distribution along the z-axis
    
    real(dp), intent(out)   :: temperature(:) !< Temperature distribution
    integer, intent(in)     :: atoms(:,:, 1:) !< Atoms on a grid
    real(dp), intent(in)    :: x1(:) !< Atom velocities (parcas units)
    real(dp), intent(in)    :: delta !< Timestep (parcas units)

    real(dp)    :: tsum, Ek
    integer     :: n, m, i, j, k

    real(dp), parameter :: kBeV = 8.61734215d-5

    do k = 1, size(atoms, 3)
        ! Calculate avarage temperature at height k
        tsum = 0
        n = 0
        do j = 1, size(atoms, 2)
        do i = 1, size(atoms, 1)
            m = atoms(i, j, k)
            if (m > 0) then
                m = (m - 1) * 3
                Ek = .5d0 * (x1(m + 1)**2 * globs%box(1)**2 + &
                        x1(m + 2)**2 * globs%box(2)**2 + &
                        x1(m + 3)**2 * globs%box(3)**2) / (delta**2)
                tsum = tsum + 2.0/(3.0*kBeV) * Ek
                n = n + 1
            end if
        end do
        end do
        if(n > 1) then 
        ! If there are very few atoms in a layer then the temperature might vary a
        ! lot due to random motion
            temperature(k) = tsum/n
        else
            temperature(k) = temperature(k - 1)
        end if
    end do
end subroutine


!> @brief Solves the heat equation in one dimension.
subroutine heateq(heat)

    type(HeatData), intent(inout)   :: heat

    real(dp), parameter     :: Cv = 3.45d-6, L = 2.44d-8 
    ! Cv in(W*fs/(nm^3 K)), L=Wiedemann-Frantz constant in W*Ohm/K^2
    integer, parameter      :: maxsteps = 10**7 
    !< Maximum number of time steps to solve heat equation
    real(dp), parameter     :: convcr = 1.d-3
    !< Convergence criterion for the heat equation reaching steady state
    
    real(dp)                :: K, K1, rho 
    !K: Thermal conductivity, W/(nm K)
    real(dp), dimension(heat%tipbounds(2)-heat%tipbounds(0) + 1) :: &
                                                a, b, c, d, Told, Tnew, radii
    !tridiagonal vectors and temporary temperature working space
    integer :: i, j, solvesteps, Nx
    logical, save :: firsttime = .true.
    
    if (pars%debug > 0) print *, 'Entering heateq'
    
    Nx = heat%tipbounds(2)-heat%tipbounds(0) + 1

    solvesteps = min(maxsteps, floor(heat%maxtime / heat%dt))

    Tnew = heat%tempinit(heat%tipbounds(0) : heat%tipbounds(2))
    ! Carefull!!! Tnew and Told run from 1: not from heat%tipbounds(0):
    Tnew(1) = heat%Tbound
    radii = sqrt(heat%Area(heat%tipbounds(0) : heat%tipbounds(2)) / pi) 

    do j = 1, solvesteps
        Told = Tnew
        do i = 2, Nx - 1   ! go through all the length of the tip except boundaries
            rho = resistivity(Told(i))
            K = L * Told(i) / rho
            K1 = K / Cv
            a(i) = -K1 * heat%dt / (heat%dx**2)
            b(i) = 1.d0 + 2.d0 * K1 * heat%dt / heat%dx**2
            c(i) = -K1 * heat%dt / (heat%dx**2)
            d(i) = Told(i) + heat%dt * heat%hpower(i+heat%tipbounds(0)-1) / Cv
        enddo
            
        ! Boundry conditions Tbottom = Tbound
        a(1) = 0.d0
        b(1) = 1.d0
        c(1) = 0.d0
        d(1) = heat%Tbound

        ! dT/dx|height = 0 => T(height) = T(height-1) =>
        a(Nx) = 1.d0
        b(Nx) = -1.d0
        d(Nx) = 0.d0

        ! Solve tridiagonal matrix with above coefficients
        call tridiag(a, b, c, d, Tnew)
        Tnew(1) = heat%Tbound
        heat%temperror = norm2((Tnew - Told) / (Told - heat%Tbound)) / size(Tnew)
        if (heat%temperror < convcr * heat%dt) exit
    enddo
    heat%tempfinal(heat%tipbounds(0) : heat%tipbounds(2)) = Tnew
    heat%tempfinal(heat%tipbounds(2)+1:) = -1.d0
    
    if (pars%debug > 0 .or. firsttime) then
        print *, 'Finished heateq after a time ', j * heat%dt, 'fs'
        print *, 'Did', min(j,solvesteps), 'timesteps' 
        firsttime = .false.
    endif
        
  
    contains
    
    subroutine tridiag(a, b, c, r, u)
        
        real(dp), intent(in)    :: a(:), b(:), c(:), r(:)
        real(dp), intent(out)   :: u(:)
        integer                 :: n ! Number of rows

        integer                 :: i
        real(dp)                :: b2, c2(size(a))

        n = size(a)

        b2 = b(1)
        u(1) = r(1)/b2
        do i = 2, n
            c2(i) = c(i-1)/b2
            b2 = b(i)-a(i)*c2(i)
            u(i) = (r(i)-a(i)*u(i-1))/b2
        end do

        do i = n-1, 1, -1
            u(i) = u(i)-c2(i+1)*u(i+1)
        end do
    end subroutine tridiag
    
end subroutine heateq




