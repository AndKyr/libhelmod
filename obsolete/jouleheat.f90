!> @file jouleheat.f90
!> @brief Home of the jouleheat module

!> @brief Module to handle resistive heating
module jouleheat
implicit none

private
public :: solve_heat_equation, resistivity

contains

!> @brief Solve the heat equation PDE
!>
!> Solve the 1D heat equation PDE of a tip on a surface. One end is kept at temperature @p T_boundary, while the other end is
!> thermally isolated. 
subroutine solve_heat_equation(temperature, current_density, T_boundary, dx, dt, fse, solvesteps)
implicit none
double precision, intent(inout) :: temperature(:) !< Temperature distribution of the tip. In: previous estimate, out: current estimate
double precision, intent(in) :: current_density(:) !< Current density distribution as a function of height
double precision, intent(in) :: T_boundary !< Boundary temperature at the bottom of the tip
double precision, intent(in) :: dx !< Grid size (Å)
double precision, intent(in) :: dt !< Timestep (fs)
double precision, intent(in) :: fse !< Multiplier for finite size effects
integer, intent(in) :: solvesteps !< Maximum number of iterations
double precision, parameter :: convergence_criterion = 1d-15

double precision :: prev_temperature(size(temperature))
integer :: i

prev_temperature = temperature

do i = 1, solvesteps
  call dsolve(temperature, current_density, T_boundary, dx, dt, fse)

  ! Check for convergence
  if (sqrt(sum((prev_temperature - temperature)**2)) < convergence_criterion) then
    exit
  end if

  prev_temperature = temperature
end do

! This was a problem at some point, but should not happen with reasonable current densities
if(any(temperature < 0)) then
  print *, "Got negative temperature. Weird!"
  stop
end if

end subroutine
!> @brief Function to calculate resistivity based on pre-tabulated values
!>
!> Return the resistivity for copper in the range 50..1200 K (unit ohm*meter), by looking up values in a pre-tabulated file (rho_table.h).
!> The values in rho_table.h are based on "Improved estimation of the resistivity of pure copper and electrical determination of thin copper film dimensions"
!> Schuster C.E.; Vangel M.G.; Schafft H.A
!>
!> One way to get the resistivity of another material would be to change the file rho_table.h
!>
!> Note, the function does not account for finite size effects, just temperature
!>
!> @todo Add explicit support for different materials (e.g. open different file with table)
function resistivity(T) result(rho)
double precision, intent(in) :: T !< Sample temperature
double precision :: rho
integer :: Ti
include "rho_table.h"

!Ti = min(nint(T)/10, 119)
Ti = nint(T)/10

if (Ti<5 .or. Ti+1>120) then
  print *, "WARNING: Temperature out of range in resisitivity calculation."
  print *, "Temperature capped at 1200K. Your system may be melting!", T

  if(Ti<5) Ti = 5
  if(Ti+1>120) Ti = 120
end if

! Resistivity is an exponential function, so use log to make it linear, do normal interpolation, and then make it exponential again
rho = exp( (log(rho_table(Ti+1)) - log(rho_table(Ti))) / (log(Ti*10d0+10) - log(Ti*10d0)) * (log(T) - log(Ti*10d0)) &
  + log(rho_table(Ti)) )

rho = rho*1e-8 ! Convert to correct units
end function

!> Function to calculate resistivity of copper at a given temperature
!> This is not really used anymore, instead the equivalent precomputed values are used in resistivity
!> This function still exists mostly as a reference.
function resistivity_cu(T) result(rho)
! The following values and equations taken from
! "Improved estimation of the resistivity of pure copper and electrical
!  determination of thin copper film dimensions"
! Schuster C.E.; Vangel M.G.; Schafft H.A
implicit none
double precision, intent(in) :: T !< Temperature of the system

double precision :: rho

! Note that the publication by Schuster et al. contains incorrect parameter values (correct ones are in the appendix of the paper)
double precision, parameter :: A = 1.816013d-8
double precision, parameter :: B = -2.404851d-3
double precision, parameter :: C = 4.560643d-2
double precision, parameter :: D = -5.976831d-3
double precision, parameter :: p = -1.838419d0
double precision, parameter :: theta = 310.8d0
double precision, parameter :: rho_R = 1.803752d-12
double precision :: expansion_correction

if(nint(T) == 0) then
  rho = rho_R
  return
end if

! Take thermal expansion into account
if(T<293.0) then
  expansion_correction = -0.2826+1.073d-3*(T-100)+2.904d-6*(T-100)**2-4.548d-9*(T-100)**3
else
  expansion_correction = 1.685d-3*(T-293)+2.702d-7*(T-293)**2+1.149d-10*(T-293)**3
end if

expansion_correction = expansion_correction/100.0 ! Calculations above are in percent

! Calculate resistivity
rho = A*(1+(B*T)/(theta-C*T)+D*((theta-C*T)/T)**p)*phi(T)+rho_R
rho = (1+expansion_correction) * rho
!rho = rho*1d10 ! Ohm meters -> Ohm Angstroms

contains

function phi(T) result(y)
implicit none
include "int_table.h" ! Needs to do some integration, but since it's slow just
                      ! use a lookup-table instead
double precision, intent(in) :: T
double precision :: x
double precision :: y
if(T < 50d0 .or. T > 1357d0) then
  print *, "Temperature out of range in resistivity calculation! T=", T
  stop
end if

x = (theta - C*T)/T
y = 4/x**5 * phi_integral(nint(T)) ! phi_integral defined in int_table.h
end function phi
end function resistivity_cu

!> Solve the heat equation PDE
!>
!> Solve one iteration of the 1D heat equation PDE of a tip on a surface. Needs to be iterated over many times.
subroutine dsolve(T, J, Tb, dx, dt, fse)
implicit none
double precision, intent(inout) :: T(:) !< Temperature distribution of the tip. In: previous estimate, out: current estimate
double precision, intent(in) :: J(:) !< Current density distribution as a function of height
double precision, intent(in) :: Tb !< Boundary temperature at the bottom of the tip
double precision, intent(in) :: dx !< Grid size (Å)
double precision, intent(in) :: dt !< Timestep (fs)
double precision, intent(in) :: fse !< Multiplier for finite size effects

double precision :: Told(size(T)) ! Old temperature distribution
double precision, parameter :: Cv = 3.45d6 ! J/(m^3 K)
double precision :: K ! Thermal conductivity, W/(m K)
double precision :: rho ! Electrical resistivity (Ohm m)
double precision :: K1 
double precision, parameter :: K2 = 1d0/Cv ! rho0/(Cv*T0)
double precision :: K3
double precision :: a(size(T)), b(size(T)), c(size(T)), d(size(T))
double precision :: cd
integer :: i
double precision, parameter :: L = 2.44e-8 ! Wiedemann Frantz
logical, save :: firsttime = .true.
integer :: height

integer, parameter :: method = 2

height = size(T)
if(firsttime) then
  print *, "fse", fse, "method", method
  print *, "J(height)", J(height)
  firsttime = .false.
end if

rho = 17e-9*fse ! Electrical resistivity
K = 400d0/fse ! Thermal conductivity

K1 = K/Cv

Told = T
T(1) = Tb

if(method==1) then
  ! Schuster et. al resistivity
  do i = 2, height-1
    rho = resistivity(T(i))*1d-8*fse
    K = L*T(i)/rho
    K1 = K/Cv

    a(i) = -K1*dt/(2*dx**2)
    b(i) = 1+K1*dt/dx**2
    c(i) = -K1*dt/(2*dx**2)
    ! Here we cheat a bit. Use old value of T as temperature when it should be avg. of new and old,
    ! since the resistivity is non-linear (Can't solve for the new temperature
    ! otherwise)
    d(i) = K1*dt/(2*dx**2)*(Told(i-1)-2*Told(i)+Told(i+1))+K2*dt*rho*J(height)**2
  end do

  a(1) = 0
  ! Boundry conditions
  ! T(1) = Tb =>
  b(1) = 1d0
  c(1) = 0d0
  d(1) = Tb

  ! dT/dx|height = 0 => T(height) = T(height-1) =>
  a(height) = 1d0
  b(height) = -1d0
  d(height) = 0d0

  ! Solve tridiagonal matrix with above coefficients
  call tridiag(a, b, c, d, T)
else if (method==2) then
  K3 = J(height)**2*K2
  ! Backward Euler with simple linear resistivity. Useful when e.g. debugging
  a = -K1*dt/dx**2
  b = 1+2*K1*dt/dx**2
  c = -K1*dt/dx**2
  d = T+K2*rho*J**2*dt

  ! Boundry conditions
  b(1) = 1d0
  c(1) = 0d0
  d(1) = Tb

  a(height) = 1d0
  b(height) = -1d0
  d(height) = 0d0

  ! Solve tridiagonal matrix with above coefficients
  call tridiag(a, b, c, d, T)

else if (method==3) then
  ! Very simple Forward Euler. If using this you still get incorrect results,
  ! clearly the problem must not be in the solver, but somewhere else.
  T(1) = Tb
  Told = T
  do i = 2, height-1
    cd = (Told(i-1)-2*Told(i)+Told(i+1))/dx**2
    T(i) = (K1*cd+K2*rho*J(i)**2)*dt + Told(i)
  end do
  i = height

  cd = (Told(i-1)-2*Told(i)+Told(i-1))/dx**2
  T(i) = (K1*cd+K2*rho*J(i)**2)*dt + Told(i)
else
  stop "Unknown solving method"
end if

T(1) = Tb
end subroutine

! Function to solve M*u=r where M is matrix
! [b1,c1,0 ,0 ,0 ,0 ,..,0 ]
! [a2,b2,c2,0 ,0 ,0 ,..,0 ]
! [0 ,a3,b3,c3,0 ,0 ,..,0 ]
! [0 ,0 ,a4,b4,c4,0 ,..,0 ]
! ...
! [0 ,0 ,0 ,0 ,0 ,..,an,bn]
! See Numerical Recipes

!> @brief Subroutine to solve tridiagonal matrix
!>
!> See numerical recipes tridiag
subroutine tridiag(a, b, c, r, u)
implicit none
double precision, intent(in) :: a(:), b(:), c(:), r(:)
double precision, intent(out) :: u(:)
integer :: n ! Number of rows

integer :: i
double precision :: b2, c2(size(a))

n = size(a)

b2 = b(1)
u(1) = r(1)/b2
do i = 2, n
  c2(i) = c(i-1)/b2
  b2 = b(i)-a(i)*c2(i)
  u(i) = (r(i)-a(i)*u(i-1))/b2
end do

do i = n-1, 1, -1
  u(i) = u(i)-c2(i+1)*u(i+1)
end do
end subroutine
end module

! Standard Fortran requires spaces instead of tabs
! vim: expandtab:tabstop=2:shiftwidth=2
! kate: indent-width 2; space-indent on; replace-tabs
