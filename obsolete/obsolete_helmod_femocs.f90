
subroutine is_surface (is_surf, i, j, k, Nx, Ny, Nz, philimit)
    !> Subroutine to determine whether the atom in grid is on surface or not

    logical, intent(out)    :: is_surf
    integer, intent(in)     :: i,j,k,Nx,Ny,Nz
    integer, intent(in)     :: philimit(:,:,:)
    
    integer :: ii,jj,kk,i0,i1,j0,j1,k0,k1
    logical :: edge_x, edge_y, edge_z, edge_xy, edge_xz, edge_yz, edge_xyz
    
    edge_x = .false.; edge_y = .false.; edge_z = .false.; edge_xyz = .false.
    edge_xy = .false.; edge_xz = .false.; edge_yz = .false.
    
    if(i > 1 .and. i < Nx) then
        edge_x = (philimit(i-1,j,k) /= DIRICHLET_BOUNDARY) .or. &
                (philimit(i+1,j,k) /= DIRICHLET_BOUNDARY)
    endif
    
    if(j > 1 .and. j < Ny) then
        edge_y = (philimit(i,j-1,k) /= DIRICHLET_BOUNDARY) .or. &
                (philimit(i,j+1,k) /= DIRICHLET_BOUNDARY)
    endif    
    
    if(k > 1 .and. k < Nz) then
        edge_z = (philimit(i,j,k-1) /= DIRICHLET_BOUNDARY) .or. &
                (philimit(i,j,k+1) /= DIRICHLET_BOUNDARY)
    endif
    
!    if(i > 1 .and. j > 1 .and. i < Nx .and. j < Ny) then
!        edge_xy = (philimit(i-1,j-1,k) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i-1,j+1,k) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i+1,j-1,k) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i+1,j+1,k) /= DIRICHLET_BOUNDARY)
!    endif
    
!    if(i > 1 .and. k > 1 .and. i < Nx .and. k < Nz) then
!        edge_xz = (philimit(i-1,j,k-1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i-1,j,k+1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i+1,j,k-1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i+1,j,k+1) /= DIRICHLET_BOUNDARY)
!    endif
    
!    if(j > 1 .and. k > 1 .and. j < Ny .and. k < Nz) then
!        edge_yz = (philimit(i,j-1,k-1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i,j+1,k-1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i,j-1,k+1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i,j+1,k+1) /= DIRICHLET_BOUNDARY)
!    endif
    
!    if(i > 1 .and. j > 1 .and. k > 1 .and. i < Nx .and. j < Ny .and. k < Nz) then
!        edge_xyz = (philimit(i-1,j-1,k-1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i-1,j-1,k+1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i-1,j+1,k-1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i-1,j+1,k+1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i+1,j-1,k-1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i+1,j-1,k+1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i+1,j+1,k-1) /= DIRICHLET_BOUNDARY)&
!              .or. (philimit(i+1,j+1,k+1) /= DIRICHLET_BOUNDARY)
!    endif
    
    is_surf = edge_x .or. edge_y .or. edge_z
    ! .or. edge_xy .or. edge_xz .or. edge_yz .or. edge_xyz
    
end subroutine is_surface


subroutine check(els, x0, atoms)
    type(Electrostatics), intent(in)    :: els
    real(dp), intent(in)    :: x0(:)
    integer, intent(in)     :: atoms(:,:,:)
    
    real(dp)    :: x(1), y(1), z(1)
    integer     :: Ni(1)
    
    integer ::i
    
!    print *, 'x0 of first:', x0(1:3)
!    print *, 'x0 edited:', (x0(1:3) + 0.5) * globs%box
    print *, 'box:', globs%box(1), 64*els%grid_spacing(1)
    
    print *, 'atoms and feild size', shape(atoms), shape(els%Efield)
    print *, '5*grid', 4*els%grid_spacing(1)
    print *, 'grid_sp:', els%grid_spacing
    do i = 1, size(els%philimit, 3) / 4
        Ni = atoms(1,1,i)
        if (Ni(1) == 0) then
            x = 0.d0; y = 0.d0; z = 0.d0
        else
            call transform_x0(els, x0, Ni, x, y, z)
        endif
        
        print *, 0.5*els%grid_spacing(1), 0.5*els%grid_spacing(2), &
            (i-els%zbounds(1)+0.5)*els%grid_spacing(3) , els%philimit(1,1,i), &
            x, y, z, atoms(1,1,i), els%Efield(1,1,i,4)

    enddo
    
    print *, 'geia sas'
    stop  'stopstop'

end subroutine check

subroutine Efield_atpoint(poten, fem, i, j, k, Efield) 
    
    type(Potential), intent(in) :: poten !< Ιnput potential structure.
    type(femocs), intent(in)    :: fem !< Femocs data structure.
    
    integer, intent(in)         :: i, j, k !< Asked point on the grid.
    real(dp), intent(out)       :: Efield(4) !< Output field. 4th is the norm.    
    real(dp)                    :: x(1), y(1), z(1) !Coordinates of the asked point.
    real(dp)                    :: Ex(1), Ey(1), Ez(1), Enorm(1) !Field components.
    
    integer                     :: iflag(1), flag
    

    if(pars%EDmethod == 0) then
        Efield(1:3) = ([poten%phi(i+1,j,k), poten%phi(i,j+1,k), poten%phi(i,j,k+1)] &
                    - [poten%phi(i-1,j,k), poten%phi(i,j-1,k), poten%phi(i,j,k-1)]) &
                    / poten%grid_spacing
        Efield(4) = norm2(Efield(1:3))
    elseif(pars%EDmethod == 1) then
    
        x(1) = 10. * poten%grid_spacing(1) * (i - 1) !femocs takes A and not nm!!
        y(1) = 10. * poten%grid_spacing(2) * (j - 1)
        z(1) = 10. * poten%grid_spacing(3) * (k - 1)

        call fem%interpolate_elfield(flag, 1, x, y, z, Ex, Ey, Ez, Enorm, iflag)
        Efield = [-10. * Ex(1), -10.*Ey(1), -10.* Ez(1), 10.* Enorm(1)] 
        !field is given in V/A in femocs, and it is inwards.
        
        if (iflag(1)/=0 .and. pars%debug) then
            print *, 'Warning: Interpolation of Efield out of bounds. flag =', iflag
            print *, '[i,j,k] = ', i, j, k  
            print *, '[x,y,z] =', x, y, z
            print *, 'Efield =', Efield
        endif
    else
        print *, 'Wrong EDmethod parameter'
        stop
    endif

end subroutine Efield_atpoint


subroutine grid2coordinates(types, x, y, z, n_atoms, philimit, grid_spacing)

    !> Transform grid information into x, y, z coordinates
    use iso_c_binding

    integer, intent(in)     :: philimit(:,:,:)
    real(dp), intent(in)    :: grid_spacing(3)
    
    real(c_double), dimension(size(philimit,1)*size(philimit,2)*size(philimit,3)), &
                    intent(out) :: x, y, z
    integer(c_int), dimension(size(philimit,1)*size(philimit,2)*size(philimit,3)), &
                    intent(out) :: types
    integer(c_int), intent(out) :: n_atoms

    integer                     :: i,j,k,Nx,Ny,Nz
    integer(c_int), parameter   :: type_bulk = 1
    integer(c_int), parameter   :: type_surf = 2
    
    logical                     :: is_surf

    Nx=size(philimit,1)
    Ny=size(philimit,2)
    Nz=size(philimit,3)
    
    n_atoms = 0
    
    do k=1,Nz
        do j=1,Ny          
            do i=1,Nx
                if (philimit(i,j,k) == DIRICHLET_BOUNDARY) then               
                    n_atoms = n_atoms + 1
                    x(n_atoms) = (i-1) * grid_spacing(1)
                    y(n_atoms) = (j-1) * grid_spacing(2)
                    z(n_atoms) = (k-1) * grid_spacing(3)
                                     
                    call is_surface (is_surf, i, j, k, Nx, Ny, Nz, philimit)
                    
                    if(is_surf) then
                        types(n_atoms) = type_surf
                    else
                        types(n_atoms) = type_bulk
                    endif 
                endif
            enddo
        enddo
    enddo
    
end subroutine grid2coordinates





