module visualize

implicit none

integer, parameter  :: dp = 8

contains

subroutine print_vector(phi, grid_spacing, fidin, Nframe)
! prints all the grid in a .xyz format. Then it can be imported to ovito and 
! visualized as colours. This is for vector quantities.

	real(dp), intent(in)    :: phi(:,:,:,:), grid_spacing(3)
    integer, optional       :: fidin, Nframe
	integer                 ::i, j, k, Nx, Ny, Nz, fid, counter	
    
    Nx=size(phi,1); Ny=size(phi,2); Nz=size(phi,3)
    
    if (.not. present(fidin)) then
        open(fid,file='scalar_grid.xyz')
    else
        fid = fidin
    endif
    
	write(fid,*) Nx * Ny * Nz  ! writing No of GP
    if (present(Nframe))  then
        write(fid,*) 'Frame', Nframe, 'GP, x, y, z, scalar'
    else
        write(fid,*) 'GP, x, y, z, scalar'
    endif
    
    counter = 1
	do k=1,Nz
		do j=1,Ny
			do i=1,Nx
                write(fid,*) counter, (i-1)*grid_spacing(1), (j-1)*grid_spacing(2), &
                (k-1)*grid_spacing(3), phi(i,j,k,1), phi(i,j,k,2), phi(i,j,k,3)
                counter = counter + 1 
			enddo
		enddo
	enddo
    
end subroutine print_vector

subroutine print_scalar(phi, grid_spacing, fidin, Nframe)
! prints all the grid in a .xyz format. Then it can be imported to ovito and 
! visualized as colours. This is for scalar quantities.

	real(dp), intent(in)    :: phi(:,:,:), grid_spacing(3)
    integer, optional       :: fidin, Nframe
	integer                 ::i, j, k, Nx, Ny, Nz, fid, counter	
    
    Nx=size(phi,1); Ny=size(phi,2); Nz=size(phi,3)
    
    if (.not. present(fidin)) then
        open(fid,file='scalar_grid.xyz')
    else
        fid = fidin
    endif
    
	write(fid,*) Nx * Ny * Nz  ! writing No of GP
    if (present(Nframe))  then
        write(fid,*) 'Frame', Nframe, 'GP, x, y, z, scalar'
    else
        write(fid,*) 'GP, x, y, z, scalar'
    endif
    
    counter = 1
	do k=1,Nz
		do j=1,Ny
			do i=1,Nx
                write(fid,*) counter, (i-1)*grid_spacing(1), (j-1)*grid_spacing(2), &
                (k-1)*grid_spacing(3), phi(i,j,k)
                counter = counter + 1 
			enddo
		enddo
	enddo
end subroutine print_scalar


subroutine print_boundary(philimit,grid_spacing)

	integer, intent(in):: philimit(:,:,:)
	double precision, intent(in):: grid_spacing(3)
	integer::i,j,k,Nx,Ny,Nz,fid=1987,counter=1
	double precision, dimension(size(philimit,1)*size(philimit,2)*size(philimit,3)):: x,y,z
    integer, dimension(size(philimit,1)*size(philimit,2)*size(philimit,3)):: boundtype
	Nx=size(philimit,1)
	Ny=size(philimit,2)
	Nz=size(philimit,3)
	
	do i=1,Nx
		do j=1,Ny
			do k=1,Nz
				if (philimit(i,j,k)==-10) then
					x(counter)=(i-1)*grid_spacing(1)
					y(counter)=(j-1)*grid_spacing(2)
					z(counter)=(k-1)*grid_spacing(3)
                    boundtype(counter) = philimit(i,j,k)
					counter=counter+1
				endif
			enddo
		enddo
	enddo
	
	open(fid,file='boundary_grid.xyz',action='write',status='replace')
	
	write(fid,*) counter-1
	write(fid,*) 'eimaste treloi'
	
	do i=1,counter-1
		write(fid,*) 0,' ',x(i),' ',y(i),' ',z(i), boundtype(i)
	enddo
	
	close(fid)

end subroutine print_boundary

subroutine print_phi(phi,grid_spacing)

	implicit none

	double precision, intent(in):: phi(:,:,:)
	double precision, intent(in):: grid_spacing(3)
	integer::i,j,k,Nx,Ny,Nz,fid=154967

	Nx=size(phi,1)
	Ny=size(phi,2)
	Nz=size(phi,3)
	
	open(fid,file='phi_grid.dat',action='write',status='replace')
	
	write(fid,*) Nx, Ny, Nz
	write(fid,*) grid_spacing
	write(fid,*) ''
		
	do i=1,Nx
		do j=1,Ny
			write(fid,*) phi(i,j,:)
		enddo
		write (fid,*)
	enddo
	
	close(fid)
end subroutine print_phi


end module visualize
