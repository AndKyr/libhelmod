! MODULE ppm
!
!> @author Stefan Parviainen, 2011-2012, 2015
!> @copyright Public Domain where applicable. No rights reserved.
!
! DESCRIPTION:
!> @brief Module to facilitate writing array data to PPM graphics files to visualize contents
!
!> This is a module which contains subroutines for writing arrays to a ppm graphics files in order to visualize their contents.
!
module ppm
implicit none
integer, parameter :: MAX_COLOR = 65535 !< Maximum color intensity (16 bit per channel)

!> Generic function for writing a 2D array to a ppm file
interface write_ppm
        module procedure write_ppm_dbl_filename
        module procedure write_ppm_dbl_unit
end interface

!> Generic function for writing a slice of an array to a ppm file
interface write_slice_ppm
        module procedure write_slice_ppm_dbl_filename
        module procedure write_slice_ppm_dbl_unit
end interface

!> Interface for function that can be used as a colormap
abstract interface
        pure function f_colormap(x)
                !> @return Array containing R,G,B values
                integer, intent(in) :: x !< Value of an element
                integer :: f_colormap(3)
        end function
end interface
contains

!> @brief Write a slice of an array as a ppm to a named file
!>
!> Take a slice along axis @a axis of array @a arr and write it to a file called @a filename using the optional colormap @a colormap
subroutine write_slice_ppm_dbl_filename(arr, axis, filename, colormap)
implicit none
double precision, intent(in) :: arr(:,:,:) !< 3D array containing data to write to PPM file
integer, intent(in) :: axis !< Axis along which to slice (1,2 or 3)
character(len=*), intent(in) :: filename !< Filename of output ppm file
procedure(f_colormap), optional :: colormap !< Colormap to use. Default: grayscale

select case(axis)
        case (1)
                if(present(colormap)) then
                        call write_ppm(arr(size(arr,1)/2,:,:), filename, colormap)
                else
                        call write_ppm(arr(size(arr,1)/2,:,:), filename)
                end if
        case (2)
                if(present(colormap)) then
                        call write_ppm(arr(:,size(arr,2)/2,:), filename, colormap)
                else
                        call write_ppm(arr(:,size(arr,2)/2,:), filename)
                end if
        case (3)
                if(present(colormap)) then
                        call write_ppm(arr(:,:,size(arr,3)/2), filename, colormap)
                else
                        call write_ppm(arr(:,:,size(arr,3)/2), filename)
                end if
end select
end subroutine

!> @brief Write a slice of an array as a ppm to an already opened file
!>
!> Take a slice along axis @a axis of array @a arr and write it to file unit @a unit using the optional colormap @a colormap
subroutine write_slice_ppm_dbl_unit(arr, axis, funit, colormap)
implicit none
double precision, intent(in) :: arr(:,:,:) !< 3D array containing data to write to PPM file
integer, intent(in) :: axis !< Axis along which to slice (1,2 or 3)
integer, intent(in) :: funit !< File unit to which data is written
procedure(f_colormap), optional :: colormap !< Colormap to use. Default: grayscale

select case(axis)
        case (1)
                if(present(colormap)) then
                        call write_ppm(arr(size(arr,1)/2,:,:), funit, colormap)
                else
                        call write_ppm(arr(size(arr,1)/2,:,:), funit)
                end if
        case (2)
                if(present(colormap)) then
                        call write_ppm(arr(:,size(arr,2)/2,:), funit, colormap)
                else
                        call write_ppm(arr(:,size(arr,2)/2,:), funit)
                end if
        case (3)
                if(present(colormap)) then
                        call write_ppm(arr(:,:,size(arr,3)/2), funit, colormap)
                else
                        call write_ppm(arr(:,:,size(arr,3)/2), funit)
                end if
end select
end subroutine

!> @brief Write a 2D array to a ppm file
!>
!> Write a 2D array @a arr to a file called @a filename using the optional color map @a colormap
subroutine write_ppm_dbl_filename(arr, filename, colormap)
implicit none
double precision, intent(in) :: arr(:,:) !< 2D array containing data to write to PPM file
character(len=*), intent(in) :: filename !< Filename of output ppm file
procedure(f_colormap), optional :: colormap !< Colormap to use. Default: grayscale

integer :: funit

! newunit requires a Fortran 2008 compiler
open(newunit=funit, file=filename)
if(present(colormap)) then
        call write_ppm(arr, funit, colormap)
else
        call write_ppm(arr, funit)
end if
close(funit)

end subroutine

!> @brief Write a 2D array to an already opened ppm file
!>
!> Write a 2D array @a arr to file unit @a funit using the optional color map @a colormap
subroutine write_ppm_dbl_unit(arr, funit, colormap)
implicit none
double precision, intent(in) :: arr(:,:) !< 2D array containing data to write to PPM file
integer, intent(in) :: funit !< File unit to which data is written
procedure(f_colormap), optional :: colormap !< Colormap to use. Default: grayscale

integer :: i, j, color
real :: amin, amax, cscale

amin = minval(arr)
amax = maxval(arr)
cscale = MAX_COLOR / (amax - amin) ! Color scaling factor to cover whole color range

! Write PPM header
if (present(colormap)) then
        write (funit, '(A2)') 'P3' ! Color
else
        write (funit, '(A2)') 'P2' ! Grayscale
end if

write (funit, '(I0," ",I0,/,I0)') size(arr,1), size(arr,2), MAX_COLOR

! Write original minimum and maximum values as a comment
! Useful if you want to make e.g. colorbars later
write (funit, *) '#', amin, amax

! Write color data. Loop backwards over rows to get "image coordinates" (y is down)
do j = size(arr,2),1,-1; do i = 1,size(arr,1)
        color = nint( (arr(i,j) - amin) * cscale ) ! Scale color
        if (present(colormap)) then
                write (funit, *) colormap(color)
        else
                write (funit, *) color
        end if
end do; end do

end subroutine write_ppm_dbl_unit

! Predefined colormaps
! --------------------

!> @brief Map a given value to a grayscale colormap
pure function grayscale(x) result(rgb)
implicit none
integer, intent(in) :: x !< Value between 0 and MAX_COLOR

integer :: rgb(3)

rgb = x
end function grayscale

!> @brief Map a given value to a rainbow colormap
pure function rainbow(x) result (rgb)
implicit none

integer, intent(in) :: x !< Value between 0 and MAX_COLOR

integer :: rgb(3)

double precision :: alpha
double precision, parameter :: pi = 3.14159!acos(0.0)

rgb(1) = sin(pi*x/MAX_COLOR + 0*pi/3)*MAX_COLOR
rgb(2) = sin(pi*x/MAX_COLOR + 2*pi/3)*MAX_COLOR
rgb(3) = sin(pi*x/MAX_COLOR + 4*pi/3)*MAX_COLOR
end function rainbow
end module
