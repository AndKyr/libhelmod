!> @file emission.f90
!> @brief Home of the emission module

!> @brief Module containing functions related to field emission from metallic emitters
module emission
implicit none
private
public :: fowler_nordheim2, gtf
double precision, parameter :: pi = 3.1415926535d0
contains

!> @brief General Thermal Field-equation from Bachelors thesis of Kristjan Eimre (Tartu)
!>
!> Variables etc. chosen to match his implementation
!> Equation becomes incorrect at very high fields, so the field is capped at 13GV/m.
!> The equation is currently contains hard-coded values for Cu! This should probably be changed.
!> Also see also work by K. L. Jensen et al.
!> @todo Make function work with other materials than Cu
function gtf(g_normE, g_T) result(g_J)
implicit none
double precision, parameter :: k_B = 1d0/11604.506
double precision, parameter :: e_charge = 1.60217657d-19
double precision, parameter :: eps_0 = 1.41859723d-39
double precision, parameter :: me = 510998.9/(2.99792458d17)**2 ! eV/(nm/s)^2
double precision, parameter :: h_bar = 6.58211928d-16 ! eV*s
double precision, parameter :: h_p = h_bar*2*pi ! eV*s
double precision, parameter :: A_RLD = 120.17349 ! A/(K^2*cm^2)
double precision, parameter :: cu_Phi = 4.5 ! eV
double precision, parameter :: cu_mu = 7 ! eV

double precision, intent(in) :: g_T !< System temperature (K)
double precision, intent(in) :: g_normE !< Electric field (V/m)

double precision :: g_F
double precision :: g_Q
double precision :: g_phi
double precision :: g_y
double precision :: g_beta_T
double precision :: g_B_q
double precision :: g_nu
double precision :: g_tau ! NB difference! g_t in original conflicts with g_T in fortran
double precision :: g_B_FN
double precision :: g_C_FN
double precision :: g_T_min
double precision :: g_T_max
double precision :: g_a
double precision :: g_b
double precision :: g_c
double precision :: g_z
double precision :: g_s
double precision :: g_n
double precision :: g_n2 ! NB difference! g_N in original conflicts with g_n in fortran
double precision :: g_J
double precision :: g_region

! Picking max of field and 1e-12 is meant to ensure that g_F is not exactly 0
g_F = max(min(g_normE*1d-9,13d0),1e-12) ! Force, eV/nm, capped

if(g_normE*1d-9 > 13) then
  print *, "WARNING, field in GTF, ", g_normE, "is above safe limit", 13d0, "Field is capped to this value"
end if

g_Q = e_charge**2/(16*pi*eps_0)
g_phi = max(cu_Phi-(4*g_Q*g_F)**(1d0/2d0), 0.2)
g_y = (4*g_Q*g_F)**(1d0/2d0)/cu_Phi
g_beta_T = 1d0/(k_B*g_T)
g_B_q = (pi/h_bar*g_phi)*sqrt(2*me)*(g_Q/g_F**3)**(1d0/4d0)
g_nu = (1-g_y**2)+1d0/3d0*g_y**2*log(g_y)
g_tau = 1d0/9d0*g_y**2*(1-log(g_y))+1 ! NB variable name
g_B_FN = (4d0/(3d0*h_bar*g_F))*sqrt(2*me*cu_Phi**3)*g_nu
g_C_FN = (2*g_phi/(h_bar*g_F))*sqrt(2*me*cu_Phi)*g_tau
g_T_min = 1d0/(k_B*1d0/g_phi*(g_C_FN))
g_T_max = 1d0/(k_B*1d0/g_phi*(g_B_q))
g_a = -3*(2*(g_B_FN)-g_B_q-(g_C_FN))
g_b = 3*(2*(g_B_FN)-g_B_q-(g_C_FN))+g_B_q-(g_C_FN)
g_c = (g_C_FN)-g_phi*g_beta_T
g_z = -g_b/(2*g_a)+sqrt((g_b/(2*g_a))**2-g_c/g_a)

if(g_T>g_T_max) then
  g_s = g_B_q
elseif(g_T<g_T_min) then
  g_s = g_B_FN
else
  g_s = g_B_FN+(g_b/2d0)*g_z**2+2d0/3d0*g_a*g_z**3
end if

if(g_T>g_T_max) then
  g_n = g_beta_T/(g_B_q/g_phi)
elseif (g_T<g_T_min) then
  g_n = g_beta_T/(g_C_FN/g_phi)
else
  g_n = 1.0
end if

if(g_n == 1.0) then
  g_n2 = exp(-g_s)*(g_s+1)
else
  g_n2 = g_n**2*GTFE_sigma(1d0/g_n)*exp(-g_s)+GTFE_sigma(g_n)*exp(-g_n*g_s)
end if

g_J = A_RLD*g_T**2*g_n2*1d4 ! A/cm^2 -> A/m^2

if (g_T>g_T_max) then
  g_region = 1
elseif(g_T<g_T_min) then
  g_region = -1
else
  g_region = 0
end if

contains
pure double precision function GTFE_sigma(x)
implicit none
double precision, intent(in) :: x
GTFE_sigma = 1d0/(1-x)-(x*(1+x)-1d0/4d0*x**3*(7*x-3)-pi**2/6d0*x**2*(1-x**2))
end function
end function

!> @brief Calculate FN-current using the given electric field
!>
!> Calculate the temperature using the Fowler-Nordheim equation and a temperature correction factor. See e.g. R. Forbes, "Simple good
!> approximations for the special elliptic functions in standard Fowler-Nordheim tunneling theory for a Schottky-Nordheim barrier" APPLIED PHYSICS LETTERS 89, 113122 2006
function fowler_nordheim2(E, T) result(J_FN)
implicit none
double precision, intent(in) :: E ! Electric field
double precision, intent(in) :: T ! Temperature

double precision :: J_FN ! Current density

double precision, parameter :: w = 4.53 ! Workfunction, assume Cu, eV
double precision, parameter:: g = 10.24624*1d9 ! eV^(-1/2) m^(-1)

double precision, parameter :: hp = 6.6261d-34 ! Planck's constant
double precision, parameter :: hpb = 1.0546d-34 ! Reduced Planck's constant
double precision, parameter :: eps0 = 0.0553d9 ! Vacuum permittivity, eV V^(-2) m^(-1)
double precision, parameter :: em = 9.11e-31 ! Electron mass, kg
double precision, parameter :: kBeV = 8.61734215d-5 ! Boltzmann constant, eV
double precision, parameter :: ec = 1.602176462d-19 ! Electron charge (C)

double precision, parameter :: c = 1d0/(4*pi*eps0)/w**2

double precision :: f
double precision :: vf
double precision :: tf

double precision :: df
double precision :: z
double precision :: lambdaT

double precision, parameter :: prefactor = ec**3/(8*pi*hp*(w*ec))
double precision :: expfactor
double precision :: E2

!E2 = min(E, 10d9) ! Limit electric field so current doesn't get too huge
E2 = E

expfactor = 4*sqrt(2*em)*(w*ec)**(3.0/2.0)/(3*hpb*ec)

! Calculate temperature correction factor
if(T > 0) then
  df = 1d0/(g*sqrt(w))*E2 ! Decay width for barrier
  z = pi*kBeV*T/df
  lambdaT = z/sin(z) ! Temperature correction factor
else
  lambdaT = 1
end if

f = c*E2 ! "scaled barrier field", f=y**2, y is Nordheim parameter

if(f>1d0) f=1d0

! This parameter explained in more detail in "Simple good approximations for the
! special elliptic functions in standard Fowler-Nordheim tunneling theory for
! a Schottky-Nordheim barrier"
! APPLIED PHYSICS LETTERS 89, 113122 2006
! Forbes, G.
if(f>0) then
  vf = 1 - f + (1d0/6.0) * f * log(f)
else
  vf=1
end if

! There is en exact series expansion for the expression above,
! but apparently the simple version is more accurate unless
! many terms are included
! "The formal derivation of an exact series expansion for
! the principal Schottky–Nordheim barrier function v,
! using the Gauss hypergeometric differential equation"
! J. Phys. A: Math. Theor. 41 (2008) 395301
! Jonathan H B Deane and Richard G Forbes
! vf =1 -(9.0/8.0*log(2.0)+3.0/16.0)*f &
!   -(27.0/256.0*log(2.0)-51.0/1024.0)*f**2 &
!   -(315.0/8192.0*log(2.0)-177.0/8192.0)*f**3 &
!   +f*log(f)*(3.0/16.0+9.0/512.0*f+105.0/16384.0*f**2)

tf = 1 + f/9.0*(1-1.0/2.0*log(f))

! Often it is assumed lambdaT=1 and tf=1, here we don't do that
! Also, sometimes vf=1 which is a horrible idea (gives very bad results)
J_FN = prefactor * lambdaT/tf**2 * E2**2 * exp(-vf*expfactor/E2)

end function fowler_nordheim2

! Version of FN that corrects for space-charge
! Hasn't been used in a while
! Vality of approach has been questioned by H. Urbassek during project evaluation in 2009
! Use at own risk
function fowler_nordheim(El, V, T) result (Jp)
implicit none
double precision, parameter :: step = 1d7 ! Step size when searching

double precision, intent(in) :: El ! Electric field above tip without space charge
double precision, intent(in) :: V ! Voltage between anode and cathode
double precision, intent(in) :: T ! Temperature of tip

double precision :: Ep ! Electric field with space charge
double precision :: E
double precision :: Jp ! Actual current density

! Search for Ep corresponding to El
Ep = El*1d-1 ! Start low enough
do
  E = Ep/theta(Ep, V, T)
  if (E-El > 0) exit ! Found corresponding Ep
  Ep = Ep+step
end do
!Jp = fowler_nordheim2(Ep, T) ! Calculate FN-current using actual field
Jp = fowler_nordheim2(El, T) ! Calculate FN-current using actual field
end function fowler_nordheim

! Correction term for electric field due to space chage effect
! See "Exact analysis of surface field reduction due to field-emitted vacuum space
! charge, in parallel-plane geometry, using simple dimensionless equations"
! Richard G. Forbes. Journal of Applied Physics v. 104, iss. 8
! Gives the correction term as a function of the "real" electric field
! (which is usually unknown)
function theta(Ep, V, temperature) result(t)
implicit none
double precision, intent(in) :: Ep ! "Real" value of electric field
double precision, intent(in) :: V ! Voltage between anode and cathode
double precision, intent(in)  :: temperature

double precision :: zeta, S, t
double precision, parameter :: K = 1.904d5

zeta = K*sqrt(V)*fowler_nordheim2(Ep, temperature)/Ep**2
S = sqrt(1+4*zeta**2*(4*zeta-3))
if (zeta < 1d-5) then
  t = 1.0
elseif (zeta < 0.5) then
  t = 1.0/(6*zeta**2)*(1-S);
else
  t = 1.0/(6*zeta**2)*(1+S);
end if
end function theta
end module

! Standard Fortran requires spaces instead of tabs
! vim: expandtab:tabstop=2:shiftwidth=2
! kate: indent-width 2; space-indent on; replace-tabs
