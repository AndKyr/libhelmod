program temperature

use helmod, only: handle_elfield, set_globs, set_params

implicit none

integer, parameter      :: dp = 8
real(dp), allocatable   :: x0(:), x1(:), xq(:), xnp(:), Efield(:,:,:,:)
integer, allocatable    :: atype(:), nborlist(:)
integer                 :: myatoms, ncell(3)
real(dp)                :: box(3), rc(3), capplfield, timeunit(2), elfield, scaling = 1.d0

print *, "read globs"
call read_globs()
call set_params(0.d0, 0.d0, elfield, elfield, 0, 1, 2, 0, 0)

print *, "read positions"
call read_positions()

print *, "setting globs"
print *, box, ncell
call set_globs(1.d0, scaling * box, ncell, 1.d0, timeunit, .false., 2*ncell(3) + 10)

allocate(x1(3*myatoms), xq(4*myatoms), xnp(3*myatoms), atype(myatoms), &
    nborlist(myatoms))

x1 = 1.
xq = 1.
xnp = 1.
atype = 1
nborlist = 0

print *, "calling handle_efield"
call handle_elfield(x0, x0, myatoms, nborlist, xq, atype, Efield, rc, xnp, capplfield )

contains

subroutine read_positions()
    
    character(len=2)       :: dummy
    integer, parameter      :: fid = 14
    integer                 :: i
    
    open(fid, file="in/mdlat.in.xyz", action = 'read')

    read(fid,*) myatoms
    read(fid,*)
    allocate(x0(3*myatoms))
    
    do i = 0, myatoms-1
        read(fid,*) dummy, x0(3*i+1), x0(3*i+2), x0(3*i+3)
        x0(3*i+1) = x0(3*i+1) / box(1)
        x0(3*i+2) = x0(3*i+2) / box(2)
        x0(3*i+3) = x0(3*i+3) / box(3)
    enddo
    close(fid)
end subroutine read_positions


subroutine read_globs()
    use libfemocs, only: femocs

    
    type(femocs)            :: fem
    integer                 :: flag
    real(dp)                :: box1, box2, box3
    
    fem = femocs("in/md.in")
    
    call fem%parse_double(flag, 'elfield', elfield)
    call fem%parse_double(flag, 'scaling', scaling)
    
    call fem%parse_double(flag, 'box1', box(1))
    call fem%parse_double(flag, 'box2', box(2))
    call fem%parse_double(flag, 'box3', box(3))
    
    call fem%parse_int(flag, 'ncell1', ncell(1))
    call fem%parse_int(flag, 'ncell2', ncell(2))
    call fem%parse_int(flag, 'ncell3', ncell(3))

    call fem%delete()

end subroutine read_globs


end program temperature
