program rhotest

integer, parameter  :: dp = 8
integer :: i, j ,k
real(8) :: x, y, z


print *, resistivity(300.d0, 60.d0)
print *, resistivity(1100.d0, 1.d0)
print *, resistivity(1500.d0, 0.5d0)



contains

function resistivity(T, radius) result(rho)
!    use std_mat, only: lininterp
    
    real(dp), intent(in)    :: T, &!< Temperature  in K
                                radius !< Radius of the cylinder in nm
    real(dp)                :: rho !< output resistivity in Ohm * nm
    real(dp)                :: drhodT, fse, radlog, temp(2)
    integer :: Ti
    include "rho_table.h"
    
    if (T >= 1200.d0) then
        drhodT = (rho_table(120)- rho_table(119)) / 10.d0
        rho = rho_table(120) + drhodT * (T - 1200.d0)
    elseif (T <= 5.d0) then
        drhodT = (rho_table(6)- rho_table(5)) / 10.d0
        rho = rho_table(5) + drhodT * (T - 50.d0)
    else
        rho = lininterp(rho_table(5:120), 50.d0, 1200.d0, T)
    endif
    
    print *, 'rho =', rho
    
    rho = 10.d0 * rho
    
    if (radius > 50.) then
        return
    elseif (radius <= 0.5) then
        fse = 81.83d0
    else
        temp = interp1(radlog_table, fselog_table, log(radius))
        if (temp(2) == 0.d0) fse = exp(temp(1))
    endif
    
    print *, 'fse = ', fse
    
    rho = rho * fse
    
end function resistivity

pure function binsearch(x, x0)  result(ind)
!binary search in sorted vector x for x(i) closest to x
    real(dp), intent(in)    :: x(:), x0
    integer                 :: ind(2) ! 1: output, 2: iflag
    !iflag 0: fine, 1: x0 out of bounds, -1: x not sorted
    integer                 :: i, ia, ib, imid
    
    if ((x0 < x(1)) .or. (x0 > x(size(x)))) then
        ind(2) = 1
        return
    endif
    
    ia = 1
    ib = size(x) +1
    do i=1,size(x)
        imid = (ia + ib) / 2
        if (x(imid) < x0) then
            ia = imid
        else
            ib = imid
        endif
        if (abs(ia-ib) <= 1) then
            if (abs(x(ia)-x0) < abs(x(ib)-x0)) then
                ind = ia
            else
                ind = ib
            endif
            ind(2) = 0
            return
        endif
    enddo
    ind(2) = -1
    
end function binsearch

pure function lininterp(yi, a, b, x) result(y)
!simple linear interpolation function
!appropriate for uniform linspace
    real(dp), intent(in)    :: a, b, x, yi(:) 
    ! yi interpolation array, a, b are x interval limits and x is the requested point
    integer                 :: Nnear, N
    real(dp)                :: y, dx, dy, xnear
    
    if (x < a .or. x > b) then
        y = 1.d308
        return
    elseif (a >= b) then
        y = 2.d307
        return
    endif
        
    N = size(yi)
    Nnear = nint((x-a) * (N-1) / (b-a)) + 1
    if (Nnear > N .or. Nnear < 1) then
        y = 2.d306
        return
    endif
    
    dx = (b-a) / dfloat(N-1)
    
    if (Nnear == N) then
        xnear = b
    elseif (Nnear == 1) then
        xnear = a
    else
        xnear=a + (Nnear-1) * dx
    endif
    
    if (x > xnear) then
        dy = yi(Nnear + 1) - yi(Nnear)
    elseif (x < xnear) then
        dy = yi(Nnear) - yi(Nnear - 1)
    else
        dy = 0.d0
    endif
    y = yi(Nnear) + (dy/dx) * (x - xnear)
    
end function lininterp


pure function interp1(xi, yi ,x) result(y)
!simple linear interpolation function (same as interp1 of matlab)
!appropriate for non-uniform linspace

    real(dp), intent(in)    :: xi(:), yi(:), x
    real(dp)                :: y(2) ! 1: output, 2: flag
    integer                 :: dN, Nclose, binout(2)
    
    binout = binsearch(xi, x) !find closest xi to x
    y(2) = real(binout(2), dp)
    if (binout(2) /= 0) return
    dN = 1
    Nclose = binout(1)
    if (xi(Nclose) > x) dN = -1
    y(1) = yi(Nclose) + (x-xi(Nclose)) * & !linear interpolation
        ((yi(Nclose + dN) - yi(Nclose)) / (xi(Nclose + dN) - xi(Nclose)))
    
end function interp1

end
