!> @file helmod.f90
!> @brief Home of the helmod module

!> @mainpage
!> HELMOD is a fork of the classical Molecular Dynamics code PARCAS 
!> (K. Nordlund et al). In addition to the standard inter-atomic forces, 
!> HELMOD also includes the effects of an external electric field on metal surface, 
!> including geometric field enhancement, induced surface charge, resistive heating 
!> due to field emission, etc. See the PhD thesis of S. Parviainen 
!> (http://urn.fi/URN:ISBN:978-952-10-8953-4) for some details.

!> @author Flyura Djurabekova, University of Helsinki
!> @author Kai Nordlund, University of Helsinki
!> @author Stefan Parviainen, University of Helsinki
!> @author Andreas Kyritsakis, University of Helsinki, Helsinki Institute of Physics.
!>
!> Module containing routines relevant for simulating metals surface under electric 
!> fields
!>                                                       
!> As this module is mostly used in connection with PARCAS, many of the data 
!> structures, variable names and units are taken directly from there. 
!> If some day this changes, it may make sense to give velocities and positions 
!> in SI units instead of parcas internal units. Especially velocities are tricky to
!> work with, because the units are velocity dependent (see Master's thesis of 
!> K. Nordlund for an explanation, the PARCAS README.DOCUMENTATION, or PARCAS 
!> src/mdx.f90).
!> 
!> Due to historical reasons some parts of the code check what kind of boundary 
!> conditions we have. It's actually unlikely the code would work with non-periodic
!> systems (in xy-plane) so this is probably unnecessary. Other parts just assume 
!> periodic boundaries.
!>
!> The documentation of this module is not great. Many subroutines use the same
!> arguments, in the same format, so if you find the documentation lacking for the
!> subroutine you are interested in, you could check another one, to see if it takes
!> the same argument, and if it is better documented there.
!> 
!> @todo: Coordinates, velocities are in the PARCAS format, e.g. in a 1D-array.
!> Would be nicer to have separate columns for x,y,z.

module helmod

use libfemocs, only: femocs

implicit none

private
public  :: handle_elfield, set_params, set_globs

! The reasons for these particular values have been lost in history
! MATERIAL was at some point -1

integer, parameter  :: dp = 8
!< Double precision No of bytes. dp = 4 if single precision is needed
integer, parameter  :: NEUMANN_BOUNDARY = -2 
!< Constant representing a Neumann boundary
integer, parameter  :: VACUUM = 1 
!< Constant representing free space
integer, parameter  :: MATERIAL = -10 
!< Constant representing a Dirichlet boundary
integer, parameter  :: EVAPORATED_POINT = -20 
!< Constant representing a point with an evaporated atom

integer, parameter  :: pbc(3) = [1, 1, 0] 
!< Periodic boundary conditions. It's unlikely anything but this would work

!> @brief HELMOD global parameters that never change. Mostly read from md.in
type parameters
    real(dp)    :: timerelE  = 10.d0
    !< Time to relax system before applying electric field (fs)
    real(dp)    :: elmigrat = 1.d0 !< Multiplier for electromigration effect
    real(dp)    :: Erate = -0.1 !< Rate for ramping el field. Unit depends on rampmode.
    real(dp)    :: target_applied_field = -0.1
    !< Magnitude of applied electric field once fully ramped (V/Å)
    integer     :: evpatype = 0 !< Atom type of evaporated atoms
    integer     :: nfullsolve = 1 
    !< Solve Laplace for full system every nfullsolve steps (timesteps)
    integer     :: aroundapx = 16 
    !< When not solving full system, solve in volume this far from apex (gridpoints)
    integer     :: rampmode = 0 !< How to ramp electric field
    integer     :: ncellp(3) = 2 !< Gridpoints per unit cell. Should be = 2
    ! If the ncellp is more than 3, only 1 GP around the CM of atom will not be 
    !enough to cover with -1

    integer     :: heatmode  = 0 !< The way in which heating is to be calculated
    !< 0: No heating is calculated
    !< 1: Temperature distribution is constant. It is a ramp with maxtemp at top
    !< 2: Full temperature calculation. heating module and getelec are called.
    integer     :: debug = 1 !< Debugging output level
    integer     :: EDmethod = 0 !< Which method to be used for solving Laplace eq.
                            !< 0 for FDM (classical Helmod), 1 for FEM (femocs)
    integer     :: charge_method = 0 !< Which method to be used for charges.
                            !< 0 for Helmod GPs, 1 for femocs)
    integer     :: Nsave = 10 !< Save debugging data every Nsave steps
    integer     :: tempmode = 1 !< Mode in which initial temperature is defined. 
    !< 0: Temperature found from MD velocities at every step.
    !< 1: Last temperature distribution used as the initial for the next time step.
    !< The initial temperature is set to the temp parameter (same as MD temperature).
    !< 2: Same as 1. The initial temperature distribution is read from the input
    !< file "in/tempinit.dat".
    real(dp)    :: bulktemp = 300. !< Bulk temperature. Same as "temp" in MD.
    integer     :: startheating = 0 !< Start heating when nsteps >= startheating
    real(dp)    :: Tchangelim = 1.d-2 
    !< Limit for considering temperature changed and recalculating heating.
    logical     :: use_dipoles = .false.
    !< Determine if dipole forces will be inserted into evaporated atoms
    logical     :: use_evaporation = .false.
    !< Determine if evaporation is to be taken into account
    real(dp)    :: maxtemp = 300. !< maximum temperature in case heatmode = 1 
    integer     :: fem_import_mode = 0
    !< The mode in which femocs will import the atoms. if 0, it imports parcas atoms
    !< if 1 it imports surface grid points.
    real(dp)    :: force_factor = .5d0 !< Force = Factor * charge * Field (1 or 0.5)
    integer     :: interp_method = 0 !< Mode of femocs interpolation around tip.
    !< 0: 3d in elements , 1: 2d in faces (faster)
    
end type parameters


!> @brief HELMOD global parameters that vary between calls
type globals
    real(dp)    :: time_fs = 0.d0 !< Current simulation time (fs)
    real(dp)    :: box(3) !< Simulation box size (Å)
    real(dp)    :: deltat_fs = 4.05 !< Length of timestep (fs)

    logical     :: movietime = .false. !< Will a movie frame be saved this step?

    integer     :: iabove = 5 !< How high above surface to calculate electric field 
                          !<(gridpoints)
    integer     :: ncells(3) !< Number of unit cells in the system 
                    !(for calculating the initial lattice constant)
    integer     :: isize(3) !< Size of the system
    integer     :: nsteps = 0 
    !< Number of time steps after HELMOD 1st call. nsteps = 1 at first call
    logical     :: firsttime = .true. !< If helmod is called for first time
    logical     :: skiplimcheck = .false. !< Whether to skip checking if the 
    !< boundary conditions have changed. (Used mainly for KMC simulations).
    
    real(dp), allocatable   :: timeunit(:) 
    !< Conversion factor between PARCAS time and fs.
    logical     :: gridprint = .true.
    !< Determines if the grid is going to be printed in this time step

end type globals


!> @brief All variables related to electrostatics and FDM solution of the Laplacian
type Electrostatics
    real(dp), allocatable   :: phi_sq(:, :, :) 
    !< Electrostatic potential on the square grid (V).
    real(dp), allocatable   :: Efield(:, :, :, :) 
    !< Electrostatic field on the grid. Th first 3 indices indicate the 
    !< coordinate on the grid, and the 4th index has the direction (Ex,Ey,Ez,|E|).
    !< E.g the magnitude of the field at point 3,4,5 (grid coordinates) is given by
    !< Efield(3, 4, 5, 4). All electric field units are V/Å.
    integer, allocatable    :: philimit(:, :, :) !< Boundary conditions for phi.
    integer, allocatable    :: philimit_sq(:, :, :) !< BC for phi_sq.
    
    real(dp), allocatable   :: charge(:,:,:) !< charge assigned to each GP
    
    real(dp)    :: current_applied_field !< Current anode applied field.
    real(dp)    :: prev_applied_field = 0.d0 !< Previous anode applied field.
    real(dp)    :: grid_spacing(3) !< The grid spacing (dx, dy, dz).
    real(dp)    :: grid_spacing_sq(3) !< The grid spacing of the square grid.
    
    logical     :: full_solve !< Whether to solve full system, or smaller system
                                !<given by aroundapx
    logical     :: applied_field_changed !< If applied field has changed.
    logical     :: philimchanged = .true. !< If boundary conditionss changed.
    logical     :: potchanged = .false.
    logical     :: valid_solution = .false.
    integer     :: xbounds(2), ybounds(2), zbounds(2) 
    !< Bounds of the grid where atoms can be found. zbounds(1) gives the top of the
    !< substrate plane surface and zbounds(2) the top of the tip.                                           
    integer     :: lastsolve !< Last timestep (globs%nsteps) that Laplace was solved.
    real(dp)    :: maxField = 0.d0 !< Maximum field found in all points (V/A)
    real(dp)    :: multiplier = 1.d0 !< Multiplier that will multiply all 
            !<electrostatic solutions to account for space charge or other effects.
    
end type Electrostatics 


type(parameters), save      :: pars !< Global parameters that never change
type(globals), save         :: globs !< Global parameters possible changed every step


contains


!> @brief Set HELMOD module parameters (read once from md.in)
subroutine set_params(timerelE, elmigrat, Erate, target_applied_field, evpatype, &
                    nfullsolve, aroundapx, rampmode, debug)
                                           
    use libfemocs, only: femocs

    real(dp), intent(in), optional  :: timerelE, elmigrat, Erate, &
                                        target_applied_field
    integer, intent(in), optional   :: evpatype, nfullsolve, aroundapx, &
                                        rampmode, debug
    
    type(femocs)            :: fem
    integer                 :: flag, use_dipoles, use_evaporation
    
    if (pars%debug > 0) print *, 'Entering set_params'

    pars%timerelE = timerelE
    pars%elmigrat = elmigrat
    pars%Erate = Erate
    pars%target_applied_field = target_applied_field
    pars%evpatype = evpatype
    pars%nfullsolve = nfullsolve
    pars%aroundapx = aroundapx
    pars%rampmode = rampmode
    pars%ncellp = 2
    
    !Reading parameters from md.in file using femocs. Nice and easy.

    if (pars%debug > 0) print *, 'Calling femocs for parameter reading'
    fem = femocs("in/md.in")
    
    call fem%parse_int(flag, 'edmethod', pars%EDmethod)
    call fem%parse_int(flag, 'nsave', pars%Nsave)
    call fem%parse_int(flag, 'tempmode', pars%tempmode)
    call fem%parse_double(flag, 'temp', pars%bulktemp)
    call fem%parse_int(flag, 'startheat', pars%startheating)
    call fem%parse_int(flag, 'dbg_lvl', pars%debug)
    call fem%parse_double(flag, 'temp_changelim', pars%tchangelim)
    call fem%parse_int(flag, 'use_dipoles', use_dipoles)
    if (flag == 0) pars%use_dipoles = use_dipoles /= 0
    call fem%parse_int(flag, 'use_evaporation', use_dipoles)
    if (flag == 0) pars%use_evaporation = use_evaporation /= 0
    call fem%parse_int(flag, 'heatmode', pars%heatmode)
    call fem%parse_double(flag, 'maxtemp', pars%maxtemp)
    call fem%parse_int(flag, 'fem_import_mode', pars%fem_import_mode)
    call fem%parse_int(flag, 'charge_method', pars%charge_method)
    call fem%parse_double(flag, 'force_factor', pars%force_factor)
    call fem%parse_int(flag, 'interp_method', pars%interp_method)
    
    call fem%delete()
    
    if (pars%debug > 0) print *, 'Exiting set_params'

end subroutine set_params


!> @brief Set HELMOD module global parameters (set from PARCAS every step)
subroutine set_globs(time_fs, box, ncells, deltat_fs, timeunit, &
                    movietime, iabove, skiplimcheck)
                    
    
    real(dp), intent(in)    :: time_fs, box(3), deltat_fs, timeunit(:)
    integer, intent(in)     :: iabove, ncells(3)
    logical, intent(in)     :: movietime
    logical, intent(in), optional   :: skiplimcheck
    
    globs%time_fs = time_fs
    globs%box = box
    globs%ncells = ncells
    globs%deltat_fs = deltat_fs
    if (.not. allocated(globs%timeunit)) allocate(globs%timeunit(size(timeunit)))
    globs%timeunit = timeunit
    globs%movietime = movietime
    globs%iabove = iabove
    
    globs%nsteps = globs%nsteps + 1 !increase time step
    
    if (present(skiplimcheck)) globs%skiplimcheck = skiplimcheck
    
end subroutine set_globs


!> @brief Handle CLIC related things in HELMOD
subroutine handle_elfield(x0, x1, myatoms, nborlist, xq, atype, &
                            Efield, rc, xnp, capplfield)

    !> Compute electric field, induced surface charge, and dynamic effects due to 
    !> field emission currents.
    !> NOTE: The bounds for x0, x1, atype, xnp MUST be given here.
    !> On the PARCAS side these are static arrays, so there may be many "unused" 
    !> elements.
    
    !> Puts atoms in grid points, calculates the electric field surrounding them and
    !> assigns the induced charges. The subroutine first determines the appropriate
    !> boundary conditions, then solves the electric field, and finally assigns the
    !> charges. For this purpose the subroutine will create an array containing atoms
    !> on the grid.
    !>
    !> The routine remembers the computed boundary conditions and the applied field,
    !> and only computes the electric field if either of these change.
    
    !> @p xq has the following format: it is a one dimensional array in a smiliar
    !> style as parcas x0. First element contains total charge in unit charges, 
    !> followed by the Lorentz force (eV/Å). The force components are used in Qforces 
    !> to calculate the total force on atoms.


    integer, intent(in)     :: myatoms !< Number of atoms
    real(dp), intent(inout) :: x0(1:myatoms * 3) 
    !< Atom coordinates [x1, y1, z1, x2, y2, z2, ...] (parcas units)

    real(dp), intent(inout) :: x1(1:myatoms*3) 
    !< Atom velocities [vx1, vy1, vz1, vx2, vy2, vz2, ...] (parcas units)
    real(dp), intent(inout) :: xnp(1:myatoms*3) 
    !< Force acting on atoms (parcas units)
    integer, intent(in)     :: nborlist(:) 
    !< Neighbor list from parcas. Will be used to import atoms to femocs.
    
    real(dp), allocatable, intent(inout) :: Efield(:, :, :, :) 
    !< Electric field (V/Å)
    real(dp), intent(inout) :: xq(1:myatoms*4) 
    !< Charge on atoms (e) and forces (eV/Å)
    integer, intent(inout)  :: atype(1:myatoms) !< Atom types
    
    real(dp), intent(out)   :: rc(3) !< Coordinates of corner for Efield. 
    !Used for converting grid point to actual coordinate (parcas units)
    real(dp), intent(out)   :: capplfield !< Current applied field. 

    integer, allocatable, save  :: atoms(:, :, :), & ! Atom ids on a grid
                    prev_philimit(:, :, :) ! Boundary conditions from previous step
    
    real(dp), allocatable, save :: xT(:), tot_charge
    
    type(Electrostatics), save  :: els
    type(femocs), save          :: fem
    
    integer, save   :: highest_atom_height_above_surf = 0 
    ! Layer where highest atom is, relative to surface_height
    integer, save   :: surface_height = 7   ! Layer where the flat surface is
    integer, save   :: maximum_atom_height  
    ! Maximum height of atomic system (excluding vacuum). This can change 
    integer, save   :: highest_atom_height, im(2), jm(2)

    integer :: i, fid = 654321, flag, i3, iatom
    
    character(len = 100)    :: gridfile, mdfile, chargefile
    
    real(dp), dimension(myatoms)    :: Ex, Ey, Ez, Enorm


    if (pars%debug > 0) print *, 'Entering handle_efield'
    
    call elfield_at_time(els) ! Get the current value of the applied electric field
    if (abs(els%current_applied_field) < 1.e-5) then
        print * , '#No field calculation. Eappl = ', els%current_applied_field
        return
    endif
    
    
    !******************************************************************************
    !Allocation of variables and preparation section
    if(globs%firsttime) then !initialization and allocation of variables
        print *, "---First time. Allocating arrays..."
        globs%isize = globs%ncells * pars%ncellp
        maximum_atom_height = globs%isize(3) + 20 
        ! System height should not grow by more than this many layers

        if(pars%debug > 0) then
            print *, 'iSize:', globs%isize, 'iabove: ', &
                                globs%iabove, 'maxatomhight:', maximum_atom_height
            call print_params()
        endif

        allocate(atoms(globs%isize(1), globs%isize(2), &
                            maximum_atom_height + globs%iabove))

        allocate(els%Efield(globs%isize(1), globs%isize(2), globs%iabove, 4))
        allocate(els%charge(globs%isize(1), globs%isize(2), globs%iabove))
        allocate(els%phi_sq(globs%isize(1), globs%isize(2), globs%iabove))
        els%Efield = 0.d0
        els%charge = 0.d0
        els%phi_sq = 0.d0
        allocate(els%philimit(globs%isize(1), globs%isize(2), &
                maximum_atom_height + globs%iabove))
        els%philimit = 0
        
        allocate(xT(size(x0)))

        allocate(prev_philimit(size(els%philimit, 1), size(els%philimit, 2), &
                size(els%philimit, 3)))
        prev_philimit = 0
        if (pars%debug > 0) &
            print *, 'globs%box = ', globs%box
    endif
    
    els%grid_spacing = globs%box / globs%isize !Initial estimate
    
    if (globs%iabove < 0) globs%iabove = globs%isize(3) + 1
    ! A good guess for how high up to calculate efield if nothing else is given 
    
    
    if (pars%debug > 0) print *, "setting nfullsolve.. globs%nsteps = ", globs%nsteps
    els%full_solve = mod(globs%nsteps - 1, pars%nfullsolve) == 0 
    ! Subtract 1 to get a full solve on the first step
    
    
    !******************************************************************************
    !Setup boundary conditions section
    
    atoms = -1 ! initalize all GPs with no atoms inside
    print *, '--- Setting up boundary conditions...'
    call setup_boundary_conditions(x0, xq, atype, els%philimit, atoms, &
            highest_atom_height_above_surf, surface_height, im, jm)     
    
    ! Check if any boundary point has changed. We can limit the check to between
    ! im(1), im(2) etc, since no atoms should be outside these ranges.
    els%philimchanged = any( &
        els%philimit(im(1):im(2), jm(1):jm(2), :highest_atom_height + 1) /= &
        prev_philimit(im(1):im(2), jm(1):jm(2), :highest_atom_height + 1) )
    if(pars%debug > 0 .and. els%philimchanged) print *, "philimit changed"
    prev_philimit = els%philimit
    
    highest_atom_height = highest_atom_height_above_surf + surface_height
    els%xbounds = im ![max(1,im(1)-1),  min(im(2)+1,globs%isize(1))]
    els%ybounds = jm ![max(1,jm(1)-1),  min(jm(2)+1,globs%isize(2))]
    els%zbounds = [surface_height, highest_atom_height]
    
    print *, '--- Cleaning boundary conditions...'
    call clean_philim(els, atoms)
    
    
    !*******************************************************************************
    !Calculating electric field section
    els%philimchanged = .true.      
    if (els%philimchanged .or. els%applied_field_changed) then 
        ! The lattice constant can change due to stretching by the field
        print *, '--- Calculating grid spacing...'
        call calc_grid_spacing(x0, els%zbounds(2) - 4, els%grid_spacing) 
        
        print *, '--- Calculating the Electric field...'
        if (pars%EDmethod == 0) then
            call fdm_solve_field(els)
        elseif (pars%EDmethod == 1) then
            call fem_solve_field(els, fem, x0, nborlist)
        else
            stop 'Wrong ED method input'
        endif
        print *, 'Field calculated. Fmax = ', els%maxField * 10.d0
        els%potchanged = .true.
    else
        els%potchanged = .false.
        print *, "#Field and charges not recalculated."
    endif
    
    ! Calculate where grid starts,needed when calculating ion trajectories in vacuum.
    ! This information is not needed in Helmod. The coordinates are in PARCAS units.
    rc(1:2) = -0.5
    !> The -1 below is needed because actually one layer is missing already from 
    !> philimit etc. @todo Fix this
    rc(3) = -0.5 + (els%zbounds(1) - 1) * els%grid_spacing(3) / globs%box(3)
    
    ! output the Efield. it is going to be needed in mdx.f90
    if (globs%firsttime) allocate(Efield(size(els%Efield,1), size(els%Efield,2), &
                                    size(els%Efield,3), size(els%Efield,4))) 
    Efield = els%Efield
    capplfield = els%current_applied_field
    els%prev_applied_field = els%current_applied_field
    
    if (pars%debug > 0) then
        print '("xbounds: ",2I5,/"ybouns:", 2I5,/"zbounds:", 2I5)', &
                els%xbounds, els%ybounds, els%zbounds
        print '("grid spacing:", 3F15.5)', els%grid_spacing
    endif
    
    if (.not. els%valid_solution) then  !valid solution does not exist! return
        print *, 'ERROR: helmod does not have a valid field solution. returning'
        globs%firsttime = .false.
        xq = 0.d0
        return
    endif
    
    
    !*******************************************************************************
    !Calculating current effects - heating section
    
    print *, '---Calling current effects...'
    
    if(pars%heatmode > 0 .and. globs%nsteps >= pars%startheating) &
        call current_effects(els, fem, x1, xT, atoms(els%xbounds(1):els%xbounds(2), &
                els%ybounds(1) : els%ybounds(2), els%zbounds(1) : els%zbounds(2)))
    
    
    !*******************************************************************************
    !Calculating charges and forces section
    
    print *, '---Calculating charges and forces...'

    if (els%potchanged) then 
        if (pars%EDmethod == 0 .or. pars%charge_method == 0) then
            call charge_gp(els)
            call charges_new(els, atoms, xq)
        else
            call fem%export_data(flag, xq, size(atype), "charge_force")
        endif
        tot_charge = 0.d0
        do i = 1,myatoms
            tot_charge = tot_charge + xq((i-1) * 4 + 1)
        enddo
        print *, 'Calculated charges. Total charge =', tot_charge
    endif
        
    !Check if something is wrong with the charges
    if ( els%potchanged ) then
        if (any(abs(xq) > 1.d3 .or. isnan(xq)) ) then
            call error_catch(els, x0, x1, xT, xq, atoms, atype,'Too high charge/force')
            stop
        endif
    endif
    
    call remove_evaps(fem, x0, x1, atype, xq)
    
    !*******************************************************************************
    !Dipole forces and printing data

    if (pars%use_dipoles) call insert_dipforce(els, fem, x0, xnp, atype)
    
    !*******************************************************************************
    !print output and debugging data
    
    if (pars%debug > 0 .or. globs%nsteps <= 1) then
        write(mdfile, '("out/partsgrid_",i5.5,".xyz")') globs%nsteps
        print *, 'writing particles and their grid info to ', trim(mdfile)
        call print_particles(els, x0, x1, xT, xq, atoms, atype, trim(mdfile), 1)
    endif

    if (pars%debug > 1 .or. globs%nsteps <= 1) then
        if (pars%EDmethod == 0) then
            print *, 'Total charge: ', sum([(xq(i), i=1,size(xq),4)]) &
                    /(5.5268d-03 * product(globs%box(1:2)) * capplfield) , &
                     '*eps_0 * E_anode * Area'
        endif
        write(gridfile, '("out/grid_",i5.5,".xyz")') globs%nsteps
        call print_grid(els, atoms, trim(gridfile))
        globs%gridprint = .false.
    endif
                                    
    if (mod(globs%nsteps, pars%Nsave) == 0 .or. globs%nsteps <= 1) then
        write(mdfile, '("out/particles_",i5.5,".xyz")') globs%nsteps
        print *, 'writing particles and their info to ', trim(mdfile)
        call print_particles(els, x0, x1, xT, xq, atoms, atype, trim(mdfile))
        print *, 'particles written successfully'
    endif
    
    !*******************************************************************************
    globs%firsttime = .false.
    
    
    if (pars%debug > 0) print *, 'Exiting handle_elfield'
    
end subroutine handle_elfield


!> @brief Compute electric field and assign charges to atoms
subroutine fdm_solve_field(els)
    
    !> This function should be used when you already have atoms assigned to a grid, 
    !> and also have the boundary conditions in theappropriate form. In case you only
    !> have atom coordinates, you should use chage_and_field_from_atoms() instead.
    !>
    !> The solution of the electrostatic potential is remembered, and used as the 
    !> initial  guess for the next call. The value of the previously used applied 
    !> field is also remembered. If it is different thant the current one, the ratio
    !> is used to scale the initial guess.
    
    type(Electrostatics), intent(inout) :: els
    !< Electrostatics data structure to be modified 

    real(dp), allocatable, save :: Efield_sq(:, :, :, :)              
    ! Electrostatic field on a square grid (intermediate variable used only here.)
    
    integer                 :: highest_atom_height_above_surf, k
    integer                 :: smaller_size(3)              
    ! Size of volume to solve when not solving the full system
    integer                 :: smaller_origin(3)
    ! Origin of the volume when not solving the full system
    logical, save           :: firsttime = .true.

    ! Make grid square to avoid numeric effects
    ! This is important for e.g. 110 surfaces, because otherwise different grid
    ! spacings are used in different directions, leading to numerically different
    ! results, even though otherwise the system is symmetric
    
    if (pars%debug > 0) print *, 'Entering fdm_solve_field'
    
    els%grid_spacing_sq = [max(els%grid_spacing(1), els%grid_spacing(2)), &
                        max(els%grid_spacing(1), els%grid_spacing(2)), &
                        els%grid_spacing(3)] ! NOTE: Only square in xy-plane
                        
    call limgridsq(els%philimit, els%grid_spacing, els%philimit_sq, els%grid_spacing_sq) 
    ! Get a square grid for philim

    if(firsttime) then
!        allocate(els%phi_sq(size(els%philimit_sq, 1), size(els%philimit_sq, 2), &
!                                                            size(els%Efield, 3)))
        allocate(Efield_sq(size(els%philimit_sq, 1), size(els%philimit_sq, 2), &
                                                            size(els%Efield, 3), 4))
        Efield_sq = 0

        if(pars%debug > 0) then
            print *, "phisize: ", shape(els%phi_sq)
            print *, "Setting initial gradient guess for electric field"
        end if

        els%phi_sq(:, :, 1:2) = 0 ! assume the linear increase of phi with the gradient
        do k = 3, size(els%phi_sq, 3)
            els%phi_sq(:, :, k) = - (els%current_applied_field * (k - 2) * &
                                els%grid_spacing_sq(3)) 
            ! assume the linear increase of phi with the gradient
        end do
        firsttime = .false.
    end if

    ! Only need to solve field if surface shape or applied field has changed
    !> @todo Move scale_potential call into solve_field
    call scale_potential(els%phi_sq, els%current_applied_field, els%prev_applied_field)

    call solve_field(els%current_applied_field, &
            els%philimit_sq(:, :, els%zbounds(1):els%zbounds(1) + globs%iabove - 1),&
            els%phi_sq, Efield_sq, els%grid_spacing_sq, els%full_solve, &
            smaller_size, smaller_origin)

    call egridunsq(Efield_sq, els%grid_spacing_sq, els%Efield, els%grid_spacing) 
    ! Get the electric field back to the original grid
                        
    els%lastsolve = globs%nsteps
    els%valid_solution = .true.
    
    els%maxField = maxval(els%Efield(:,:,:,4))

    if (pars%debug > 0) print *, 'Exiting fdm_solve_field'
    
end subroutine fdm_solve_field


!> @brief Calculate electric field from the electrostatic potential
subroutine Efield_from_potential(phi, grid_spacing, Efield, &
                                smaller_size, smaller_origin)

    !> Compute the electric field from the given electrostatic potential, 
    !> in the given volume

    ! Input and output variables
    real(dp), intent(in) :: phi(:, :, :) !< Electrostatic potential
    integer, intent(in) :: smaller_size(3) !< Size of the solved system (gridpoints)
    integer, intent(in) :: smaller_origin(3) !< Origin of the solved system (gridpoints)
    real(dp), intent(in) :: grid_spacing(3) !< Grid spacing (Å)

    real(dp), intent(out) :: Efield(:, :, :, :) !< Electric field (V/Å)
      
    integer :: i, j, k, ilim1, ilim2, jlim1, jlim2

    real(dp) :: Emax, Emin, phimax, phimin

    phimin = 1d30; phimax = -1d30;
    Emin = 1d30; Emax = -1d30;

    ! Calculate numerical derivative to estimate E = - Nabla phi
    ! Hackish: Only handle periodics when needed
    ! Better would be to always handle periodics separatly
    if(all(smaller_origin == 1)) then
      if(pars%debug > 1) print *, "Full solution", smaller_origin, smaller_size, shape(phi)
      do k = 2, size(phi, 3) - 2 ! Top layers contain constant boundary and don't need to be calculated
      do j = 1, size(phi, 2)
      do i = 1, size(phi, 1)
        if (phi(i, j, k) /= 0) then
          ! Periodics in x
          ilim1 = i - 1; if (ilim1 == 0) ilim1 = size(phi, 1);
          ilim2 = i + 1; if (ilim2 == size(phi, 1) + 1) ilim2 = 1
          ! Periodics in y
          jlim1 = j - 1; if (jlim1 == 0) jlim1 = size(phi, 2);
          jlim2 = j + 1; if (jlim2 == size(phi, 2) + 1) jlim2 = 1
          ! Parabolic numerical derivative for even interval 
          Efield(i, j, k, 1) = 0.5d0*(phi(ilim1, j, k) - phi(ilim2, j, k))/grid_spacing(1);
          Efield(i, j, k, 2) = 0.5d0*(phi(i, jlim1, k) - phi(i, jlim2, k))/grid_spacing(2);
          ! No periodics needed in z
          Efield(i, j, k, 3) = 0.5d0*(phi(i, j, k - 1) - phi(i, j, k + 1))/grid_spacing(3);
        else
          Efield(i, j, k, 1) = 0.0d0; Efield(i, j, k, 2) = 0.0d0; Efield(i, j, k, 3) = 0.0d0;
        endif

        ! Calculate |E| and store it in fourth column
        Efield(i, j, k, 4) = sqrt(Efield(i, j, k, 1)*Efield(i, j, k, 1) + Efield(i, j, k, 2)*Efield(i, j, k, 2) +      &
                   Efield(i, j, k, 3)*Efield(i, j, k, 3))

        !if (philimit(i, j, k) == VACUUM) then
        if (phi(i, j, k) /= 0) then
          if (Efield(i, j, k, 4) < Emin) Emin = Efield(i, j, k, 4)
          if (Efield(i, j, k, 4) > Emax) then
            Emax = Efield(i, j, k, 4)
          end if
          if (phi(i, j, k) < phimin) phimin = phi(i, j, k)
          if (phi(i, j, k) > phimax) phimax = phi(i, j, k)
        endif
      enddo
      enddo
      enddo
    else
      if(pars%debug > 1) print *, "partial solution", smaller_origin, smaller_size
      do k = smaller_origin(3), smaller_origin(3) + smaller_size(3) - 1
      do j = smaller_origin(2), smaller_origin(2) + smaller_size(2) - 1
      do i = smaller_origin(1), smaller_origin(1) + smaller_size(1) - 1
        if (phi(i, j, k) /= 0) then
          ! Periodics in x
          ilim1 = i - 1;
          ilim2 = i + 1;
          ! Periodics in y
          jlim1 = j - 1;
          jlim2 = j + 1;
          ! Parabolic numerical derivative for even interval 
          Efield(i, j, k, 1) = 0.5d0*(phi(ilim1, j, k) - phi(ilim2, j, k))/grid_spacing(1);
          Efield(i, j, k, 2) = 0.5d0*(phi(i, jlim1, k) - phi(i, jlim2, k))/grid_spacing(2);
          ! No periodics needed in z
          Efield(i, j, k, 3) = 0.5d0*(phi(i, j, k - 1) - phi(i, j, k + 1))/grid_spacing(3);

          ! Calculate |E| and store it in fourth column
          Efield(i, j, k, 4) = sqrt(Efield(i, j, k, 1)*Efield(i, j, k, 1) + Efield(i, j, k, 2)*Efield(i, j, k, 2) +      &
                     Efield(i, j, k, 3)*Efield(i, j, k, 3))
        else
          Efield(i, j, k, :) = 0
        endif
      enddo
      enddo
      enddo
    end if

    if(pars%debug > 1) write (6, '(A, 2G12.5, A, 2G12.5)') "|E| min max", Emin, Emax, " phi min max", phimin, phimax

    call flush(16)

    return
end subroutine Efield_from_potential


!> @brief Calculate current and heating effects on a nanotip.
subroutine current_effects(els, fem, x1, xT, atoms)

    !> This function connects HELMOD to heating and GETELEC modules and 
    !> calculates the heating deposited due to Joule and Nottingham effects. The 
    !> heat equation is then solved and the resulting electronic temperature 
    !> distribution is found. The lattice velocities are then scaled according to
    !> Beredsen thermostat to be in equilibrium with the electronic temperature.
    
    use heating, only: Potential, poten_create, space_charge, HeatData, &
        scale_vels, set_heatparams, Nsteps, prepare_heat, plot_temp, get_temp_md, &
        heateq, print_heat, update_heat_data, solve_heat_long 
    use std_mat, only: linspace
    
    
    type(Electrostatics), intent(inout) :: els
    !< Electrostatics data structure input 
    type(femocs), intent(in)            :: fem  
    !< Finite elements object to be created-modified.  
    integer, intent(in)     :: atoms(:, :, 1:) !< Atoms placed on a grid
    real(dp), intent(inout) :: x1(:) !< Atom velocities [vx1, vy1, vz1, vx2, ...].
    !< Out: adjusted for temperature (parcas units)
    real(dp), intent(out)   :: xT(:) !< Temperature on atoms(K)
    !< Used only for visualization printing into file

    integer, parameter      :: tempfid = 2318968
    type(Potential), save   :: poten
    type(HeatData), save    :: heat
    
    real(dp), save          :: Tchange = 0.d0 !rms change of temperature in the step
    integer, save           :: Ncalls = 0, tipheight = 0
    
    real(dp)                :: delta
    ! delta: Helper for scaling velocities
    integer                 :: i, k, Nh !loop indices, height of the Efield vector
    logical                 :: recalc = .true. !If tipbounds and heat deposition 
                                                !is to be recalculated
    
    if (pars%debug > 0) print *, 'Entering heating'
    
    Nsteps = globs%nsteps !passing Ncalls to heating module
    delta = globs%deltat_fs/globs%timeunit(1)
    
    !if first time set params
    if (Ncalls == 0) call set_heatparams(filename = 'in/md.in', debug = pars%debug, &
                                    EDmethod = pars%EDmethod, Nsavedata = pars%Nsave)
                                    
    
    recalc = Tchange > pars%Tchangelim .or. Ncalls == 0 .or. &
            els%lastsolve == globs%nsteps .or. els%zbounds(2) /= tipheight 
    !if something changed or first time, prepare heating and allocate memory
    if (recalc) then
        if (pars%EDmethod == 0) then !prepare interpolation with square grid 
            call poten_create(poten, els%philimit_sq(:, :, els%zbounds(1) : &
                size(els%phi_sq, 3) + els%zbounds(1)-1), els%grid_spacing_sq, &
                els%phi_sq)
        else   ! pass philimit and proper grid spacing. Efield is dummy in this case.
            call poten_create(poten, els%philimit(:, :, &
                els%zbounds(1):els%zbounds(1) + globs%iabove - 1), &
                els%grid_spacing, Efield = els%Efield)
        endif
        call prepare_heat(heat, poten, els%zbounds(2) - els%zbounds(1) + 1)
        tipheight = els%zbounds(2)
    endif
        
    if (pars%debug > 0) print *, 'tipbounds:', heat%tipbounds(1:2), &
                        'zbounds', els%zbounds
    
    call get_temp_md(atoms, x1, heat, delta, globs%box)
                        
    if (pars%heatmode == 1) then!Ramp temp is assumed. Just scale velocities
        if (Ncalls == 0) then
            xT = heat%Tbound
            heat%tempfinal(heat%tipbounds(0):heat%tipbounds(2)) = &
                            linspace(pars%bulktemp, pars%maxtemp, &
                            1 + heat%tipbounds(2) - heat%tipbounds(0))
        endif
        xT = heat%Tbound
        call scale_vels(atoms, x1, xT, heat, globs%deltat_fs, delta, globs%box)
        Ncalls = Ncalls + 1
        return
    endif
    
    
    ! prepare the input initial temperature
    if (pars%tempmode == 0) then! Calculate heat distribution from parcas velocities
        ! A better distribution might be obtained by first smoothing the data a bit
        heat%tempinit(heat%tipbounds(1)) = .5 * ( heat%temp_md(heat%tipbounds(1)) + &
                                           heat%temp_md(heat%tipbounds(1) + 1) )
        heat%tempinit(heat%tipbounds(1)) = .5 * ( heat%temp_md(heat%tipbounds(1)) + &
                                           heat%temp_md(heat%tipbounds(1) + 1) )
        do k = heat%tipbounds(1) + 1, heat%tipbounds(2) - 1
            heat%tempinit(k) = .25 * heat%temp_md(k-1) + .5 * heat%temp_md(k) + &
                                .25 * heat%temp_md(k+1)
        enddo
    elseif (Ncalls > 1) then !get temp distribution from previous heating output
        heat%tempinit = heat%tempfinal
    elseif (pars%tempmode == 1) then ! initial temperature with bulktemp parameter
        heat%tempinit = pars%bulktemp
    elseif (pars%tempmode == 2) then !get temp distribution from file
        open(tempfid, file = 'in/tempinit.dat', action = 'read')
        read(tempfid, *) heat%tempinit(heat%tipbounds(0):heat%tipbounds(2))
        if (pars%debug > 0) then
            print *, 'read initial temperature from file. tempinit = '
            print *, heat%tempinit(heat%tipbounds(0):heat%tipbounds(2))
        endif
        close(tempfid)
        heat%tempinit(heat%tipbounds(2) + 1 : ) = heat%tempinit(heat%tipbounds(2))
    else
        stop 'Error: wrong tempmode'
    endif
    
    if (pars%debug > 0 .and. Ncalls == 0) then
            print *, 'Starting with initial temperature:'
            print *, heat%tempinit(heat%tipbounds(1): heat%tipbounds(2))
    endif
    
    if (recalc) then
        heat%Tbound = pars%bulktemp
        heat%hpold = heat%hpower                                            
        call space_charge(heat, poten, fem)
        els%multiplier = poten%multiplier
        Tchange = 0.d0
    else
        print *, '#Deposited heating not recalculated.'
    endif
    
    
    if (Ncalls == 0 .or. mod(globs%nsteps, 6 * pars%Nsave) == 0) then 
        call solve_heat_long(heat, poten, fem)
    endif

    heat%dt = globs%deltat_fs * 0.01
    
    call update_heat_data(heat)
    call heateq(heat, 100)
    
    print '(A15,ES13.5,A10)', 'Tfinal @top =', heat%tempfinal(heat%tipbounds(2)), 'K'  

    ! scale the velocities
    call scale_vels(atoms, x1, xT, heat, globs%deltat_fs, delta, globs%box)
    
    ! check if temperature has changed significantly
    Tchange = Tchange+norm2(((heat%tempfinal(heat%tipbounds(1): heat%tipbounds(2))) &
            - heat%tempinit(heat%tipbounds(1): heat%tipbounds(2))) &
             / heat%tempinit(heat%tipbounds(1): heat%tipbounds(2))) &
             / (heat%tipbounds(2) - heat%tipbounds(1) + 1)

    if (pars%Nsave /= 0 .and. mod(globs%nsteps, pars%Nsave)==0 .and. Ncalls/=0) then
        if (pars%debug > 0) call plot_temp(heat)
        call print_heat(heat)
    endif

    Ncalls = Ncalls + 1
    
    if (pars%debug > 0) print *, 'Exiting heating'

end subroutine current_effects


!> @brief Get the electric field at a specifc time
subroutine elfield_at_time(els)

    !> Subroutine that calculates the actual applied electric field at a given time,
    !> accounting for ramping. Would be a pure function except for stopping if
    !> rampmode is unknown. It updates applied_field_changed as well.
    
    integer, parameter  :: RAMP_NO = 0, RAMP_LINEAR_FIELD = 1, RAMP_LINEAR_STRESS = 2 
    !< Constant indicating ramping mode: 
    !< RAMP_NO: No ramping, RAMP_LINEAR_FIELD: electric field ramped linearly 
    !< RAMP_LINEAR_STRESS   surface stress is increased linearly (efield square root)

    type(Electrostatics), intent(inout) :: els
    
    integer, parameter  :: nrampevery = 3 ! How often to update the field
    ! Significant speedup if field is only updated every few steps
    real(dp)            :: time_with_elfield
    
    


    time_with_elfield = max(globs%time_fs - pars%timerelE, 0d0)
    if (pars%debug > 0) then
        print *, 'In elfield_at_time. time_with_elfield = ', &
        time_with_elfield
        print *, 'time_fs = ', globs%time_fs, 'rampmode = ', pars%rampmode
    endif
    
    ! Check how to ramp the field, linear with field, force or not at all
    if(pars%rampmode == RAMP_NO) then
        els%current_applied_field = pars%target_applied_field
    else
        if (mod(globs%nsteps, nrampevery) == 1) then 
        ! Only ramp field every N steps to speed things up (larger steps at a time!)
        ! Pick one of these
            if (pars%rampmode == RAMP_LINEAR_FIELD) then
                els%current_applied_field = time_with_elfield*pars%Erate 
                ! Linear ramping of field
            elseif (pars%rampmode == RAMP_LINEAR_STRESS) then
                els%current_applied_field = sqrt(time_with_elfield)*pars%Erate 
            ! Linear ramping of force
            else
                print *, "Unknow ramping mode", pars%rampmode
                stop
            end if
        else
            els%current_applied_field = els%prev_applied_field
        end if
    end if

    if (abs(els%current_applied_field) > abs(pars%target_applied_field)) &
        els%current_applied_field = pars%target_applied_field ! Don't go too high
    els%current_applied_field = sign(els%current_applied_field, &
                                                        pars%target_applied_field) 
    ! Always use the correct sign even if rate is wrong way around

    els%applied_field_changed = (els%current_applied_field /= els%prev_applied_field)
    
end subroutine elfield_at_time


!> @brief Scale electrostatic potential
subroutine scale_potential(phi, current_applied_field, prev_applied_field)

    !> Subroutine for scaling the electrostatic potential when e.g. ramping the 
    !> electric field. The potential depends linearly on the applied field, 
    !> so simply scaling the potential gives the same result as solving the 
    !> potential with the new applied field.

    real(dp), intent(in)    :: current_applied_field !< The new applied field (V/Å)
    real(dp), intent(in)    :: prev_applied_field 
    !< The applied field at the previous step (V/Å)
    real(dp), intent(inout) :: phi(:,:,:) 
    !< The electrostatic potential. In: initial potential, out: scaled potential (V)

    if(abs(prev_applied_field) > 0) then
        phi = phi*current_applied_field/prev_applied_field
        if(pars%debug > 0) &
            print *, "Eratio", current_applied_field/prev_applied_field
    end if
    
end subroutine scale_potential


!> @brief Solve electric field
subroutine solve_field(current_applied_field, philimit, phi, Efield, &
                    grid_spacing, full_solve, smaller_size, smaller_origin)
        
    !> Solve the electric field by first calculating the electrostatic potential,
    !> and then the electric field from that

    real(dp), intent(in) :: current_applied_field !< Applied electric field (V/Å)
    integer, intent(in) :: philimit(:, :, :) !< Boundary conditions
    real(dp), intent(in) :: grid_spacing(3) !< Grid spacing (Å)
    logical, intent(in) :: full_solve 
    !< Solve full system despite it not being time for it yet

    real(dp), intent(inout) :: phi(:, :, :) 
    !< Electrostatic potential. In: potential at previous step, out: potential now (V)

    integer, intent(out) :: smaller_size(3) !< Size of the solved system (gridpoints)
    integer, intent(out) :: smaller_origin(3) 
    !< Origin of the solved system (gridpoints)
    real(dp), intent(out) :: Efield(:, :, :, :) !< Electric field (V/Å)

    real :: t1, t2

    !call print_boundary(philimit,grid_spacing)

    call cpu_time(t1)

    ! Solve the whole system every N steps, or when entire field has changed
    call solve_potential(current_applied_field, philimit, phi, grid_spacing, &
      full_solve, smaller_size, smaller_origin)
    call cpu_time(t2)

    if (pars%debug > 0) print *, "Efield solver took", t2 - t1

    !call print_phi(phi,grid_spacing)

    ! Calculate the electric field from the electrostatic potential
    call Efield_from_potential(phi, grid_spacing, Efield, &
                                smaller_size, smaller_origin)
    
end subroutine solve_field


!> @brief Solve electrostatic potential
subroutine solve_potential(current_applied_field, philimit, phi, grid_spacing, &
                            full_solve, smaller_size, smaller_origin)

    !> Solve the electrostatic potential using the multigrid solver. Depending on 
    !> the timestep either the full system is solved, or only a small subsystem.                        
                            
    use multigridmod, only: multigrid

    real(dp), intent(in) :: current_applied_field !< Applied electric field (V/Å)
    logical, intent(in) :: full_solve 
    !< Solve full system despite it not being time for it yet 
    integer, intent(in) ::  philimit(:, :, :) !< Boundary conditions
    real(dp), intent(in) :: grid_spacing(3) !< Grid spacing (Å)

    real(dp), intent(inout) :: phi(:, :, :) !< Electrostatic potential (V)

    integer, intent(out) :: smaller_size(3) !< Size of the volume that was solved
    integer, intent(out) ::  smaller_origin(3) !< Origin of the volume that was solved

    integer :: partsolve ! Do we have a partial solution or a full solution

    if(size(philimit) /= size(phi)) then
      print *, "Shape of philimit is different than phi", shape(philimit), shape(phi)
      stop
    end if

    if(full_solve) then
        partsolve = 0
        smaller_size = shape(phi)
        smaller_origin = 1
    else
        partsolve = 1
        smaller_size = pars%aroundapx

        smaller_origin(1) = size(phi, 1)/2 - smaller_size(1)/2 + 1
        smaller_origin(2) = size(phi, 2)/2 - smaller_size(2)/2 + 1
        smaller_origin(3) = size(phi, 3)/2 - smaller_size(3)/2 + 1

        if(pars%debug > 1) print *, "Solving partial potential!", smaller_size, &
                            smaller_origin, smaller_origin(1) + smaller_size(1) - 1
        if(smaller_origin(3) + smaller_size(3) > size(phi, 3) .or. &
            smaller_origin(3) < 1) then
            print *, "Not enough space around apex", pars%aroundapx, &
                smaller_origin(3) + smaller_size(3), smaller_origin(3)
            stop
        end if
    end if

    call multigrid(phi(smaller_origin(1):smaller_origin(1) + smaller_size(1) - 1, &
                smaller_origin(2):smaller_origin(2) + smaller_size(2) - 1, &
                smaller_origin(3):smaller_origin(3) + smaller_size(3) - 1), &
                philimit(smaller_origin(1):smaller_origin(1) + smaller_size(1) - 1, &
                smaller_origin(2):smaller_origin(2) + smaller_size(2) - 1, &
                smaller_origin(3):smaller_origin(3) + smaller_size(3) - 1), &
                current_applied_field, grid_spacing, pars%debug > 1, partsolve)
                
end subroutine solve_potential


!> @brief Determine the boundary condition for the electric field solver. 
subroutine setup_boundary_conditions(x0, xq, atype, philimit, atoms, &
                            highest_atom_height_above_surf, surface_height, im, jm)

    !> As a "side effect" also places atoms on a grid which is useful in many 
    !> situations.
    !>
    !> Put the boundary conditions corresponding to a set of atom coordinates. 
    !> Also handles evaporating atoms that are detached from the surface.
    !> 
    !> @todo Consider putting making boundary conditions and atoms on a grid in 
    !> separate subroutines. If the user already has atoms on a grid 
    !> (from e.g. a lattice MC code) the coordinate conversion in unnecessary.
    !> One way to do this is to make x0 optional, and add atoms as another optional 
    !> argument to the subroutine. If atoms is supplied, skip coordinate conversion.

    real(dp), intent(in)    :: x0(:) !< Position of atoms (parcas units)
    
    real(dp), intent(inout) :: xq(:) !< Atom charge (e) and forces (eV/Å)
    integer, intent(inout)  :: atype(:) 
    !< Atom types. Modified if an atom is evaporated
    integer, intent(inout)  :: philimit(:, :, 1:) 
    !< Boundary conditions (see below)
    integer, intent(inout)  :: atoms(:, :, 1:)
    !< Atom coordinates on a grid
    integer, intent(inout)  :: im(2), jm(2)
    !< Store between which gridpoints atoms were last seen 
    !<(no need to scan over the full grid always)

    integer, intent(inout)  :: surface_height                       
    !< Height of the surface from the bottom of the entire system (gridpoints)
    integer, intent(out)    :: highest_atom_height_above_surf       
    !< Height above surface of the highest non-evaporated atom (gridpoint)
    ! At which layer to start looking for surface
    ! Needed if e.g. voids are inserted below the surface
    
    integer, save           :: highest_atom_height, minimum_surface_height = 1  
    ! Height above surface of the highest "connected" atom, i.e. one that has no
    ! free space between it and the one below

    ! Local variables
    integer, allocatable, save  :: prephilimit(:, :, :) 
    !An initial verison of philimit, 
    !which is used to determine the actual values later
    
    logical, save               :: firsttime = .true.
    logical                     :: invac

    integer :: ii, jj, kk, i_ii, j_jj, k_kk, ii1, jj1, kk1, i, j, k, num_1, ia2, surf
    integer :: i_cnt, j_cnt, k_cnt, ia, i3, i4, k_phi, kmax0, nmultiple, myatoms
    real    :: xi, yi, zi

    
    
    if (pars%debug > 0) print *, 'Entering setup_boundary_conditions'

    myatoms = size(x0)/3

    nmultiple = 0


    if (firsttime) then
        highest_atom_height = size(atoms, 3)
        im(1) = 1
        im(2) = size(atoms, 1) 
        jm(1) = 1
        jm(2) = size(atoms, 2)
        atoms = 0
        allocate(prephilimit(size(philimit, 1), &
                size(philimit, 2), 1:size(philimit, 3)))
        ! allocate(prephilimit, mold = philimit) ! CLEANUP: Legal Fortran 2008, 
        ! but not supported by gfortran (maybe in GCC 5.1) so can't be used :-(
        prephilimit(:, :, :minimum_surface_height) = MATERIAL
        prephilimit(:, :, minimum_surface_height + 1:) = VACUUM
        philimit = prephilimit
    end if

    atoms = 0

    !TODO: Do these boundaries need to be expanded a bit?
    prephilimit(im(1):im(2), jm(1):jm(2), &
                :minimum_surface_height) = MATERIAL
    prephilimit(im(1):im(2), jm(1):jm(2), &
                minimum_surface_height + 1:size(prephilimit,3)) = VACUUM

    im(1) = size(atoms, 1) 
    im(2) = 1
    jm(1) = size(atoms, 2)
    jm(2) = 1

    highest_atom_height = -999
    
    ! This loop puts atoms in the grid and sets all GPs that have an atom and their
    ! neighbors to prephilimit = MATERIAL

    do ia = 1, myatoms 
        i3 = ia*3 - 3 ! Helper indices
        i4 = ia*4 - 4

        ! No periodic boundaries in z-direction
        zi = (x0(i3 + 3) + 0.5d0) * globs%isize(3)
        k_cnt = ceiling(zi)

        if(zi < minimum_surface_height - 1) then
            cycle
        end if

        if(atype(ia) == pars%evpatype) then ! Skip evaporated atoms
            cycle
        end if

        ! Make coord from 0 to 1 instead of -0.5 .. 0.5
        xi = (x0(i3 + 1) + 0.5d0) * globs%isize(1)
        i_cnt = ceiling(xi) 

        ! Handle periodic boundaries
        if (pbc(1) == 1 .and. i_cnt < 1)    i_cnt = i_cnt + size(atoms, 1)
        if (pbc(1) == 1 .and. i_cnt > size(atoms, 1))  i_cnt = i_cnt - size(atoms, 1)

        ! Same in other direction
        yi = (x0(i3 + 2) + 0.5d0) * globs%isize(2)
        j_cnt = ceiling(yi)
        if(pbc(2) == 1 .and. j_cnt < 1)                j_cnt = j_cnt + size(atoms, 2)
        if(pbc(2) == 1 .and. j_cnt > size(atoms, 2))   j_cnt = j_cnt - size(atoms, 2)

        ! Update maximum k where an atom is centered
        if(highest_atom_height < k_cnt) then
            highest_atom_height = k_cnt
        end if

        ! Store minimum and maximum coords where atoms were found
        ! This gives us a range outside of which we don't have to look for atoms

        if(im(1) > i_cnt) then
            im(1) = i_cnt
        end if
        if(im(2) < i_cnt) then
            im(2) = i_cnt
        end if
        if(jm(1) > j_cnt) then
            jm(1) = j_cnt
        end if
        if(jm(2) < j_cnt) then
            jm(2) = j_cnt
        end if

        ! Sanity check
        if(k_cnt <= 0 .or. k_cnt > ubound(atoms,3)) then
            print *, "k_cnt has wrong value. k_cnt = ", &
                k_cnt, "zi = ", zi, "ia = ", ia, "x0(i3+1) = ", x0(i3+1)
            stop
        end if

        atoms(i_cnt, j_cnt, k_cnt) = ia ! Store index of atom found here

        ! Loop over neighbouring sites
        do ii = -1, 1
        do jj = -1, 1
        do kk = -1, 1
            ! Indices for neighbouring site
            i_ii = i_cnt + ii; j_jj = j_cnt + jj; k_kk = k_cnt + kk

            if(k_kk <= 0) cycle
            ! Handle periodicity
            if(pbc(1) == 1 .and. i_ii < 1)  i_ii = i_ii + size(atoms, 1)
            if(pbc(1) == 1 .and. i_ii > globs%isize(1))  i_ii = i_ii - size(atoms, 1)
            if(pbc(2) == 1 .and. j_jj < 1)          j_jj = j_jj + size(atoms, 2)
            if(pbc(2) == 1 .and. j_jj > globs%isize(2))  j_jj = j_jj - size(atoms, 2)

            ! The GP with the center of the atom and its neighbor GPs are -10
            prephilimit(i_ii, j_jj, k_kk) = MATERIAL
        enddo; enddo; enddo
    enddo ! end of loop over myatoms

    philimit(max(1,im(1) - 2):min(size(atoms,1), im(2) + 2), &
            max(1,jm(1) - 2):min(size(atoms,2), &
            jm(2) + 2),:highest_atom_height + 2) &
        = prephilimit(max(1,im(1) - 2):min(size(atoms,1), &
                        im(2) + 2), max(1,jm(1) - 2):min(size(atoms,2), &
                        jm(2) + 2), :highest_atom_height + 2)
    if (pars%debug > 1) print *, 'before kloop2' 
    
    !the next loop goes through all GPs. If more than one of its neighbors are found
    !with prephilimit ==VACUUM, i.e. without atom, we set philimit as VACUUM
    
    kloop2: do k = minimum_surface_height, highest_atom_height + 2
        do j = max(1, jm(1) - 3), min(size(atoms, 2), jm(2) + 3)
        do i = max(1, im(1) - 3), min(size(atoms, 1), im(2) + 3)
            num_1 = 0 ! Number of GPs with prephilimit = 1 (empty-from-atom point)
            outer: do kk= -1, 1 ! Loop around point i, j, k
                k_kk = k + kk
                if(k_kk <= 0) cycle kloop2 ! Don't go too low

                do jj= -1, 1
                    j_jj = j + jj;
                    if(pbc(2) == 1 .and. j_jj < 1) &
                        j_jj = j_jj +  size(atoms, 2)
                    if(pbc(2) == 1 .and. j_jj > size(atoms, 2))  &
                        j_jj = j_jj - size(atoms, 2)

                    do ii = -1, 1
                        ! Skip center
                        if(ii == 0 .and. jj == 0 .and. kk == 0) cycle

                        i_ii = i + ii;
                        if(pbc(1) == 1 .and. i_ii < 1) &
                            i_ii = i_ii + size(atoms, 1)
                        if(pbc(1) == 1 .and. i_ii > size(atoms, 1))  &
                            i_ii = i_ii - size(atoms, 1)

                        if(prephilimit(i_ii, j_jj, k_kk) == VACUUM) then
                            num_1 = num_1 + 1  
                            ! The number of GPs with philimit = 1 (empty)
                            if(num_1 > 1) then
                                philimit(i, j, k) = VACUUM 
                                ! GP is 1 if any neighbouring is 1
                                exit outer 
                                ! We only care that >1, so exit before counting them all
                            endif
                        endif
                    enddo
                enddo
            enddo outer
        enddo; enddo
    enddo kloop2
      
    if (pars%debug > 1) print *, 'before KMC part'  
    ! With KMC, the surface need to be found only once and not every step. 
    
    if( (globs%skiplimcheck .and. firsttime) .or. .not. globs%skiplimcheck ) then

        ! Let's define the surface
        surface_height = highest_atom_height 
        ! highest_atom_height is at this point the highest coordinate of any atom

        kmax0 = highest_atom_height
        highest_atom_height = -99 ! Find highest k where there are atoms everywhere

        ! Find point where surface starts
        ! Look above minimum_surface_height to avoid mistakes related to voids
        do j = jm(1), jm(2) 
        do i = im(1), im(2)
            surf = -1
            kloop: do k = minimum_surface_height, kmax0 +  1
                ! Surface not found for this i, j

                ! This makes the surface search work if a void is present, but note
                ! that search must start from inside the void
                ! i.e. can't have solid - void -solid - surface
                ! must be void - solid - surface
                if(surf == -1) then ! Still haven't encountered anything solid
                    if(philimit(i, j, k) /= VACUUM) then
                        surf = 0
                    end if
                    cycle kloop
                end if

                if(surf == 0) then
                    k_phi = 0 ! Number of free GPs above atom
                    k_kk = k + 1
                    do jj = -1, 1
                        j_jj = j + jj;
                        if(pbc(2) == 1 .and. j_jj < 1)  j_jj = j_jj + size(atoms, 2)
                        if(pbc(2) == 1 .and. j_jj > globs%isize(2)) &
                            j_jj = j_jj - size(atoms, 2)
                        do ii = -1, 1
                            ! Skip center GPs

                            i_ii = i + ii;
                            ! Handle periodic boundaries
                            if(pbc(1) == 1 .and. i_ii < 1)  &
                                i_ii = i_ii + size(atoms, 1)
                            if(pbc(1) == 1 .and. i_ii > globs%isize(1)) &
                                i_ii = i_ii - size(atoms, 1)

                            if(philimit(i_ii, j_jj, k_kk) == VACUUM) &
                                k_phi = k_phi + 1 
                                ! count all the free GP over the atom

                            if(k_phi == 9) then 
                                ! If there are 9 free GPs above an atom   
                                ! it terminatesthe surface. 
                                !All atoms above are not bound to the surface.

                                surf = k ! Surface has been found for these i, j
                                if(k > highest_atom_height) highest_atom_height = k 
                                ! Update highest_atom_height with actual surface k

                                ! This is the really important part here: 
                                ! surface_height denotes the layer which hosts the 
                                ! lowest surface atom.
                                if(k - 1 < surface_height) surface_height = k - 1 
                            endif
                            ! The surface_height is the lower layer where the atoms 
                            ! belong. But, phi array has the first element starting 
                            ! from the layer with atoms, i.e. phi(1, j, k) 
                            !  defines the potential in the layer of 
                            ! philimit(surface_height,j,k), this is why
                            ! surface_height = k - 1 is needed.
                        enddo
                    enddo
                else ! surf == 1 for this i, j
                    ! All atoms handled here are above the atom that terminated 
                    ! the surface, ie. possibly evaporating at this i, j

                    if(atoms(i, j, k) < 1) cycle ! Skip GPs without atoms

                    ! Mark GP as evaporated if an atom is here 
                    ! (it's above the surface)
                    philimit(i, j, k) = EVAPORATED_POINT

                    if (pars%debug > 1) &
                        print *, "above the surface1", surf, atoms(i, j, k), &
                                philimit(i, j, k), prephilimit(i, j, k), i, j, k

                    do kk1 = -1, 1 !loop over all neighbors
                    do jj1 = -1, 1
                    do ii1 = -1, 1
                        if(abs(ii1) + abs(jj1) + abs(kk1) == 0) cycle 
                        ! Skip center GPs
                        i_ii = i + ii1; j_jj = j + jj1; k_kk = k  + kk1 
                        !neighbor indices

                        ! Handle periodic boundaries
                        if(pbc(1) == 1 .and. i_ii < 1)  i_ii = i_ii + size(atoms, 1)
                        if(pbc(1) == 1 .and. i_ii > globs%isize(1)) &
                            i_ii = i_ii - size(atoms, 1)
                        if(pbc(2) == 1 .and. j_jj < 1)  j_jj = j_jj + size(atoms, 2)
                        if(pbc(2) == 1 .and. j_jj > globs%isize(2)) &
                            j_jj = j_jj - size(atoms, 2)

                        ! Check if neighbouring cell is empty (no atom)
                        if(atoms(i_ii, j_jj, k_kk) < 1) then
                            if(philimit(i_ii, j_jj, k_kk) == EVAPORATED_POINT) then
                                print *, &
                                    "Changed philimit from -20 to 1 due to neigh", &
                                    i_ii, j_jj, k_kk
                                philimit(i_ii, j_jj, k_kk) = VACUUM
                            endif
                        endif
                    enddo; enddo; enddo 
                endif ! surf == 0
            enddo kloop; ! loop over k

            if (surf <= 0) surface_height = minimum_surface_height
            ! No surface found for this i, j, use a default value
        enddo; enddo ! loop over i, j
        surface_height = max(surface_height, minimum_surface_height)
    endif

    if (pars%debug > 0) &
        print *, "surface_height", surface_height, kmax0, highest_atom_height

    ! Loop over grid above bulk
    if (pars%debug > 1) print *, 'before unevaporation' 
    invac = .true.
    do k = kmax0 + 1, surface_height + 1, -1
    do j = jm(1), jm(2)
    do i = im(1), im(2)
        if(philimit(i, j, k) == EVAPORATED_POINT) then 
        ! Check atoms marked as evaporated
            if(pars%debug > 1) &
                print *, "an atom is marked as evaporated at ", i, j, k, atoms(i, j, k)
            do kk1= -1, 1
                k_kk = k + kk1
                do jj1= -1, 1
                    j_jj = j + jj1
                    if(pbc(2) == 1 .and. j_jj < 1) j_jj = j_jj + size(atoms, 2)
                    if(pbc(2) == 1 .and. j_jj > globs%isize(2))  &
                        j_jj = j_jj - size(atoms, 2)
                    do ii1= -1, 1
                        ! Skip central GP
                        if(ii1 == 0 .and. jj1 == 0 .and. kk1 == 0) cycle
                        i_ii = i + ii1;
                        
                        ! Handle periodic boundaries
                        if (pbc(1) == 1 .and. i_ii < 1) i_ii = i_ii + size(atoms, 1)
                        if (pbc(1) == 1 .and. i_ii > globs%isize(1))  &
                            i_ii = i_ii - size(atoms, 1)
                        ! Check neigbouring points
                        if (philimit(i_ii, j_jj, k_kk) == MATERIAL) &
                            philimit(i, j, k) = MATERIAL 
                            ! Make atom close to surface "un-evaporate"
                            ! This is problematic when clusters evaporate!
                    enddo
                enddo
            enddo
        endif
        ! find higherst tip point to correct highest_atom_hight
        if ( invac .and. philimit(i,j,k) == MATERIAL ) then
            highest_atom_height = k
            invac = .false.
        endif 
    enddo; enddo; enddo

    ! Highest k in coordinates where 0 is surface
    highest_atom_height_above_surf = highest_atom_height - surface_height 
    ! highest_atom_height_above_surf = 1 correspond to the layer k = surface_height + 1

    !*************************************************************************************

    if(im(1) > im(2) .or. jm(1) > jm(2)) then
        print *, "It seems like no atoms were found. Strange!"
        stop
    end if
    
    if (pars%debug > 0) print *, 'Entering remove_evaporated_atoms'

    ! Just checking up to the maximum atom height does not seem to work
    if (pars%use_evaporation) &
        call remove_evaporated_atoms(atoms(im(1):im(2), jm(1):jm(2),:),&
                                philimit(im(1):im(2), jm(1):jm(2), : ), xq, atype)
    
    firsttime = .false.
    if (pars%debug > 0) then
        print *, 'exiting setup_boundary_conditions'
        print *, 'highest_atom_height_above_surf = ', highest_atom_height_above_surf
        print *, 'surface_height = ', surface_height
        print *, 'highest_atom_height = ', highest_atom_height
    endif
    
end subroutine setup_boundary_conditions


!> @brief Subroutine to remove atoms that have evaporated
subroutine remove_evaporated_atoms(atoms, philimit, xq, atype)

    !> Set atom type of evaporated atoms. This is done by looping over the grid 
    !> with atoms and checking what atoms are at points marked as evaporated.
    !> This may seem like it should be slow (looping over a large array) but in 
    !> practice it seems to take pretty much no time. An alternative implementation 
    !> would be to check the atoms already when marking a point as evaporated. 
    !> Maybe this would matter more with a very large grid.
    
    integer, intent(in)     :: atoms(:,:,:) !< Atoms on grid
    integer, intent(in)     :: philimit(:,:,:) !< Boundary conditions
    real(dp), intent(inout) :: xq(:) !< Induced charge on atoms (e) and 
    !< forces (eV/Å). Will be set to 0 for evaporated atoms.
    integer, intent(inout)  :: atype(:) !< Atom types. 
    !< Type of evaporated atoms will be set to evpatype
    integer                 :: Nrem !< Number of removed atoms    
    integer :: i, j, k, i4

    Nrem = 0
    do k = 1, size(atoms, 3)
    do j = 1, size(atoms, 2)
    do i = 1, size(atoms, 1)
        if(atoms(i, j, k) > 0 .and. philimit(i, j, k) == EVAPORATED_POINT) then
            i4 = atoms(i, j, k)*4 - 4
            xq(i4 + 1) = 0d0
            xq(i4 + 2) = 0d0
            xq(i4 + 3) = 0d0
            xq(i4 + 4) = 0d0
            atype(atoms(i, j, k)) = pars%evpatype
        endif
    enddo; enddo; enddo

end subroutine remove_evaporated_atoms


!> @brief Subroutine to remove atoms that are out of box in xy or lower of box
subroutine remove_evaps(fem, x0, x1, atype, xq)

    !> Remove atoms that are out of bounds (out of x-y box and below zmin). The 
    !> removed atoms are set to atom type evpatype and put at the left corner of the
    !> box with zero velocity.
    
    type(femocs), intent(in):: fem !< Femocs object
    real(dp), intent(inout) :: x0(:), x1(:), xq(:) !< Atomic data to be modified
    integer, intent(inout)  :: atype(:) !< Atom types.
    !< Type of evaporated atoms will be set to evpatype
    
    integer :: i, i3, i4, types(size(atype)), flag

    ! Femocs imports atoms (not grid points) and field comes from Femocs
    if (pars%fem_import_mode == 0 .and. pars%edmethod == 1) then
        call fem%export_data(flag, real(types, 8), size(atype), "atom_type")
    else
        types = 0
    endif

    do i = 1, size(x0) / 3
        i3 = (i - 1) * 3 + 1
        if (types(i) <= -2) atype(i) = pars%evpatype
!        if (atype(i) == pars%evpatype) x1(i3 : (i3+2))  = 0.d0
        if ((abs(x0(i3)) > 0.5) .or. (abs(x0(i3+1)) > 0.5) .or. &
                                (x0(i3+2) < - 0.5 ) .or. (x0(i3+2) > 1.5 )) then
            i4 = (i - 1) * 4 + 1
            print *, 'moved atom', i, 'from atype=', atype(i), 'to', pars%evpatype
            print *, 'x0 before = ', x0(i3 : (i3+2))
            atype(i) = pars%evpatype
            x1(i3 : (i3+2)) = 0.d0
            xq(i4: (i4+3)) = 0.d0
            x0(i3) = max(min(x0(i3),.499d0), -.499d0)
            x0(i3+1) = max(min(x0(i3+1),.499d0), -.499)
            x0(i3+2) = max(min(x0(i3+2),1.499d0), -.499d0)
            
            print *, 'x0 after = ', x0(i3 : (i3+2))
        endif
    enddo

end subroutine remove_evaps


!> @brief Calculates the charges induced from the field on the surface grid points
subroutine charge_gp(els)
    
    !> Calculates the charge on each surface GP. The charge is calculated based on
    !> Gauss' law, on all 6 of the GP's faces. Charge of a face is -dV/dx * eps0 * dS
    !> where dS is its area, and -dV/dx is the field running through it. 

    real(dp), parameter :: eps0 = 5.5268d-03 !dielectric constant [e/VA]
    !> Calculates the charges on all the surface grid points
    type(Electrostatics), intent(inout) :: els
    
    integer     :: i, j, k, k2, ptype, plim_nbors(6), icount
    real(dp)    :: dx, dy, dz, Efaces(6), geoms(6), dphi(6), isbulk(6)
    
    if (pars%debug > 0) print * , 'entering charge_gp'
    
    dx = els%grid_spacing(1)
    dy = els%grid_spacing(2)
    dz = els%grid_spacing(3)
    ! charge geometrical factors(face area / dr)

    
    els%charge = 0.d0
    if (pars%EDmethod == 0) then
        geoms = [dy*dz/dx, dy*dz/dx, dx*dz/dy, dx*dz/dy, dx*dy/dz, dx*dy/dz]
        do k = max(els%zbounds(1), 2), els%zbounds(2) !k runs in philimit coordinates
            k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
            do j = max(2,els%ybounds(1)), min(globs%isize(2)-1, els%ybounds(2))
            do i = max(2,els%xbounds(1)), min(globs%isize(1)-1, els%xbounds(2))
                ptype = classify_point(els%philimit, i, j, k, 1)
                if (ptype == -1) then
                    !differences between phi of point and its neighbors
                    dphi = [els%phi_sq(i+1,j,k2), els%phi_sq(i-1,j,k2), &
                            els%phi_sq(i,j+1,k2), els%phi_sq(i,j-1,k2), &
                            els%phi_sq(i,j,k2+1), els%phi_sq(i,j,k2-1)] &
                            - els%phi_sq(i,j,k2)
                    els%charge(i,j,k2) = -sum(dphi * geoms) * eps0 * els%multiplier
                endif
            enddo; enddo; 
        enddo
    else
        geoms = [dy*dz, dy*dz, dx*dz, dx*dz, dx*dy, dx*dy]
        do k = max(els%zbounds(1), 2), els%zbounds(2)!k runs in philimit coordinates
            k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
            do j = max(2,els%ybounds(1)), min(globs%isize(2)-1, els%ybounds(2))
            do i = max(2,els%xbounds(1)), min(globs%isize(1)-1, els%xbounds(2))
                ptype = classify_point(els%philimit, i, j, k, 1)
                if (ptype == -1) then
                    !Efield at the 6 faces of the grid point
                    Efaces = .5 * [els%Efield(i+1,j,k2,1), -els%Efield(i-1,j,k2,1), &
                            els%Efield(i,j+1,k2,2), -els%Efield(i,j-1,k2,2), &
                            els%Efield(i,j,k2+1,3), -els%Efield(i,j,k2-1,3)] &
                            + .5 * [els%Efield(i,j,k2,1), -els%Efield(i,j,k2,1), &
                            els%Efield(i,j,k2,2), -els%Efield(i,j,k2,2), &
                            els%Efield(i,j,k2,3), -els%Efield(i,j,k2,3)]
                    plim_nbors = [els%philimit(i+1,j,k), els%philimit(i-1,j,k), &
                                els%philimit(i,j+1,k), els%philimit(i,j-1,k), &
                                els%philimit(i,j,k+1), els%philimit(i,j,k-1)]
                    isbulk = 1.d0
                    do icount = 1, 6
                        if (plim_nbors(icount) == MATERIAL) isbulk(icount) = 0.d0
                    enddo
                    
                    ! charge geometrical factors(face area / dr)
                    els%charge(i,j,k2) = sum(Efaces * geoms * isbulk) * eps0 * &
                                            els%multiplier
                endif
            enddo; enddo; 
        enddo
    endif
        
    
end subroutine charge_gp


!> @brief Distributes the already calculated charge on GPs to the surrounding atoms
subroutine charges_new(els, atoms, xq)

    !> Calculates the charge and the Lorentz forces on the atoms. The charge has to
    !> be already calculated on the grid points by charge_gp. The charge of each
    !> gp is distributed to its surrounding atoms. If the charged GP has atom on it,
    !> its charge goes to that atom. If it does not, it is distributed equally to all
    !> first neighbor GPs that have an atom.
    
    type(Electrostatics), intent(inout) :: els !< Electrostatics struct
    
    integer, intent(in)     :: atoms(:, :, 1:) !< Atoms on a grid
    real(dp), intent(out)   :: xq(:) !< Charge on atoms (e) and force (eV/Å)
    
    integer     :: i, j, k, k2, myatom, atom_nbors(6), N_nbors
    real(dp)    :: dx, dy, dz, charge_nbors(6), charge_give
    logical     :: nbor_give(6)
    
    if (pars%debug > 0) print * , 'entering charges_new'
    
    dx = els%grid_spacing(1)
    dy = els%grid_spacing(2)
    dz = els%grid_spacing(3)
    
    xq = 0.d0
    !go through all GPs and distribute their charge to nearby atoms
    do k = max(els%zbounds(1), 2), els%zbounds(2)  !k runs in philimit coordinates
        k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
        do j = max(2,els%ybounds(1)), min(globs%isize(2)-1, els%ybounds(2))
        do i = max(2,els%xbounds(1)), min(globs%isize(1)-1, els%xbounds(2))
            if (abs(els%charge(i,j,k2)) < 1.d-30) cycle
            myatom = atoms(i,j,k) !atom belonging on the current GP
            if (myatom > 0) then
                xq((myatom-1) * 4 + 1) = xq((myatom-1) * 4 + 1) + els%charge(i,j,k2)
            else
                charge_nbors = [els%charge(i+1,j,k2), els%charge(i-1,j,k2), &
                                els%charge(i,j+1,k2), els%charge(i,j-1,k2), &
                                els%charge(i,j,k2+1), els%charge(i,j,k2-1)]
                atom_nbors  =  [atoms(i+1,j,k), atoms(i-1,j,k), &
                                atoms(i,j+1,k), atoms(i,j-1,k), &
                                atoms(i,j,k+1), atoms(i,j,k-1)]
                ! the Nbors that the charge will be given:
                ! Give only to nbors that have an atom and also face the vacuum 
                ! (they already have some charge)
                nbor_give = abs(charge_nbors) > 1.d-30 .and. atom_nbors > 0
                !Number of Nbors that the charge will be divided to:
                N_nbors = count(nbor_give)
                !Charge to give to each neighbor
                charge_give = els%charge(i,j,k2) / N_nbors
                if (nbor_give(1)) xq((atoms(i+1,j,k)-1)*4 + 1) = &
                                    xq((atoms(i+1,j,k)-1)*4 + 1) + charge_give 
                if (nbor_give(2)) xq((atoms(i-1,j,k)-1)*4 + 1) = &
                                    xq((atoms(i-1,j,k)-1)*4 + 1) + charge_give
                if (nbor_give(3)) xq((atoms(i,j+1,k)-1)*4 + 1) = &
                                    xq((atoms(i,j+1,k)-1)*4 + 1) + charge_give
                if (nbor_give(4)) xq((atoms(i,j-1,k)-1)*4 + 1) = &
                                    xq((atoms(i,j-1,k)-1)*4 + 1) + charge_give 
                if (nbor_give(5)) xq((atoms(i,j,k+1)-1)*4 + 1) = &
                                    xq((atoms(i,j,k+1)-1)*4 + 1) + charge_give
                if (nbor_give(6)) xq((atoms(i,j,k-1)-1)*4 + 1) = &
                                    xq((atoms(i,j,k-1)-1)*4 + 1) + charge_give
            endif
        enddo; enddo 
    enddo
    
    !calculate the forces
    if (pars%EDmethod == 0) then !FDM
        do k = max(els%zbounds(1), 2), els%zbounds(2) !k runs in philimit coordinates
            k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
            do j = max(2,els%ybounds(1)), min(globs%isize(2)-1, els%ybounds(2))
            do i = max(2,els%xbounds(1)), min(globs%isize(1)-1, els%xbounds(2))
                ! if the GP has no charge, has no force too
                if (abs(els%charge(i,j,k2)) < 1.d-30) cycle
                myatom = atoms(i,j,k) !atom belonging on the current GP
                if (myatom > 0) then
                    xq((myatom-1)*4 + 2) = - pars%force_factor * els%multiplier * &
                                    xq((myatom-1)*4 + 1) * &
                                    (els%phi_sq(i+1,j,k2) - els%phi_sq(i-1,j,k2))/ dx
                    xq((myatom-1)*4 + 3) = - pars%force_factor * els%multiplier * &
                                    xq((myatom-1)*4 + 1) * &
                                    (els%phi_sq(i,j+1,k2) - els%phi_sq(i,j-1,k2))/ dy
                    xq((myatom-1)*4 + 4) = - pars%force_factor * els%multiplier * &
                                    xq((myatom-1)*4 + 1) * &
                                    (els%phi_sq(i,j,k2+1) - els%phi_sq(i,j,k2-1))/ dz
                endif
            enddo; enddo 
        enddo
    else
        do k = max(els%zbounds(1), 2), els%zbounds(2) !k runs in philimit coordinates
            k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
            do j = max(2,els%ybounds(1)), min(globs%isize(2)-1, els%ybounds(2))
            do i = max(2,els%xbounds(1)), min(globs%isize(1)-1, els%xbounds(2))
                ! if the GP has no charge, has no force too
                if (abs(els%charge(i,j,k2)) < 1.d-30) cycle
                myatom = atoms(i,j,k) !atom belonging on the current GP
                if (myatom > 0) then
                    xq((myatom-1)*4+2) = pars%force_factor * els%multiplier * &
                                        xq((myatom-1)*4+1) * els%efield(i,j,k2,1)
                    xq((myatom-1)*4+3) = pars%force_factor * els%multiplier * &
                                        xq((myatom-1)*4+1) * els%efield(i,j,k2,2)
                    xq((myatom-1)*4+4) = pars%force_factor * els%multiplier * &
                                        xq((myatom-1)*4+1) * els%efield(i,j,k2,3)
                endif
            enddo; enddo 
        enddo
    endif
    
    if (pars%debug > 1) print *, 'Exiting charges_new'
        

end subroutine charges_new


!> @brief Cleans the grid points that are on the surface and should not.
subroutine clean_philim(els, atoms)

    !> Assigns the grid points that have more than three faces to vacuum as vacuum.

    type(Electrostatics), intent(inout) :: els !< Electrostatics
    integer, intent(in)                 :: atoms(:,:,:) !< Atoms on a grid
    
    integer     :: i, j, k, vac_neigh(6)  !< Number of vacuum neighbors 
    logical     :: deletes(size(els%philimit,1), size(els%philimit,2), &
                                                size(els%philimit, 3) )

    if (pars%debug > 0) print *, 'entering clean_philim'

    deletes = .false.  ! set deleting originally to false
    
    !loop over all GPs (exclude boundaries)  
    do k = max(els%zbounds(1), 2), els%zbounds(2) + 1
    do j = max(2,els%ybounds(1)), min(globs%isize(2)-1, els%ybounds(2))
    do i = max(2,els%xbounds(1)), min(globs%isize(1)-1, els%xbounds(2))
        if (els%philimit(i, j, k) == MATERIAL .and. atoms(i,j,k) == 0) then
            vac_neigh = [els%philimit(i+1,j,k), els%philimit(i-1,j,k), &
                        els%philimit(i,j+1,k), els%philimit(i,j-1,k), &
                        els%philimit(i,j,k+1), els%philimit(i,j,k-1)]    
            if (count(vac_neigh == VACUUM) > 2) deletes(i,j,k) = .true.
        endif
    enddo; enddo; enddo
    
    !loop over all GPs (exclude boundaries)
    do k = max(els%zbounds(1), 2), els%zbounds(2) + 1 
    do j = max(2,els%ybounds(1)), min(globs%isize(2)-1, els%ybounds(2))
    do i = max(2,els%xbounds(1)), min(globs%isize(1)-1, els%xbounds(2))
        if (deletes(i,j,k)) els%philimit(i,j,k) = VACUUM
    enddo; enddo; enddo
    
    if (pars%debug > 0) print *, 'cleaned ', count(deletes), 'points'

end subroutine clean_philim


!> @brief Calculate the lattice constant, accounting for stretching etc.
subroutine calc_grid_spacing(x0, n, grid_spacing)

    !> This algorithm is based on k-mean clustering: 
    !> https://en.wikipedia.org/wiki/K-means_clustering
    !> @todo Iterate until convergence is reached (i.e. no atoms are reassigned)

    integer, intent(in) :: n 
    !< Number of layers to consider for calculation (gridpoints)
    real(dp), intent(in) :: x0(:) !< Atom coordinates (parcas units)

    real(dp), intent(inout) :: grid_spacing(3) 
    !< Grid spacing. In: estimated spacing, out: calculated spacing

    real(dp) :: centroids(n) ! Centroids that will correspond to atomic layers
    integer :: i3
    real(dp) :: zs(n, 2) ! First column contains sum of atomic coordinates, 
    ! second the number of atoms contributing (for calc. average)
    real(dp) :: dist ! Distance betwwen atom and a layer
    real(dp) :: dists(n-1) ! Distances between atomic layers
    integer :: closest ! Index of closest layer
    real(dp), save :: zmin
    integer :: i, j, n2
    real(dp) :: mi

    integer :: myatoms

    logical, save :: firsttime = .true.

    myatoms = size(x0)/3

    if(n <= 2) then
      print *, "find_latticeconst tried to determine the lattice constant based on less than three layers. This is not possible."
      print *, "This probably means that the code got confused about where you have atoms (things everything has evaporated)."
      print *, "Check md.movie for any weirdness."
      stop
    end if

    ! Find where the lowest atoms are. They should be fixed, so only do it once
    if(firsttime) then
      zmin = minval(x0(3::3))*globs%box(3) 
      if (pars%debug > 1) print *, "DBG: zmin at ", zmin
      if (pars%debug > 1) print *, "DBG: initial grid_spacing", grid_spacing
      firsttime = .false.
    end if


    centroids = 0
    zs = 0
    closest = -1
    ! Loop over all atoms and see which centroid (position of an atomic layer) is closest
    ! Assign atom to the closest centroid
    ! Then calculate the average position of atoms in the centroid
    ! That average position becomes the new position of the centroid, i.e. the atomic layer
    ! In practice iteration is probably not needed though
    do i = 1, myatoms
      i3 = 3*i
      mi = 1000000
      do j =1, n
        ! Find closest centroid
        dist = abs(x0(i3)*globs%box(3) - (zmin + grid_spacing(3)*(j - 1)))
        if(dist<mi) then
          closest = j
          mi = dist
        end if
      end do

      ! Assign position to closest centroid
      if (closest == -1) then
        print *, "Could not assign an atom to a centroid when determining the lattice constant"
        print *, "This means that the code thinks an atom is REAAAALLY far away, so probably an error"
        stop
      end if
      zs(closest, 1) = zs(closest, 1) + x0(i3)*globs%box(3)
      zs(closest, 2) = zs(closest, 2) + 1
    end do

    ! Just calculate up to the actually highest layer
    n2 = n
    ! Calculate centroid position
    do i = 1, n
      if(zs(i, 2) == 0) then ! Stop when no more atoms near where a layer should have been
        n2 = i
        exit
      end if
      centroids(i) = zs(i, 1)/zs(i, 2)
    end do

    if (pars%debug > 1) then
        print *, "Found ", n2, "layers", n
        print *, "Centroid coords"
        if (pars%debug > 2) print *, centroids(:n2)
        print *, "Shape of dists is ", shape(dists)
        print *, "Shape of centroids is ", shape(centroids), n2 -1
    endif

    ! Calculate distance between centroids
    dists = 0
    dists(1:n2-1) = centroids(2:n2) - centroids(1:n2 - 1)
    if (pars%debug > 1) then
        print *, "Shape of dists is now", shape(dists)
        print *, "Shape of centroids is now ", shape(centroids)
        if (pars%debug > 2) print *, "dists", dists
    endif

    if(n2>2) then
    ! Lattice const given by avg distance
      grid_spacing(3) = sum(dists(1:n2 - 1 - 1)) / ( n2 - 1 -1)
    else
      grid_spacing(3) = dists(1)
    end if
    if (pars%debug > 1) print *, "grid_spacing(3)", grid_spacing(3)

end subroutine calc_grid_spacing


!> @brief Move the boundary conditions from the original grid to the square grid
subroutine limgridsq(grid1, a1, grid2, a2, surface_height)

    !> Interpolate the boundary conditions to a square grid to avoid unsymmetry due 
    !> to numerical effects. This is only really needed when working with a 110 
    !> surface. A square grid is only used in the xy-plane, while the z-direction 
    !> is left unaffected.
    !>
    !> Optionally pass in a the height where the surface begins on the rectangular 
    !> grid and get out the height on the square grid.  However, currently nothing
    !> is done in the z-direction, so there should be no change.

    integer, intent(in)     :: grid1(:, :, :) !< The rectangular grid
    real(dp), intent(in)    :: a1(3) !< The grid spacing on the rectangular grid
    real(dp), intent(in)    :: a2(3) !< The grid spacing on the square grid

    integer, intent(inout), optional    :: surface_height
    !< In: Surface height on rectangular grid, out: height on square grid
    integer, allocatable, intent(out)   :: grid2(:, :, :)!< The square grid

    integer :: s1(3), s2(3), i, j, k, i2, j2, k2

    real(dp), parameter :: tolerance = 0.0001

    s1(1) = size(grid1, 1)
    s1(2) = size(grid1, 2)
    s1(3) = size(grid1, 3)

    s2 = ceiling(s1*a1/a2 - tolerance)

    if(.not.(allocated(grid2))) then
        ! CLEANUP: Replace with allocate(grid2, mold = s2) when it's supported 
        !in compilers
        allocate(grid2(s2(1), s2(2), s2(3)))
    end if

    if(all(abs(a1 - a2)<tolerance)) then
        grid2 = grid1
        return
    end if

    if (pars%debug > 1) print *, "RECTGRID:", s1, s2
    if (pars%debug > 1) print *, "RECTGRID:", a1, a2

    if(present(surface_height)) then
        if (pars%debug > 1) print *, "RECT: surface_height is ", surface_height
        surface_height = ceiling(surface_height*a1(3)/a2(3) - tolerance)
        if (pars%debug > 1) print *, "RECT: surface_height is ", surface_height
    end if

    grid2 = -99
    do i = 1, s1(1)
    do j = 1, s1(2)
    do k = 1, s1(3)
        i2 = ceiling(i*a1(1)/a2(1) - tolerance)
        if(i2 < 1 .or. i2 > s2(1)) then
            print *, "i2 problem", i2, s2(1)
            stop
        end if
        j2 = ceiling(j*a1(2)/a2(2) - tolerance)
        if(j2 < 1 .or. j2 > s2(2)) then
            print *, "j2 problem", j2, s2(2)
            stop
        end if

        k2 = ceiling(k*a1(3)/a2(3) -tolerance)
        if(k2 < 1 .or. k2 > s2(3)) then
            print *, "k2 problem", k, a1, a2, k2, s2(3)
            stop
        end if
        grid2(i2, j2, k2) = grid1(i, j, k)
    end do
    end do
    end do

    if(.true.) then ! Set to true for added safety
        if(any(grid2 == -99)) then
            do k2 = 1, s2(3)
            do j2 = 1, s2(2)
            do i2 = 1, s2(1)
                if(grid2(i2, j2, k2) == -99) then
                    print *, "Skipped index ", i2, j2, k2
                    stop
                end if
            end do
            end do
            end do
        end if
    end if
end subroutine limgridsq


!> @brief Move the electric field from a square grid to the original grid
subroutine egridunsq(grid1, a1, grid2, a2)

    !> Interpolate the electric field back from a square grid to the original grid.
    !> This is only really needed when working with 110 surface.

    real(dp), intent(in)    :: grid1(:, :, :, :) !< Square grid
    real(dp), intent(in)    :: a1(3) !< Grid spacing of square grid
    real(dp), intent(in)    :: a2(3) !< Grid spacing of rectangular grid
    real(dp), intent(out)   :: grid2(:, :, :, :) !< Rectangular grid
    
    real(dp), parameter     :: tolerance = 0.0001
    integer                 :: s1(3), s2(3), i, j, k, i2, j2, k2
    

    s1(1) = size(grid1, 1)
    s1(2) = size(grid1, 2)
    s1(3) = size(grid1, 3)

    s2 = shape(grid2(:, :, :, 1))

    !allocate(grid2(s2(1), s2(2), s2(3), 4))
    if(all(abs(a1 - a2)<tolerance)) then
      grid2 = grid1
      return
    end if
    
    print *, 'Warning: Unsquaring the grid'

    grid2 = -99
    ! Should really do some sort of interpolation?
    do i2 = 1, s2(1)
    do j2 = 1, s2(2)
    do k2 = 1, s2(3)
      i = max(1, floor(i2*a2(1)/a1(1) + tolerance))
      if(i < 1 .or. i > s1(1)) then
        print *, "i problem", i, s1(1)
        stop
      end if
      j = max(1, floor(j2*a2(2)/a1(2) + tolerance))
      if(j < 1 .or. j > s1(2)) then
        print *, "j problem", j, s1(2)
        stop
      end if
      k = max(1, floor(k2*a2(3)/a1(3) + tolerance))
      if(k < 1 .or. k > s1(3)) then
        print *, "k problem", k, k2, s1(3), s2(3)
        stop
      end if
      grid2(i2, j2, k2,  :) = grid1(i, j, k, :)
    end do
    end do
    end do

    if(.true.) then
    if(any(grid2 == -99)) then
      stop "problem in egridunsq"
    end if
    end if
end subroutine egridunsq


!> @brief Solve field using femocs. 
subroutine fem_solve_field(els, fem, x0, nborlist)
        
    !> The solution is transfered into els%Efield for boundary grid points. 
    
    use libfemocs, only: femocs  
    use iso_c_binding

    type(femocs), intent(inout)         :: fem  !< Finite elements object to be 
                                                !< created-modified.
    type(Electrostatics), intent(inout) :: els  !< Helmod electrostatics object to
                                                !< be modified from fem solution.                                           
    real(dp), intent(in)    :: x0(:) 
    !< Atom coordinates [x1, y1, z1, x2, y2, z2, ...] (parcas units)
    integer, intent(in)     :: nborlist(:) 
    !< Neighbor list from parcas. Will be used to import atoms to femocs.
    
    real(dp)            :: x0_new(size(x0)) !transformed x0 to helmod coordinates

    integer             :: flag, Ni(size(x0)/3), i
    real(dp)            :: x(size(x0)/3), y(size(x0)/3), z(size(x0)/3)
    logical, save       :: firstime = .true.
    
    real(dp),  pointer  :: xp(:), yp(:), zp(:)
    integer, pointer    :: types(:)
    
    if (pars%debug > 0) print *, 'Entering fem_solve_field'
    
    if (firstime) then
        if (pars%debug > 0) print *, 'Calling femocs initialization'
        fem = femocs("in/md.in") ! input string  gives the input parameters file
    endif
    
    
    if (pars%fem_import_mode == 0) then
        Ni = [(i, i = 1,size(Ni))]
        call transform_x0(els, x0, Ni, x, y, z)
        do i = 1, size(Ni)
            x0_new((i-1) * 3 + 1 : (i - 1) * 3 + 3) = [x(i), y(i) ,z(i)]
        enddo
        if (pars%debug > 0) print *, "Running femocs.import_parcas."
        call fem%import_parcas(flag, size(Ni), x0_new, [1.d0, 1.d0, 1.d0], nborlist)
        if (flag /= 0) print *, 'FEM import not completed normally. flag =', flag
    elseif (pars%fem_import_mode == 1) then
        call extract_surface(els, xp, yp, zp, types)
        if (pars%debug > 0) print *, 'importing surface grid to femocs...'
        call fem%import_atoms(flag, size(xp), xp, yp, zp, types)
    else
        stop 'wrong fem_import_mode'
    endif
  
    if (pars%debug > 0) print *, "Run femocs with Eappl =", els%current_applied_field
    call fem%run(flag, -1, -1.d0)
    if (flag == 0) then
        els%lastsolve = globs%nsteps
        els%valid_solution = .true.
        if (pars%debug > 0) print *, 'femocs returned successfully. flag = ', flag
    else
!        els%valid_solution = .false.
        if (pars%debug > 0) print *, 'FEM solution not calculated. flag = ', flag
    endif
    
    ! Transfer field if it is going to be needed either by charges or heating
    if (els%valid_solution .and. (pars%charge_method == 0 .or. pars%heatmode > 1)) &                             
        call transfer_elfield(els, fem)
    firstime = .false.
    
    if (pars%debug > 0) print * , 'Exiting fem_solve_field'
    
end subroutine fem_solve_field


!> @brief Transfer electric field from femocs to helmod.
subroutine transfer_elfield(els, fem)

    !> The electric field is interpolated by femocs in all the grid points around
    !> the surface. The surface is defined currently by up to 3rd order neighbors.
    
    type(femocs), intent(inout)         :: fem  
    !< Finite elements object to be created-modified.
    type(Electrostatics), intent(inout) :: els 
    !< Helmod electrostatics object to be modified from fem solution. 

    integer     :: i, j, k, k2, ptype, icount, flag, printed
    integer, dimension(product(shape(els%Efield))) :: ilist, jlist, klist
    real(dp), allocatable   :: x(:), y(:), z(:), Efield(:, :)
    integer, allocatable    :: flags(:)
    
    if (pars%debug > 0) print *, 'entering transfer_elfield'
    
    icount = 0
    if (els%lastsolve == globs%nsteps) then !case field is new transfer everything        
        do k = max(els%zbounds(1), 2), els%zbounds(2) + 1
            k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
            do j = max(2,els%ybounds(1)-1), min(globs%isize(2)-1, els%ybounds(2)+1)
            do i = max(2,els%xbounds(1)-1), min(globs%isize(1)-1, els%xbounds(2)+1)
                ptype = classify_point(els%philimit, i, j, k, 1)
                if (abs(ptype) <= 1) then
                    icount = icount + 1
                    ilist(icount) = i
                    jlist(icount) = j
                    klist(icount) = k2
                else
                    els%Efield(i,j,k,:) = 0.d0
                endif
            enddo; enddo 
        enddo
    else  !in case the field is already old, transfer only the GPs where Efield = 0
        do k = max(els%zbounds(1), 2), els%zbounds(2) + 1
            k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
            do j = max(2,els%ybounds(1)-1), min(globs%isize(2)-1, els%ybounds(2)+1)
            do i = max(2,els%xbounds(1)-1), min(globs%isize(1)-1, els%xbounds(2)+1)
                ptype = classify_point(els%philimit, i, j, k, 1)
            if (abs(ptype) <= 1 .and. els%Efield(i, j, k2, 4) < 1.d-30) then
                icount = icount + 1
                ilist(icount) = i
                jlist(icount) = j
                klist(icount) = k2
            endif
        enddo; enddo; enddo
    endif
    if (icount == 0) return
    
    allocate(x(icount), y(icount), z(icount), flags(icount), Efield(icount, 4))
    
    x = (ilist(1:icount) - 0.5) * els%grid_spacing(1)
    y = (jlist(1:icount) - 0.5) * els%grid_spacing(2)
    z = (klist(1:icount) - 0.5) * els%grid_spacing(3)
    
    call  Efield_atpoint(els, fem, x, y, z, Efield, flags) 
    

    
    printed = 0
    
    do i = 1, icount
        if (.not. any(isnan(Efield(i,:))) .and. &
                    Efield(i,4) > 1.d-50 .and. Efield(i,4) < 2.d2) then
            els%Efield(ilist(i), jlist(i), klist(i), :) = Efield(i,:)
        else
            if (i==1) then
                els%Efield(ilist(i), jlist(i), klist(i), :) = els%maxField
            else
                els%Efield(ilist(i), jlist(i), klist(i), :) = &
                    els%Efield(ilist(i-1), jlist(i-1), klist(i-1), :)
            endif
   
            print *, 'Unacceptable field value from interp... assigning previous'
            print '(A5,I10,A5,3I10,A9,4ES13.5)', 'i = ', i, 'GP=', &
                ilist(i), jlist(i), klist(i), '  Efield = ', Efield(i,:)
        endif
        if (els%lastsolve == globs%nsteps) els%maxField = 0.d0
        if (els%Efield(ilist(i), jlist(i), klist(i), 4) > els%maxField) &
            els%maxField = els%Efield(ilist(i), jlist(i), klist(i), 4)
    enddo
    
    deallocate(x, y, z, flags, Efield)
    print *, '---finishing field transfer---'

end subroutine transfer_elfield


!> @brief Transfer electrostatic potential from femocs to helmod.
subroutine transfer_phi(els, fem)

    !> The electric potential is interpolated by femocs in all the grid points around
    !> the surface, both on the vacuum and material side.
    !> The surface is defined by up to 1st order neighbors.
    
    type(femocs), intent(in)            :: fem  
    !< Finite elements object that contains all field info as input.
    type(Electrostatics), intent(inout) :: els 
    !< Helmod electrostatics object to be modified from fem solution. 

    integer                 :: i, j, k, k2, ptype, icount, iflag, Nlist
    real(dp), allocatable   :: x(:), y(:), z(:), phi(:)
    integer, allocatable    :: flags(:), ilist(:), jlist(:), klist(:)
    
    
    if (pars%debug > 0) print * , 'entering transfer_phi'
    
    Nlist = (els%zbounds(2)-max(els%zbounds(1), 2)+2) * & 
            (min(globs%isize(2), els%ybounds(2)) - max(2,els%ybounds(1)) + 1) * &
            (min(globs%isize(1), els%xbounds(2)) - max(2,els%xbounds(1)) + 1)
    allocate(ilist(Nlist), jlist(Nlist), klist(Nlist))
    
    icount = 0
    do k = max(els%zbounds(1), 2), els%zbounds(2) + 1
        k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
        do j = max(2,els%ybounds(1)-1), min(globs%isize(2)-1, els%ybounds(2)+1)
        do i = max(2,els%xbounds(1)-1), min(globs%isize(1)-1, els%xbounds(2)+1)
            ptype = classify_point(els%philimit, i, j, k, 1)
            if (abs(ptype) <= 1) then
                icount = icount + 1
                ilist(icount) = i
                jlist(icount) = j
                klist(icount) = k2
            endif
        enddo; enddo 
    enddo
    
    allocate(x(icount), y(icount), z(icount), flags(icount), phi(icount))
    
    x = (ilist(1:icount) - 0.5) * els%grid_spacing(1)
    y = (jlist(1:icount) - 0.5) * els%grid_spacing(2)
    z = (klist(1:icount) - 0.5) * els%grid_spacing(3)
    
    print *, 'here'
    
    call fem%interpolate_phi(iflag, icount, x, y, z, phi, flags)
    
    print *, 'after interpolation', iflag, minval(klist(1:icount)), maxval(klist(1:icount))
    
    do i = 1, icount
        els%phi_sq(ilist(i), jlist(i), klist(i)) = phi(i)
    enddo
    
    deallocate(x, y, z, flags, phi, ilist, jlist, klist)

end subroutine transfer_phi


!< @brief Classifies a grid point on surface, bulk and vacuum.
function classify_point(plim, i, j, k, nlevel) result(pointype)
    
    !> The classification is done depending on the neighbors on the square grid.
    !> If a point has any neighbor up to nlevel order that is of the other kind, 
    !> it is designated as surface point.
    
    integer, intent(in)     :: plim(:,:,:) !< Boundary conditions on grid.

    integer, intent(in)     :: i, j, k  !< Indices of point to be classified.
    integer, intent(in)     :: nlevel !< Level of the neighbor list to be used
    integer                 :: pointype !< 2: exterior, 1: surface on vacuum side,
    !< -1: surface on material side, !< -2 bulk material
    
    integer                 :: nlist1(6), nlist2(12), nlist3(8), on 
    !on: philimit on the current point
    !nlist1: philimit on the 1st nearest neighbouring grid points.
    !nlist2: philimit on the 2nd nearest neighbouring grid points. 
    !nlist3: philimit on the 3rd nearest neighbouring grid points.
    
    on  = plim(i, j, k)
    
    !define neighbor lists (each for each level of neighbors)
    nlist1 = [plim(i+1,j,k), plim(i-1,j,k), plim(i,j+1,k), &
            plim(i,j-1,k), plim(i,j,k+1), plim(i,j,k-1)]
    if (nlevel > 1)  nlist2 = [plim(i+1,j+1,k), plim(i-1,j-1,k), plim(i+1,j-1,k), &
                                plim(i-1,j+1,k), plim(i,j+1,k+1), plim(i,j-1,k-1), &
                                plim(i,j+1,k-1), plim(i,j-1,k+1), plim(i+1,j,k+1), &
                                plim(i-1,j,k-1), plim(i+1,j,k-1), plim(i-1,j,k+1)]
    if (nlevel > 2) nlist3 = [plim(i+1,j+1,k+1), plim(i+1,j+1,k-1), &
                        plim(i+1,j-1,k+1), plim(i+1,j-1,k-1), plim(i-1,j+1,k+1), &
                        plim(i-1,j+1,k-1), plim(i-1,j-1,k+1), plim(i-1,j-1,k-1)]

    if (on == VACUUM) then !in case vacuum
        if (any(nlist1 == MATERIAL)) then !check first neighbor level
            pointype = 1
        elseif (nlevel > 1 .and. any(nlist2 == MATERIAL) ) then !check 2nd nbors
            pointype = 1
        elseif (nlevel > 2 .and. any(nlist2 == MATERIAL) ) then !check 3rd nbors
            pointype = 1
        else
            pointype = 2
        endif
    elseif (on == MATERIAL) then !in case material
        if (any(nlist1 == VACUUM)) then
            pointype = -1
        elseif (nlevel > 1 .and. any(nlist2 == VACUUM) ) then
            pointype = -1
        elseif (nlevel > 2 .and. any(nlist2 == VACUUM) ) then
            pointype = -1
        else
            pointype = -2
        endif
    endif
    
end function classify_point


!> @brief Calculates the electric field at an arbitrary point by interpolation.
subroutine Efield_atpoint(els, fem, x, y, z, Efield, flags)

    !> In case of helmod the standard bspline interpolation is used. In case of 
    !> femocs, the femocs interpolation functions are called.
    
    use bspline, only: db3ink, db3val
    
    type(Electrostatics), intent(in)    :: els !< Ιnput Electrostatics structure.
    type(femocs), intent(in)            :: fem !< Femocs data structure.
    
    real(dp), intent(in)    :: x(:), y(:), z(:) !< Asked points on the grid (A)
    real(dp), intent(out)   :: Efield(:, :) !< Output fields. 4th is the norm.
    integer, intent(out)    :: flags(:) !< Output flags. non-zero if out of grid
        
    real(dp), dimension(size(x))    :: Ex, Ey, Ez, Enorm
    integer                         :: flag(size(x)), i, j, Nx, Ny, Nz
    
    integer, save           :: setstep ! last step that interpolation was set
    logical, save           :: firstime = .true.
    
    real(dp), allocatable, save :: xgr(:), ygr(:), zgr(:), tx(:), ty(:), tz(:), &
                                    bcoef(:,:,:,:)
    integer, parameter      :: kx = 4, ky = 4, kz = 4, iknot = 0
    integer                 :: inbvx=1, inbvy=1, inbvz=1, iloy=1, iloz=1
    integer                 :: idx=0, idy=0, idz=0, iflag 
    
    
    if(pars%EDmethod == 0) then
        Nx = size(els%Efield, 1); Ny = size(els%Efield, 2); Nz = size(els%Efield, 3)
        if (firstime) then !if firsttime allocate
            allocate(bcoef(Nx, Ny, Nz, 4), tx(Nx+kx), ty(Ny+ky), &
                tz(Nz+kz), xgr(Nx), ygr(Ny), zgr(Nz))
            xgr = [(els%grid_spacing(1) * (i-1), i = 1, Nx)]
            ygr = [(els%grid_spacing(2) * (i-1), i = 1, Ny)]
            zgr = [(els%grid_spacing(3) * (i-1), i = 1, Nz)]
        endif
        if (firstime .or. els%lastsolve > setstep) then !if spline setup is obsolete
            do i = 1, 4  !setup interpolation
                call db3ink(xgr, Nx, ygr, Ny, zgr, Nz, els%Efield(:, :, :, i), &
                            kx, ky, kz, iknot, tx, ty, tz, bcoef(:, :, :, i), iflag)
            enddo
            setstep = globs%nsteps
            if (iflag /= 0) print *, 'Error: interpolation setup. iflag =', iflag
        endif
        do j = 1, 4
            do i = 1, size(x) !interpolate for all points of rline
                call db3val(x(i), y(i), z(i), idx, idy, idz, tx, ty, tz, Nx, Ny, Nz,&
                            kx, ky, kz, bcoef(:, :, :, j), Efield(i, j), &
                            flags(i), inbvx, inbvy, inbvz, iloy, iloz)
            enddo
        enddo
    elseif(pars%EDmethod == 1) then
        if (pars%debug > 0) &
            print *, 'calling fem interpolator at n =', size(x), 'points'
        
        if (pars%interp_method == 0) then
            call fem%interpolate_elfield(iflag, size(x), x, y, z, &
                    Efield(:,1), Efield(:,2), Efield(:,3), Efield(:,4), flags)
        elseif (pars%interp_method == 1) then    
            call fem%interpolate_surface_elfield(iflag, size(x), x, y, z, &
                    Efield(:,1), Efield(:,2), Efield(:,3), Efield(:,4), flags)
        else
            stop 'wrong interp_method parameter'
        endif
        
        if (pars%debug > 0 .and. iflag /= 0) &
            print *, 'interpolation not set correctly in fem'
    else
        print *, 'Wrong EDmethod parameter'
        stop
    endif
    
    firstime = .false.

end subroutine Efield_atpoint


!> @brief Extracts the surface
subroutine extract_surface(els, x, y, z, types)
    
    type(Electrostatics), intent(in)    :: els 
    !< Helmod electrostatics object to be modified from fem solution. 
    real(dp), pointer, intent(out)      :: x(:), y(:), z(:)
    !< position of the surface atoms
    integer, pointer, intent(out)       :: types(:)
    !< Types of surface atoms (by default it is 2 - needed from femocs)
    
    real(dp), target, allocatable, save :: xx(:), yy(:), zz(:)
    integer, target, allocatable, save  :: ttypes(:)
    
    integer :: i, j, k, k2, icount, Nx, Ny, Nz, ptype
    
    if (pars%debug > 0) print *, 'entering extract_surface'
    
    icount = 0
    
    Nz =  size(els%Efield, 3)-2
    Ny =  size(els%Efield, 2)-2
    Nx =  size(els%Efield, 1)-2
    
    if (.not. allocated(xx)) &
        allocate(xx(Nx*Ny*Nz), yy(Nx*Ny*Nz), zz(Nx*Ny*Nz), ttypes(Nx*Ny*Nz))
        
    
    do k = max(els%zbounds(1), 2), els%zbounds(2)!k runs in philimit coordinates
        k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
        do j = max(2,els%ybounds(1)), min(globs%isize(2)-1, els%ybounds(2))
        do i = max(2,els%xbounds(1)), min(globs%isize(1)-1, els%xbounds(2))
            ptype = classify_point(els%philimit, i, j, k, 1)
            if (ptype == -1) then
                icount = icount + 1
                xx(icount) = (i-0.5) * els%grid_spacing(1)
                yy(icount) = (j-0.5) * els%grid_spacing(2)
                zz(icount) = (k2-0.5) * els%grid_spacing(3)
                ttypes(icount) = 2
            endif
        enddo; enddo; 
    enddo
    
    x => xx(1:icount)
    y => yy(1:icount)
    z => zz(1:icount)
    types => ttypes(1:icount)
    
    if (pars%debug > 0) print *, 'exiting extract_surface'

end subroutine extract_surface


!> @brief Calculates the dipole force that a neutral atom 'feels' at a given point.
subroutine dipole_force(els, fem, x, y, z, force)

    type(Electrostatics), intent(in)    :: els !< Ιnput Electrostatics structure.
    type(femocs), intent(in)            :: fem !< Femocs data structure.
    
    real(dp), intent(in)    :: x(:), y(:), z(:) !< Asked points on the grid (A)
    real(dp), intent(out)   :: force(:, :) !< Output force (eV/A). 4th is the norm.
    
    real(dp), parameter     :: alpha = 0.55d0 ! polarizability in eV A^2/V^2
    integer                 :: i, j, Nx, flags(size(x))
    real(dp)                :: E0(size(x), 4), E1(size(x), 4)
    real(dp)                :: U0(size(x)), U1(size(x)), dx
    
    
    Nx = size(x)
    dx = els%grid_spacing(1) * 0.5d0
    call Efield_atpoint(els, fem, x, y, z, E0, flags)
    U0 = 0.5d0 * alpha * E0(:,4)**2
    do i = 1, Nx
        if (flags(i) /= 0) U0(i) = 0.d0
    enddo
    
    do j = 1, 3
        if (j == 1) then
            call Efield_atpoint(els, fem, x + dx, y, z, E1, flags)
        elseif (j == 2) then
            call Efield_atpoint(els, fem, x, y + dx, z, E1, flags)
        else
            call Efield_atpoint(els, fem, x, y, z + dx, E1, flags)
        endif
        U1 = 0.5d0 * alpha * E1(:, 4)**2
        do i = 1, Nx
            if (flags(i) /= 0) U1(i) = 0.d0
        enddo
        force(:, j) = (U1 - U0) / dx
    enddo
    force(:, 4) = norm2(force(:, 1:3), 2)
    
end subroutine dipole_force


!> @brief Goes through all atoms list and assigns the dipole force on the atoms.
subroutine insert_dipforce(els, fem, x0, xnp, atype)

    type(Electrostatics), intent(in)    :: els !< Ιnput Electrostatics structure.
    type(femocs), intent(in)            :: fem !< Femocs data structure.
    
    real(dp), intent(in)    :: x0(:) 
    !< Atom coordinates [x1, y1, z1, x2, y2, z2, ...] (parcas units)
    real(dp), intent(inout) :: xnp(:) 
    !< Force acting on atoms (parcas units)
    integer, intent(in)     :: atype(:) !< Atom types
    
    integer                 :: myatoms, i, Nevaps
    real(dp), allocatable   :: dforce(:,:), x(:), y(:), z(:)
    
    myatoms = size(x0) / 3
    
    Nevaps =  0
    do i = 1, myatoms
        if (atype(i) == pars%evpatype) Nevaps = Nevaps + 1
    enddo
    
    if (Nevaps == 0) return
    
    if (allocated(x)) deallocate(x,y,z,dforce)
    allocate(x(Nevaps), y(Nevaps), z(Nevaps), dforce(Nevaps,4))
    
    do i = 1, myatoms
        if (atype(i) == pars%evpatype) then
            call transform_x0(els, x0, [i], x, y, z)
        endif
    enddo

    call dipole_force(els, fem, x, y, z, dforce)
    
    do i = 1, myatoms
        if (atype(i) == pars%evpatype) then
            xnp((i-1)*3 + 1: (i-1)*3 + 3) = dforce(i , 1:3) / globs%box
        endif
    enddo

end subroutine insert_dipforce


!> @brief Transforms the position vector x0 from PARCAS units to Helmod-femocs units.
subroutine transform_x0(els, x0, Ni, x, y, z)
    !> Transforms the position vector x0 of the Nth atom from parcas coordinates to
    !> the coordinates of the els%Efield vector. The origin of the coordinate system
    !> is at the grid point [1,1,1] of the Efield vector. This corresponds to the
    !> horizontal plane surface of the material. 
    
    type(Electrostatics), intent(in)    :: els
    !< Electrostatics data structure input 
    real(dp), intent(in)    :: x0(:) 
    !< Atom coordinates [x1, y1, z1, x2, y2, z2, ...] (parcas units)
    integer, intent(in)     :: Ni(:)
    !< indices of atoms to be exported
    real(dp), intent(out)   :: x(:), y(:), z(:)
    
    integer                 :: i
    
    do i = 1, size(Ni)
        x(i) = (x0((Ni(i)-1)*3 + 1) + 0.5d0) * globs%box(1)
        y(i) = (x0((Ni(i)-1)*3 + 2) + 0.5d0) * globs%box(2)
        z(i) = (x0((Ni(i)-1)*3 + 3) + 0.5d0) * globs%box(3) - &
                els%grid_spacing(3) * (els%zbounds(1)-1)
    enddo

end subroutine transform_x0


!> @brief Prints the particle positions, velocities etc to ovito-readable xyz file.
subroutine print_particles(els, x0, x1, xT, xq, atoms, atype, filename, mode)

    !> Prints the atom type, position, corresponding grid point, velocities and 
    !> charges to an xyz file which is readable by ovito.

    type(Electrostatics), intent(in)    :: els !< Electrostatics data structure input 
    real(dp), intent(in)                :: x0(:), x1(:), xq(:), xT(:) 
    !< Atom coordinates [x1, y1, z1, x2, y2, z2, ...] and velocities (parcas units)
    integer, intent(in)                 :: atoms(:,:,:) !< Atoms placed on a grid
    integer, intent(in)                 :: atype(:)!< atom types
    character(len=*), intent(in)        :: filename !< filename to write
    integer, intent(in), optional       :: mode !< What to print. If 0 (default)
    !< only position, charge and forces are printed
    !< if 1, the GP where the atom belongs is printed
    
    integer, parameter  :: fid = 1564
    integer             :: i, j, k, Ni(1), i3, i4, close_status
    
    real(dp)            :: x(1), y(1), z(1)
    character(len=3)    :: atypestr(0:1)
    
    atypestr(0) = "Ev "
    atypestr(1) = "Cu "
    
    
    open(fid, file = trim(filename), action = 'write')
    write(fid,*) size(x0)/3
    
    if (present(mode) .and. mode > 0) then
        write(fid,*) 'GP, x, y, z, i, j, k, charge, Fx, Fy, Fz', &
                 'Properties=species:S:1:pos:R:3:gp:I:3:charge:R:1:force:R:3'   
        do k = 1, els%zbounds(2)
        do j = 1, globs%isize(2)
        do i = 1, globs%isize(1)
            Ni(1) = atoms(i,j,k)
            if (Ni(1) > 0) then
                i3 = (Ni(1) - 1)*3 + 1; i4 = (Ni(1) - 1)*4 + 1
                call transform_x0(els, x0, Ni, x, y, z)
                write(fid,'("GP ",3ES13.5,3I8,4ES13.5)') x,y,z, i,j,k, xq(i4:i4+3)
            endif
        enddo; enddo; enddo
    else
        write(fid,*) 'type, x, y, z, atype, id, vx, vy, vz, T, charge, Fx, Fy, Fz  ' , &
                    'boxsize =', globs%box, &
        'Properties=species:S:1:pos:R:3:mu:I:1:id:I:1:velo:R:3:temp:R:1:charge:R:1:force:R:3'
        do i = 1, size(x0) / 3
            i3 = (i - 1)*3 + 1; i4 = (i - 1)*4 + 1;
            write(fid,'(A3,3ES13.5,I3,I8,8ES13.5)') atypestr(abs(atype(i))), &
                x0(i3:i3+2) * globs%box, atype(i), i, &
                x1(i3:i3+2) * globs%box / globs%deltat_fs, xT(i), xq(i4:i4+3)
        enddo
    endif
    
    close(fid, iostat=close_status)
    if ( close_status /= 0 ) then
        print *, 'Error with closing the file. status =', close_status
    endif

    if (pars%debug > 0) print *, 'Exiting print_particles'

end subroutine print_particles


!> @brief Prints all the grid with its resulting field in an xyz file.
subroutine print_grid(els, atoms, filename)
    
    !> Prints all the grid points on an xyz file to be read by ovito. The philimit
    !> and the corresponding field values are also printed.

    type(Electrostatics), intent(in)    :: els !< Electrostatics data structure input
    integer, intent(in)                 :: atoms(:,:,:) !< atoms on the grid
    character(len=*), intent(in)        :: filename !< filename to write
    
    integer, parameter  :: fid = 186541
    integer             :: i, j, k, k2
    
    if (pars%debug > 0) print *, 'Entering print_grid'
    
    print *, 'writing the whole grid into ', trim(filename)
    open(fid, file = trim(filename), action = 'write')

    write(fid,*) (els%zbounds(2) + 5) * globs%isize(1) * globs%isize(2)
    write(fid,*) 'x, y, z, i, j, k, atom, philimit, Ex, Ey, Ez, Enorm, Charge', &
        'Properties=pos:R:3:gp:I:3:atom:I:1:philim:I:1:force:R:3:Enorm:R:1:charge:R:1' 

    do k = 1, els%zbounds(2) + 5
        k2 = k - els%zbounds(1) + 1 !k2 runs in Efield coordinates 
        do j = 1, globs%isize(2)
        do i = 1, globs%isize(1)
            if (k2 > 0) then
                write(fid,"(3ES13.5,5I10,5ES13.5)") &
                    (i-0.5) * els%grid_spacing(1), (j-0.5) * els%grid_spacing(2), &
                    (k2-0.5) * els%grid_spacing(3), i, j, k, atoms(i,j,k), &
                    els%philimit(i, j, k), els%Efield(i, j, k2, :), &
                    els%charge(i, j, k2)
            else
                write(fid,"(3ES13.5,5I10,5ES13.5)") &
                    (i-0.5) * els%grid_spacing(1), (j-0.5) * els%grid_spacing(2), &
                    (k2-0.5) * els%grid_spacing(3), i, j, k, atoms(i,j,k), &
                    els%philimit(i, j, k), 0.d0, 0.d0, 0.d0, 0.d0, 0.d0 
            endif
        enddo; enddo; 
    enddo

    close(fid)
    
    if (pars%debug > 0) print *, 'Finished grid printing. Closing file.'

end subroutine print_grid


subroutine error_catch(els, x0, x1, xT, xq, atoms, atype, msg)
    
    !> When called prints error messages and print all the infrormation of the   
    !> current frame (particles, partsgrid, grid)

    type(Electrostatics), intent(in)    :: els !< Electrostatics data structure input 
    real(dp), intent(in)                :: x0(:), x1(:), xq(:), xT(:) 
    !< Atom coordinates [x1, y1, z1, x2, y2, z2, ...] and velocities (parcas units)
    integer, intent(in)                 :: atoms(:,:,:) !< Atoms placed on a grid
    integer, intent(in)                 :: atype(:)!< atom types
    character(len=*), intent(in)        :: msg !< error message
    
    print '("Error occured in timestep :",I6,". Message: ")', globs%nsteps 
    print *, trim(msg)
    print *, 'printing all grid and particle information...'
    
    call print_particles(els, x0, x1, xT, xq, atoms, atype, 'particles_error.xyz', 0)
    call print_grid(els, atoms, 'grid_error.xyz')

end subroutine error_catch
    
    
!> @brief Prints the parameters
subroutine print_params()

    print *, "Helmod parameters:"
    
    print *, 'bulktemp [K]', pars%bulktemp
    print *, 'Erate [eV]', pars%Erate
    print *, 'Nsave', pars%Nsave
    print *, 'startheating', pars%startheating
    print *,
    
end subroutine print_params

end module helmod
