!> @file heating.f90
!> @brief Home of the heating module

!> @mainpage
!> heating is a module that connects GETELEC to HELMOD and FEMOCS. It takes as input 
!> the potential on the grid, and it gives the heating at each emitting point on a 
!> column-like tip. It also solves the heat equation in 1-D.

!> @author Andreas Kyritsakis, University of Helsinki, 2016

module heating

use libfemocs, only: femocs

implicit none

private
public  :: HeatData, Potential, poten_create, heateq, space_charge, &
            set_heatparams, Nsteps, scale_vels, prepare_heat, plot_temp, &
            get_temp_md, print_heat, solve_heat_long, update_heat_data

!parameters that are set probably permanently and are not changed by user:

integer, parameter  :: dp = 8  !< Double precision
integer, parameter  :: sp = 4  !< Single precision
integer, parameter  :: Nr = 64 !< No of interpolation points in emission line
real(dp), parameter :: kBoltz = 8.6173324d-5 !< Boltzmann constant in eV/K
real(dp), parameter :: pi = acos(-1.d0) !< 3.14159
integer, parameter  :: kx = 4, & !< Number of extra knots for interpolation in x
                       ky = 4, & !< Number of extra knots for interpolation in y
                       kz = 4, & !< Number of extra knots for interpolation in z
                       iknot = 0 !< Interpolation parameter (keep 0)

  
!> @brief Global parameters that never change.
type parameters
    real(dp)    :: Flimratio = 0.2d0 !< Minimum field ratio for calling GETELEC.
    !< If F<Flimratio * Fmax then a very rough estimation with FN equation is done.
    real(dp)    :: Jlimratio = 1.d-4 !< Mminimum J ratio to max for full calculation.
    !< If J < Jlimratio * Jmax, GETELEC is called with GTF approximations 
    real(dp)    :: Ilimratio = 2.d-3 !< Minimum current contribution of a slice
    !< as we go down the tip for full calculation.
    real(dp)    :: workfunc = 4.5d0 !< Work function for Field emission
    real(dp)    :: tau = 500.d0 !< Beresden thermostat tau for velocity scaling.
    real(dp)    :: Vappl = 5.d3 !< Total applied voltage (only for space charge)
    real(dp)    :: temp_cap = 4.d4 !< Maximum allowed temperature for MD scaling
    real(dp)    :: L_wf = 2.d-8 !< Lorentz number (Wiedemann-Frantz)[W*Ohm/K^2]
    real(dp)    :: sigma_SB = 5.67d-26 !< Stefan-Boltzmann constant [W/(K^4 * nm^2)]
    real(dp)    :: Cv = 3.45d-6 ! Heat capacity [W*fs/(nm^3 K)]
    real(dp)    :: maxerr_SC = 1.d-2 !< Convergence criterion for SC theta
    real(dp)    :: heateq_dt = 0.05 !< Euler integration of heat equation, dt
    
    integer     :: compmode = 1 !< Mode of calculation for comparison purposes.
    !< 1: full calculation with full implementation of getelec
    !< 2: forcing "blunt" calculation with GTF (takes into account Nottingham)
    !< 3: Not taking into account the Nottingham effect (only Joule heating)
    !< 4: both (2) and (3)
    integer     :: getelec_approx = 1 !< Approximation level that getelec uses for
                                        !< energy integration
    integer     :: Nsavedata = 50 !< Save data every Nsavedata steps
    integer     :: EDmethod = 0 !< Method used for Laplace (0: FDM, 1: FEM)
    integer     :: debug = 0 !< Debugging level

    logical     :: shank = .false. !< If extra shank is going to be input.
    logical     :: printheat = .true. !< If heat parameters are printed on stdout.
    logical     :: space_charge = .true. !< If space_charge is to be used
    logical     :: fse = .true. !< Include Finite Size Effects in resistivity.
    logical     :: cooling = .false. !< if true then heat is cooling and no current assumed
     
end type parameters


!> @brief Heating data. Mainly temperature and deposited heating power.
type    :: HeatData
    real(dp), allocatable   ::  &
                tempinit(:), &  !< Ιnitial temperature distribution (K) 
                tempfinal(:), & !< Final temperature distribution  (K)
                temp_md(:), &   !< Temperature distribution from MD velocities (K)
                Pjoule(:), &    !< Joule heat deposited in each slice [W]
                Icur(:), &      !< Current flowing through every slice [Amps]
                Pnot(:), &      !< Nottingham heat deposited in each slice [W] 
                hpower(:), &    !< Deposited heat density at each slice [W/nm^3]
                hpold(:), &     !< hpower of the previous step
                Area(:), &      !< Cross-sectional area of each slice [nm^2]
                fluxArea(:), &  !< Current flux area for each slice [nm^2]
                radii(:), &     !< Radius of each slice [nm]
                rho(:), &       !< Resistivity of each slice [Ohm * nm]
                Kcond(:), &     !< Thermal conductivity of each slice [W / K * nm]
                Fflux(:), &     !< int(F * J * dS)
                Pthomson(:), &  !< Thomson power [W]
                Pbbr(:)         !< Black body ratiation power [W]
    real(dp)    ::  Tbound, &   !< Boundary bulk temperature (K)
                    dx, &       !< Space step for FDM heat equation (nm)
                    dt = 0.1d0  !< Heat equation timestep
    integer ::  tipbounds(0:2)  !< Indices-bounds of the tip.
    real(dp)    :: temperror    !< RMS change of the temperature after a heating step
    real(dp)    :: Itot         !< Total current running through the base of the tip
    real(dp)    :: Jrep         !< Representative current density 
                                !< Mean J in the region where 1/2 of the Itot comes
    real(dp)    :: Frep         !< Representative field. 
                                !< Mean F in the region where 1/2 of the Itot comes
    real(dp)    :: Fmax         !< Maximum field on the tip
end type HeatData


!> @brief Data defining the potential at all gridpoints.
type    :: Potential 

    real(dp), pointer       :: phi(:,:,:) => null() !< Potential at all gridpoints
    integer, pointer        :: philimit(:,:,:) => null() 
    !< Boundary conditions on the grid
    real(dp), pointer       :: Efield(:,:,:,:) => null() !< Electric field at GP.
    real(dp)                :: grid_spacing(3)  !< Grid spacing in nm
    integer                 :: nx = 0, ny = 0, nz = 0 !< Shape(phi)
    
    real(dp), allocatable   :: bcoef(:,:,:), tx(:), ty(:), tz(:) 
    !< Interpolation data. Set after db3ink is called
    logical                 :: set = .false.
    !< true if interpolation has been set (db3ink called)
    real(dp)                :: timing !< CPU timing needed to set interpolation.
    real(dp)                :: multiplier = 1._dp 
    !< Multiplier that will multiply all electrostatic solutions to account for 
    !< space charge or other effects
end type Potential


!> @brief Data related to the emission in one point. 
type    :: PointEmission
   
    real(dp)    :: F(3) = [1.d0, 1.d0, 1.d0] !< Electric field in V/nm
    real(dp)    :: kT = 5.d-21 !< Thermal energy at the point (eV)
    real(dp)    :: W = 4.5d0 !< Work function at the point (eV)
    real(dp)    :: Jem  !< Ouput: Emission current density at the point (A/nm^2).
    real(dp)    :: heatNot !< Ouput: Nottingham heat at a point. (W/nm^2)
    real(dp)    :: Jmax = 0.d0 !< Maximum current density found up to now.
    real(dp)    :: Fmax = 0.d0 !< Maximum local field encountered up to now.

    integer     :: Nstart(3) !< Indices of starting surface surf_points
    
    real(dp), dimension(Nr) :: &
            rline, &    !< Distance (nm) from the starting point on the line
            Vline, &    !< Line data: Potential on the line points (V)
            xline, &    !< x-coordinate of the line point (nm)
            yline, &    !< y-coordinate of the line point (nm)
            zline       !< z-coordinate of the line point (nm) 
    
    real(dp)                :: timings(7) !< CPU timing variables
    !1: Interpolation set, 2: interpolation evaluate 3: Cur: fitting
    !4: Cur: 1D interpolation set. 
    !5: Cur: J_num_integ with barrier model
    !6: J_num_integ with interpolation
    !7: Cur: GTF
    integer                 :: counts(7)  !< Counting variables (same as timings)
    
    integer                 :: ierr !< Error indicator
    !0 : everything ok
    !10+ getelec ierr : geterlec error in the first not full calculation
    !20+ getelec ierr : getelec error in the second full calculation
    !-1 : line_create gave error
    integer                 :: mode !< Mode of calculation
    ! 0: Full calculation
    ! 1: Rough FN approximation 
    
end type PointEmission


type(Parameters)    :: pars !< Global parameters structure.
integer, save       :: Nsteps !< Number of MD steps (updated from Helmod).

contains


!> @brief Prepares the heating module. Allocates heat and poten data types.
subroutine prepare_heat(heat, poten, tipheight)
    
    use std_mat, only: interp1
    
    type(Potential), intent(in)     :: poten !< Ιnput potential structure.
    type(HeatData), intent(inout)   :: heat !< Heat data structure.
    integer, intent(in)             :: tipheight !< Height of the tip in GPs 
    
    logical         :: intip
    integer         :: i, j, k, ptype, Nshank, Nh, Nl
    real(dp), allocatable, save   :: r_shank(:), z_shank(:)
    real(dp)        :: shank_h, rtemp(2)
    
    integer, parameter  :: fid = 12678
    
    
    if (pars%debug > 0) print *, 'Entering prepare_heat'
    
    heat%tipbounds(1) = 2

    
    if (.not. allocated(heat%tempinit)) then
        if (pars%shank) then
            open(fid,file='in/extension.dat', action='read')
            read(fid, *) Nshank
            allocate(r_shank(Nshank), z_shank(Nshank))
            read(fid, *) z_shank
            read(fid, *) r_shank
            shank_h = minval(z_shank)
            heat%tipbounds(0) = ceiling(0.1d0 * shank_h / poten%grid_spacing(3))
            if (pars%debug > 0) then
                print *, 'rshank =', r_shank, 'z_shank =', z_shank
                print *, 'shank_h =', shank_h, 0.1d0 * shank_h / poten%grid_spacing(3)
                print *, floor(0.1d0 * shank_h / poten%grid_spacing(3))
                print *, 'grid_space =', poten%grid_spacing
            endif
            
        else
            heat%tipbounds(0) = 1
        endif
        
        Nh = poten%nz
        Nl = heat%tipbounds(0) - 1
        
        if (pars%debug > 0) print *, 'allocating temps, Nh = ', Nh, 'Nl = ', Nl
        
        allocate(heat%tempinit(Nl:Nh), heat%tempfinal(Nl:Nh), heat%temp_md(Nl:Nh), &
            heat%Pjoule(Nl:Nh), heat%Pnot(Nl:Nh), heat%Icur(Nl:Nh), &
            heat%hpower(Nl:Nh), heat%hpold(Nl:Nh), heat%Area(Nl:Nh), &
            heat%fluxArea(Nl:Nh), heat%Fflux(Nl:Nh), heat%radii(Nl:Nh), &
            heat%rho(Nl:Nh), heat%Kcond(Nl:Nh), heat%Pthomson(Nl:Nh), &
            heat%Pbbr(Nl:Nh))
        
        heat%tempinit = heat%Tbound
        heat%tempfinal = heat%Tbound
        heat%temp_md = heat%Tbound
        heat%Pjoule = 0.d0
        heat%Pbbr = 0.d0
        heat%Pthomson = 0.d0
        heat%Icur = 0.d0
        heat%Pnot = 0.d0
        heat%hpower = 0.d0
        heat%hpold = 0.d0
        heat%Area = 1.d-20
        heat%rho = 0.d0
        heat%Kcond = 0.d0
        heat%radii = 0.d0
        
        if (pars%shank) then
            do k = heat%tipbounds(0), heat%tipbounds(1)-1
                rtemp = interp1(.1 * abs(z_shank), .1 * r_shank, &
                                                    abs(k) * poten%grid_spacing(3))
                heat%radii(k) = rtemp(1)
                heat%Area(k) = pi * heat%radii(k)**2
                heat%fluxArea(k) = 2 * pi * heat%radii(k) * heat%dx
            enddo
            heat%Area(heat%tipbounds(1):heat%tipbounds(1) + 1) = &
                                                    heat%Area(heat%tipbounds(1) - 1)
        endif
    endif
    
    if (heat%tipbounds(2) > tipheight) then
        heat%tempinit(heat%tipbounds(2)+1:tipheight) = &
                                                    heat%tempinit(heat%tipbounds(2))
        heat%tempfinal(heat%tipbounds(2)+1:tipheight) = &
                                                    heat%tempfinal(heat%tipbounds(2))
        heat%hpower(heat%tipbounds(2)+1:tipheight) = &
                                                    heat%hpower(heat%tipbounds(2))
        heat%hpold(heat%tipbounds(2)+1:tipheight) = &
                                                    heat%hpold(heat%tipbounds(2))
    endif
    
    
    heat%tipbounds(2) = tipheight
    if (.not. pars%shank) heat%tipbounds(0) = heat%tipbounds(1)
    
    print *, 'Exiting prepare_heat. new tipbounds', heat%tipbounds

end subroutine prepare_heat


!> @brief Calculates the deposited heat distribution on the tip.
subroutine get_heat(heat, poten, fem)
    !> @brief Finds surfacepoints from poten, calculates current and Nottingham at 
    !> each one and calculates deposited heat density at each grid z slice.
    
    type(Potential), intent(in)     :: poten !< Ιnput potential structure.
    type(HeatData), intent(inout)   :: heat !< Heat data structure.
    type(femocs), intent(in)        :: fem !< Femocs data structure.
        
    type(PointEmission)             :: point !< 
               
    integer                         :: i, j, k, icount, ptype, Nerrors, Ncorrect, &
                                        ii, plim_nbors(6), Nl, Nh, khalf 
    real(dp)                        :: direc(6), faces(6), fluxes(6), dx, dy, dz, &
                                        R_fse, dI, dIk, dS

    integer, save                   :: Ncalls = 0
    character(len = 100)            :: command
    logical                         :: ex
    
    if (pars%debug > 0) print *, 'entering get_heat'
    
    dx = poten%grid_spacing(1)
    dy = poten%grid_spacing(2)
    dz = poten%grid_spacing(3)
    
    point%W = pars%workfunc
    point%mode = 0
    heat%dx = dz
    point%Fmax = 0.d0
    point%Jmax = 0.d0
    Nerrors = 0 !initialize to zero the number of encountered errors
    Ncorrect = 0
    do k = heat%tipbounds(2), heat%tipbounds(1), -1 !iterate over all k layers (z=z0)
        heat%Pnot(k) = 0.d0 !Counts from zero (every slice's heat)
        heat%Area(k) = 0.d0
        if (k < heat%tipbounds(2)) then 
            heat%Icur(k) = heat%Icur(k+1)!upwards current is accumulative
            heat%Fflux(k) = heat%Fflux(k+1) ! So is Frepresentative
        else
            heat%Icur(k) = 0.d0
            heat%Fflux(k) = 0.d0    
        endif
        heat%fluxArea(k) = 0.d0 
        point%kT = kBoltz * heat%tempinit(k) 
        do j=2,poten%ny-1
        do i=2,poten%nx-1
            faces = [dy*dz, -dy*dz, dx*dz, -dx*dz, dx*dy, -dx*dy]
            ptype = classify_point(poten, i,j,k)
            if (ptype == 0 .and. .not. pars%cooling) then !surface point
                point%Nstart = [i, j, k]
                call emit_atpoint(poten, fem, point)
                if (point%ierr /= 0) then !something wrong in J coming out
                    if (pars%debug > 0) &
                        print *, 'WARNING: error in point.', i,j,k, &
                                                    'ierr = ', point%ierr
                    heat%Icur(k) = heat%Icur(k) + 1.d-200
                    Nerrors = Nerrors + 1
                else
                    plim_nbors = [poten%philimit(i+1,j,k), poten%philimit(i-1,j,k), &
                            poten%philimit(i,j+1,k), poten%philimit(i,j-1,k), &
                            poten%philimit(i,j,k+1), poten%philimit(i,j,k-1)]
                    do ii = 1, 6
                        if (plim_nbors(ii) == -10) faces(ii) = 0.d0
                    enddo
                    !determine the directions of the emission vectors (same as F)
                    do ii = 0, 2
                        direc(ii*2+1 : ii*2+2) = point%F(ii+1)
                    enddo
                    direc = direc / norm2(point%F)
                    fluxes = max(direc * faces, [0.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.d0])
                    !direc = [Fx, Fx, Fy, Fy, Fz, Fz] / |F|
                    dS = sum(fluxes)
                    dI = point%Jem * dS
                    
                    !add Not heat (W)
                    heat%Pnot(k) = heat%Pnot(k) + point%heatNot * dS 
                    ! add Current (A) 
                    heat%Icur(k) = heat%Icur(k) + dI
                    ! add 
                    heat%Fflux(k) = heat%Fflux(k) + norm2(point%F) * dI
                    heat%fluxArea(k) = heat%fluxArea(k) + dS 
                    ! increase slice area [nm^2]
                    Ncorrect = Ncorrect + 1
                endif
            endif
            heat%Area(k) = heat%Area(k) + dx * dy !Area is added
        enddo; enddo
        if (pars%compmode >= 3) heat%Pnot(k) = 0.d0
        if(heat%Area(k) > 1.d-20) then
            heat%radii(k) =  sqrt(heat%Area(k) / pi)
        else
            print *, "Warning: tipbounds is wrong. Area=0. Updating tipbounds(2)"
            heat%tipbounds(2) = k - 1
        endif
        
        if (k < heat%tipbounds(2)) then 
            dIk = heat%Icur(k) - heat%Icur(k+1) !Current contribution by this slice
            if (dIk  < pars%Ilimratio * heat%Icur(k)) point%mode = 1
            ! Switch to rough calculation mode
        endif
    enddo
    
    heat%Itot = heat%Icur(heat%tipbounds(1))
    
    if (Ncalls == 0) then
        inquire(file='in/rho_table.dat.in', exist=ex)
        if (ex) then
            print *, 'resistivity data file already exists'
        else
            if (pars%fse) then
                R_fse = heat%radii(ceiling(.5*(heat%tipbounds(0)+heat%tipbounds(2))))
            else ! give bulk size (very big)
                R_fse = 5000.
            endif
            print *, 'Running java simulation to get the resistivity &
                for a nanowire of diameter',  2* R_fse, 'nm'
            write(command,*) "java -cp javafse Copper", 2 * R_fse
            call system(trim(command))
        endif
    endif
    
    khalf = heat%tipbounds(1)
    do k = heat%tipbounds(2), heat%tipbounds(0), -1
        heat%rho(k) = resistivity(heat%tempinit(k))
        if (heat%Icur(k) > heat%Itot * 0.5 .and. k > khalf ) khalf = k
    enddo
    
    heat%Kcond = pars%L_wf * heat%tempinit / heat%rho
    heat%Frep = heat%Fflux(khalf) / heat%Icur(khalf)
    heat%Jrep = heat%Icur(khalf) / sum(heat%fluxArea(khalf:heat%tipbounds(2)))
    heat%Fmax = point%Fmax
    heat%Pnot(heat%tipbounds(0):heat%tipbounds(1)-1) = 0.d0
    heat%Icur(heat%tipbounds(0):heat%tipbounds(1)-1) = heat%Icur(heat%tipbounds(1))
    
    
    if (pars%printheat) then
        print '(A15,F13.4,A10)', 'Multiplier =', poten%multiplier
        print '(A15,F13.5,A10)', 'TipHeight =', &
            (heat%tipbounds(2) - heat%tipbounds(0)) * poten%grid_spacing(3), 'nm'
        print '(A15,3I10)', 'TipBounds =', heat%tipbounds
        print '(A15,3I10)', 'khalf     =', khalf
        print '(A15,ES13.5,A10/A15,ES13.5,A10)', 'Jrep =', &
                heat%Jrep,  'A/nm^2', &
                'Pnot =', sum(heat%Pnot(heat%tipbounds(1) : heat%tipbounds(2))), 'W'
        print '(A15,F13.5,A10)', 'Fmax =', point%Fmax, 'V/nm'
        print '(A15,F13.5,A10)', 'Frep =', heat%Frep, 'V/nm'
        print '(A15,ES13.5,A10)', 'Itot =', heat%Itot, 'A'
        print '(A15,I12,A1,I10,/)', 'Nerr / Ncorr =', Nerrors , '/', Ncorrect
        print '(A15,ES13.5,A10)', 'J_mdbase =', &
            heat%Itot / heat%Area(heat%tipbounds(1)), 'A/nm^2' 
    endif

    Ncalls = Ncalls + 1
    
    if (pars%debug > 0) print *, 'Exiting get_heat'

end subroutine get_heat


!> @brief Iteratively recalculates get_heat to take into account the space charge.
subroutine space_charge(heat, poten, fem)

    !> @brief Finds surfacepoints from poten, calculates current and Nottingham at 
    !> each one and calculates deposited heat density at each grid z slice.
    
    type(Potential), intent(inout)  :: poten !< Ιnput potential structure.
    type(HeatData), intent(inout)   :: heat !< Heat data structure.
    type(femocs), intent(in)        :: fem !< Femocs data structure.
    
    integer     :: i, j
    real(dp)    :: J_p, F_p, theta_new, theta_old, error, err_fact

    
    if (pars%debug > 0) print *, 'Entering space_charge'
    err_fact = 0.5_dp
    theta_old = poten%multiplier
    
    do i = 1, 3
        do j = 1, 3
            call get_heat(heat, poten, fem)
            if (.not. pars%space_charge) return
            J_p = heat%Jrep
            F_p = heat%Frep
            theta_new = theta_SC(J_p, pars%Vappl, F_p)
            error =  theta_new - theta_old
            theta_new = theta_old + error * err_fact
            theta_old = theta_new
            poten%multiplier = theta_new
            if (abs(error) < pars%maxerr_SC) then
                print *, 'SC converged. Error = ', error
                return 
            endif
        enddo
        err_fact = err_fact * 0.5
    enddo
    
    if (pars%debug > 0) print *, 'Exiting space_charge'


end subroutine space_charge


!> Calculates the space charge reduction factor theta
function theta_SC(J, V, F) result(thetout)
    real(dp), parameter     :: k = 1.904e5
    real(dp), intent(in)    :: J, V, F !< Current density, total Voltage, local Field
    real(dp)                :: z, theta(2), thetout
    real(sp)                :: poly(3), work(8)
    complex(sp)             :: rt(2)
    integer                 :: ierr 
    
    z = k * sqrt(V) * J / F**2
    
    if (z < 1.e-3) then ! if z is too small the polynomial solution with sp fails
        thetout = 1.d0 - 4.d0 * z / 3.d0 + 3.d0 * z**2 !use asymptotic approximation
        print *, 'zeta= ', z, 'theta= ', thetout
        return
    endif
    
    poly =  real([9. * z**2, -3.d0, -4. * z + 3.], sp)
    call RPQR79(2, poly, rt, ierr, work)
    
    if (ierr /= 0) then
        print *,'polynomial root finder in theta_SC returned error.'
        print *, 'poly = ', poly
    endif
    
    theta = real(rt,dp)
    
    if (abs(theta(1) - 2./3.) < abs(theta(2) - 2./3.)) then
        thetout = theta(1)
    else
        thetout = theta(2)
    endif
    print *, 'zeta= ', z, 'theta= ', thetout

end function theta_SC


!> @brief Classifies a grid point in vacuum, surface and bulk. (surface is outside)
function classify_point(poten, i, j, k) result(pointype)

    type(Potential), intent(in)     :: poten !< Ιnput potential structure.

    integer, intent(in) :: i,j,k !point indices
    integer             :: pointype !1: exterior, 0: surface, -1: interior

    if (poten%philimit(i,j,k) == 1) then
        pointype = 1
    elseif (poten%philimit(i,j,k) == -10) then
        if (poten%philimit(i+1,j,k) == 1 .or. poten%philimit(i-1,j,k) == 1 .or. &
            poten%philimit(i,j+1,k) == 1 .or. poten%philimit(i,j-1,k) == 1 .or. &
            poten%philimit(i,j,k+1) == 1 .or. poten%philimit(i,j,k-1) == 1) &
            then
            pointype = 0
        else
            pointype = -1
        endif
    else
        pointype = 1
    endif
end function classify_point


!> @brief Calculates emission current and nottingham heating in a grid point.
subroutine emit_atpoint(poten, fem, point)
    
    !> For a given point calculates the line on the direction of the field, gets
    !> the potential distribution on that line and calls GETELEC to find the current
    !> density and deposited Nottingham heat on that point.
    !> Carefull: every distance in this function is in nm. 

    use std_mat, only: linspace, lininterp
    use GeTElEC, only: EmissionData, cur_dens, print_data
    
    type(Potential), intent(in)         :: poten !< Ιnput potential structure.
    type(femocs), intent(in)            :: fem !< Femocs data structure.
    type(PointEmission), intent(inout)  :: point !< Point data struct
    
    type(EmissionData), save    :: that !emission data structure
    
    integer     :: i, j, k, istart, jstart, kstart, xt(1), yt(1), zt(1), Vr(1)
       
    real(dp)    :: direc(3), Fnorm, rmax, rmin
    !direc: direction of field, rmax: maximum distance for rline. All in nm
    real(dp)    :: dx, dy, dz !grid spacing in nm
    logical     :: firsttime = .true.
    real(dp)    :: t1, t2  
    integer, save   :: Ncalls = 0
    
    if (pars%debug > 1) then
        print *, 'entering emit_atpoint,[i,j,k] =', point%Nstart
        call cpu_time(t1)
    endif
    
    Ncalls = Ncalls + 1
    point%ierr = 0
    istart = point%Nstart(1) 
    jstart = point%Nstart(2) 
    kstart = point%Nstart(3)
    dx = poten%grid_spacing(1) 
    dy = poten%grid_spacing(2) 
    dz = poten%grid_spacing(3)
    
    if (.not. poten%set .and. pars%EDmethod == 0) then
        print *, 'Error: interpolation not set'
        stop
    endif

    if (pars%EDmethod == 0) then
        point%F = poten%multiplier *  ([poten%phi(istart+1,jstart,kstart), &
                 poten%phi(istart,jstart+1,kstart), &
                 poten%phi(istart,jstart,kstart+1)] - &
                 [poten%phi(istart-1,jstart,kstart), &
                 poten%phi(istart,jstart-1,kstart), &
                 poten%phi(istart,jstart,kstart-1)]) / [dx, dy, dz]
        Fnorm = norm2(point%F)
    else
        point%F = -10.d0*poten%multiplier * poten%Efield(istart, jstart, kstart, 1:3)
        Fnorm = 10.d0 * poten%multiplier * poten%Efield(istart, jstart, kstart, 4)
    endif
        
    if (Fnorm < 1.d-5) then
        point%ierr = 1
        return
    endif

    if (Fnorm > point%Fmax) point%Fmax = Fnorm !keep the maximum field stored in Fmax
    that%F = Fnorm
    that%approx = pars%getelec_approx
    if (Fnorm >= point%Fmax * pars%Flimratio .and. point%mode == 0 .and. &
                    (pars%compmode == 1 .or. pars%compmode == 3)) then
        that%F = Fnorm
        call line_create()
        
        if (any(that%Vr> 30.d0 .or. that%Vr<0.d0)) then
            if (pars%debug > 0) then
                print *, 'Warning: Vr has wrong values. error catching. max(Vr) =', &
                            maxval(that%Vr)
                if (pars%debug > 1) call print_data(that,.true.)
            endif
            that%Vr(size(that%Vr)) = 0.d0
        endif
                    
        if (that%Vr(size(that%Vr)) > point%W) then
            that%mode = -21 !if ok use polyfit trial
        elseif (that%Vr(size(that%Vr)) > 0.2d0*point%W)  then 
            that%mode = -10 ! else use model fitting
        else
            that%R = 1.d4
            that%gamma = 1.1d0
            that%mode = -1
            point%ierr = -1
        endif
    else
        that%R = 1.d4
        that%gamma = 1.1d0
        that%mode = -1  
    endif
    
    if (pars%debug > 1) then
        call cpu_time(t2)
        point%timings(2) = point%timings(2) + t2 - t1
        point%counts(2) = point%counts(2) + 1
    endif
    
    that%W = point%W
    that%kT = point%kT
    that%approx = pars%getelec_approx
    
    call cur_dens(that)
    
    
    if (that%ierr /= 0) then
        if (pars%debug > 0) then
            print *, 'Error in getelec cur_dens 1st call at point', point%Nstart
        endif
        point%ierr = 100 + that%ierr
        !catch error. calculate with simple FN
        that%R = 1.d4
        that%gamma = 1.1d0
        that%mode = -1
        call cur_dens(that)
    endif
    
    if (that%Jem > point%Jmax * pars%Jlimratio .and. point%mode == 0 .and. &
                    (pars%compmode==1 .or. pars%compmode==3) .and.point%ierr==0) then
        that%approx = pars%getelec_approx
        call cur_dens(that)
    
        if (that%ierr /= 0) then
            if (pars%debug > 0) &
                print *, 'Error in getelec cur_dens 2nd call at point', point%Nstart
            point%ierr = 200 + that%ierr
        endif
    endif
    
    point%Jem = that%Jem
    point%heatNot = that%heat
    do i =1,5 !copy timings and counts from that
        point%timings(i+2) = that%timings(i)
        point%counts(i+2) = that%counts(i)
    enddo
    point%timings(1) = poten%timing
    point%counts(1) = 1
    if (point%Jem > point%Jmax) point%Jmax = point%Jem
    
    contains
    

    subroutine line_create()
    
        real(dp), dimension(2*Nr)     :: rline, Vline, xline, yline, zline
        integer                     :: i,j,  Nstart, Nend, badcondition
        logical                     :: neg
        real(dp), dimension(1)      :: xmax, ymax, zmax, Vmax
        integer, dimension(2*Nr)      :: iflag
        
        if (pars%debug > 1) print *, 'entering line_create'
        
        direc = point%F / Fnorm
        
        if (firsttime) then
            allocate(that%xr(2*Nr), that%Vr(2*Nr))
            firsttime = .false.
        endif
        
        if (pars%EDmethod == 0) then
            rmax = (point%W / Fnorm) * 0.1d0
            do i = 1, 100!search for length from tip where V>Work function (full bar)
                xmax = dx * (istart-0.5) + rmax * direc(1)
                ymax = dy * (jstart-0.5) + rmax * direc(2)
                zmax = dz * (kstart-0.5) + rmax * direc(3)
                call phi_atpoints(poten, fem, xmax, ymax, zmax, Vmax, iflag)
                if (iflag(1) /=0) then  !interpolation error, probably out of bounds
                    rmax = rmax / 1.1d0
                    badcondition = 1
                    if (pars%debug > 0) &
                        print *, 'error in bspline interpolation. iflag = ', iflag(1)
                    exit
                endif
                if (Vmax(1) > 1.1d0*point%W) exit
                rmax = 1.1d0 * rmax
            enddo
            
            if (badcondition /= 0) &  !recalculate for the last point
                call phi_atpoints(poten, fem, xmax, ymax, zmax, Vmax, iflag)

            if (Vmax(1) < 0.2d0*point%W)  then ! use just field
                that%F = Fnorm
                that%R = 1.d4
                that%gamma = 1.1d0
                that%mode = -1
            else
                point%rline = linspace(0.d0, rmax, Nr)
                !set the line of interpolation and interpolate
                point%xline = dx * (istart-0.5) + point%rline * direc(1)
                point%yline = dy * (jstart-0.5) + point%rline * direc(2)
                point%zline = dz * (kstart-0.5) + point%rline * direc(3)
            
                call phi_atpoints(poten, fem, point%xline, point%yline, &
                                            point%zline, point%Vline, iflag)
                that%xr = point%rline - point%rline(1)
                that%Vr = point%Vline - point%Vline(1)
                
            endif
        else
            rmin  = -1.d0
            rmax =  3.d0 * that%W / Fnorm
            rline = linspace(rmin, rmax, 2 * Nr)
            xline = dx * (istart-0.5) + rline * direc(1)
            yline = dy * (jstart-0.5) + rline * direc(2)
            zline = dz * (kstart-0.5) + rline * direc(3)
            call phi_atpoints(poten, fem, xline, yline, zline, Vline, iflag)
            
            if (Vline(2*Nr) < 1.1 * point%W) &
                print *, "Warning: Vline not sufficient. Vline(1,end)=", &
                    Vline(1), Vline(2*Nr)
            
            Nstart = 0
            Nend = 0        
            do i = 1, 2*Nr
                if (Vline(i) > 1.e-20 .and. Nstart == 0) Nstart = i
                if (Vline(i) > 1.1 * point%W) then
                    Nend = i
                    exit
                endif
            enddo
            
            if (pars%debug > 1)  then
                print *, 'Nstart =', Nstart, 'Nend =', Nend
                print *, 'after interpol.. rmin =', rmin, 'rmax =', rmax, 'Vline:'
                do i = 1, size(rline)
                    print '(a3,a15,a15,a3)', 'i', 'rline', 'Vline', 'iflag'
                    print '(i3,es15.5,es15.5,i3)', i, rline(i), Vline(i), iflag(i)
                enddo
            endif
            
                
            if (Nstart == 0 .or. Nend < Nstart + 5) then
                that%Vr(1) = 300.d0 !the line data are bad. return and make sure that
                if (pars%debug > 0) print *, 'exiting line_create unseccessfully'
                return            ! the point result is error
            endif

            if (size(that%xr) /= (Nend - Nstart + 1)) then
                if (allocated(that%xr)) deallocate(that%xr, that%Vr)
                allocate(that%xr(Nend - Nstart + 1), that%Vr(Nend - Nstart + 1))
            endif
            that%xr = rline(Nstart:Nend) - rline(Nstart)
            that%Vr = Vline(Nstart:Nend) - Vline(Nstart)
            
            !conditioning vline... get rid of non-monotonous points
            neg = .false.
            do i = 2, size(that%xr)
                if (that%Vr(i) < that%Vr(i-1)) then
                    neg = .true.
                    if (pars%debug > 1) print *, 'Warning: Vr non monotonous in i=',i
                        
                    do j = i + 1, size(that%xr)
                        if (that%Vr(j) > that%Vr(i-1)) exit
                    enddo
                    if (j == size(that%xr) + 1) exit
                    do k = i, j
                        that%Vr(k) = lininterp([that%Vr(i-1), that%Vr(j)], &
                                that%xr(i-1), that%xr(j), that%xr(k))
                    enddo
                endif
            enddo
            if (neg .and. pars%debug > 1) then
                print *, 'After treating. Printing GETELEC data.'
                call print_data(that,.true.)
            endif
        endif
        
        if (pars%debug > 1) print *, 'exiting line_create'
        
    end subroutine line_create
    
end subroutine emit_atpoint


!> @brief Gets the potential at a given list of points by interpolation.
subroutine phi_atpoints(poten, fem, x, y, z, phi, iflag) 
    use bspline, only: db3val
    
    type(Potential), intent(in) :: poten !< Ιnput potential structure.
    type(femocs), intent(in)    :: fem !< Femocs data structure.
    
    real(dp), intent(in)        :: x(:), y(:), z(:)
    
    real(dp), intent(out)       :: phi(:)
    integer, intent(out)        :: iflag(:)
    
    integer                     :: i, Nr
    
    integer                     :: inbvx=1, inbvy=1, inbvz=1, iloy=1, iloz=1
    integer                     :: idx=0, idy=0, idz=0, flag 
    !interpolation parameters       
    
    Nr = size(x)
    if(pars%EDmethod == 0) then
        do i=1, Nr !interpolate for all points of rline
            call db3val(x(i), y(i), z(i), idx, idy, idz, poten%tx, poten%ty,  &
                    poten%tz, poten%nx, poten%ny, poten%nz, kx, ky, kz,&
                    poten%bcoef, phi(i), iflag(1), inbvx, inbvy, inbvz, iloy, iloz)
        enddo
    elseif(pars%EDmethod == 1) then
        call fem%interpolate_phi(flag, Nr, 10.*x, 10.*y, 10.*z, phi, iflag)
    else
        print *, 'Wrong EDmethod parameter'; stop
    endif
    phi = poten%multiplier * phi

end subroutine phi_atpoints


!> @brief Solves the heat equation in one dimension, new version
subroutine heateq(heat, solvesteps)
    !> Solves the heat equation by a simple Euler method, taking into account
    !> the varying heat conductivity and the varying cross section of the tip

    type(HeatData), intent(inout)   :: heat
    integer, intent(in)             :: solvesteps !< Nr of steps to be evolved


    real(dp)    :: deltaT, term1, term2

    integer :: i, k
    
    if (pars%debug > 0) print *, 'Entering heateq'
    
    !update tempfinal
    heat%tempfinal = heat%tempinit

    do i = 1, solvesteps
        do k = heat%tipbounds(0)+1, heat%tipbounds(2) !go through slices except first 
            term1 = heat%Area(k+1)*heat%Kcond(k+1) - heat%Area(k-1)*heat%Kcond(k-1)
            term1 = 0.25d0 * term1 * (heat%tempfinal(k+1) - heat%tempfinal(k-1)) &
                    / (heat%Area(k) * heat%dx**2)
            term2 = heat%tempfinal(k+1) + heat%tempfinal(k-1) - 2*heat%tempfinal(k)
            term2 = term2 * heat%Kcond(k) / heat%dx**2
            
            deltaT = (heat%hpower(k) + term1 + term2) * heat%dt / pars%Cv
            heat%tempfinal(k) =  heat%tempfinal(k) + deltaT
        enddo
        heat%tempfinal(heat%tipbounds(2)+1) = heat%tempfinal(heat%tipbounds(2))
    enddo
    
    if (any(isnan(heat%tempfinal))) then
        print *, 'Warning: NaN found in heateq. Printing error data file'
        call print_heat(heat,"error")
        heat%tempfinal = heat%tempinit
        stop
    endif

end subroutine heateq


!> @brief Updates conductivities, Thomson and black body radiation
subroutine update_heat_data(heat)
    
    type(HeatData), intent(inout)   :: heat
    real(dp)    ::  sigma
    integer     :: k

    !update rho Kcond and Thomson power
    do k = heat%tipbounds(0), heat%tipbounds(2) 

        sigma = -2.9d-7 + 5.44d-9 * heat%tempfinal(k)
        heat%Pthomson(k) = .5d0 * (heat%tempfinal(k+1) - heat%tempfinal(k-1)) * &
                heat%Icur(k) * sigma
    enddo
    
    do k = lbound(heat%rho,1), ubound(heat%rho,1)
        heat%rho(k) = resistivity(heat%tempfinal(k))
    enddo
    
    !update Kcond, Joule, bbr and Thomson
    heat%Kcond = pars%L_wf * heat%tempfinal / heat%rho
    heat%Pjoule =  heat%rho * heat%dx * heat%Icur**2 / heat%Area
    heat%Pbbr = - pars%sigma_SB * heat%fluxArea * heat%tempfinal**4
    
    !update total power
    heat%hpower =  (heat%Pjoule + heat%Pnot + heat%Pbbr + heat%Pthomson) / &
                    (heat%Area * heat%dx)
end subroutine update_heat_data



!> @brief Solves the heat equation until steady state is reached
subroutine solve_heat_long(heat, poten, fem)

    type(Potential), intent(inout)     :: poten !< Ιnput potential structure.
    type(HeatData), intent(inout)   :: heat !< Heat data structure.
    type(femocs), intent(in)        :: fem !< Femocs data structure.

    real(dp), parameter     :: convcr = 5.d-7
    !< Maximum temperature change for the heat equation reaching steady state  [K]
    
    real(dp)                :: time = 0.d0, Tmax, Terror  ! Time in ps
    real(dp), allocatable   :: Tsave(:)


    integer :: i, k
    
    integer, parameter  :: maxsteps = 10000000, runsteps = 1000
    logical             :: recalc
    character(len=80)   :: timestring
    
    allocate(Tsave(size(heat%tempinit)))
    Tsave = heat%tempinit
    
    if (pars%debug > 0) print *, 'Entering solve_heat_long'
    
    heat%dt = pars%heateq_dt
    heat%tempfinal = heat%tempinit !initialize temperature distribution
    
    recalc = .false.
    time = 0.d0
    do i = 1, maxsteps
        if (recalc) then
            call space_charge(heat, poten, fem)
            Tsave = heat%tempfinal
            recalc = .false.
        endif
        
        call update_heat_data(heat)
        call heateq(heat, runsteps)
        time = time + runsteps * heat%dt * 1.d-3
        
        heat%temperror = maxval(abs(heat%tempfinal - heat%tempinit))
        heat%tempinit = heat%tempfinal
        
        Tmax = maxval(heat%tempfinal(heat%tipbounds(0):heat%tipbounds(2))) 
        
        if (Tmax < 1000.) then
            Terror = 500.
        elseif (Tmax < 1250.) then
            Terror = 100.
        elseif (Tmax < 1800.) then
            Terror = 50.
        else
			Terror = 20.
		endif
        if (heat%temperror < 0.01) then
            Terror = 1.
        endif
        
        if (maxval(abs(heat%tempfinal - Tsave)) > Terror) recalc = .true.            
            
        if (mod(i, 1000) == 0) then
            write(timestring, '("t_",i5.5)') floor(time)
            call print_heat(heat, trim(timestring))
        endif
        
        if (mod(i,10) == 0) &
            print *, "t = ", time, "Tmax = ", Tmax, "Terror = ", heat%temperror 

        
        if (heat%temperror < convcr  * runsteps * heat%dt) then
            print *, 'heateq converged. Temperror = ', heat%temperror
            call print_heat(heat, 'final')
            exit
        endif
    enddo
    deallocate(Tsave)
    
end subroutine solve_heat_long



!> @brief Creates potential type from he helmod grid and prepares interpolation.
subroutine poten_create(this, philimit, gridspace, phi, Efield)

    use bspline, only: db3ink

    type(Potential), intent(inout)  :: this  !< Potential data structure
    
    real(dp), intent(in)            :: gridspace(3) !< grid spacing (in A)
    integer, intent(in), target     :: philimit(:,:,:) !< Boundary conditions on grid
    
    real(dp), intent(in), target, optional    :: phi(:,:,:), & !< Potential on a grid
                                        Efield(:,:,:,:) !< Electric field on grid.

    real(dp), allocatable           :: x(:), y(:), z(:)
    integer                         :: i, iflag
    real(dp)                        :: t2, t1 !timing
    
    if (pars%debug > 0) then
        call cpu_time(t1)
        print *, 'entering poten_create'
    endif
    
    this%grid_spacing = .1d0 * gridspace  !!!!! Careful: gridspace comes in in A and 
                                            !! this%grid_spacing is in nm.
    
    
    if(pars%debug > 0) then
        print *, 'gridspace =', this%grid_spacing
        print *, 'incoming gridspace:', gridspace
    endif
    
    this%philimit => philimit
    
    this%nx = size(philimit,1)
    this%ny = size(philimit,2)
    this%nz = size(philimit,3)
    
    if (present(phi)) this%phi => phi
    if (present(Efield)) this%Efield => Efield
    
    if (pars%EDmethod == 0) then
        if (.not. present(phi)) stop 'phi not present'
        if (pars%debug > 0) print *, 'allocating in poten create'
        if (this%nx/=size(x) .or. this%ny/=size(y) .or. this%nz/=size(z)) then
            if (allocated(x)) deallocate(x,y,z)
            allocate(x(this%nx), y(this%ny), z(this%nx))
            if (allocated(this%bcoef)) deallocate(this%bcoef,this%tx,this%ty,this%tz)
            allocate(this%bcoef(this%nx, this%ny, this%nz), this%tx(this%nx + kx), &
                        this%ty(this%ny + ky), this%tz(this%nz + kz))
        endif

        x = [(this%grid_spacing(1)*(i-0.5), i=1,this%nx)]
        y = [(this%grid_spacing(2)*(i-0.5), i=1,this%ny)]
        z = [(this%grid_spacing(3)*(i-0.5), i=1,this%nz)]
        
        if (pars%debug > 0) print *, "Calling db3ink"
        
        call db3ink(x, this%nx, y, this%ny, z, this%nz, this%phi, kx, ky, kz, iknot, &
                    this%tx, this%ty, this%tz, this%bcoef, iflag)
        if (iflag /= 0) then
            print *, 'Something went wrong with interpolation set. iflag=', iflag
            stop 
        endif
        this%set = .true.
    endif
    
    if (pars%debug > 0) then
        call cpu_time(t2)
        this%timing = this%timing + t2 - t1
    endif
    
end subroutine poten_create


!> @brief Calculates resistivity
function resistivity(Temp) result(rho)
    use std_mat, only: interp1
    
    real(dp), intent(in)    :: Temp !< Temperature  in K
    real(dp)                :: rho !< output resistivity in Ohm * nm
    real(dp)                :: intout(2)
    
    real(dp), allocatable, save :: Ti(:), rhoi(:)
    
    integer, save           :: Ntable
    integer                 :: i
    
    integer, parameter      :: fid = 5496851
    logical, save           :: firsttime = .true.
    
    if (firsttime) then
        open(fid, file = "in/rho_table.dat.in", action = "read")
        read(fid,*) Ntable
        print *, 'Ntable = ', Ntable
        allocate(Ti(Ntable), rhoi(Ntable))
        do i = 1, Ntable
            read(fid,*) Ti(i), rhoi(i)
            print *, Ti(i), rhoi(i)
        enddo
        close(fid)
    endif
    
    if (Temp > Ti(Ntable)) then
        rho = rhoi(Ntable)
    elseif (Temp < Ti(1)) then
        rho = rhoi(1)
    else
        intout = interp1(Ti, rhoi, Temp)
        if (intout(2) == 0.d0) rho = intout(1)
    endif

    firsttime = .false.
 
end function resistivity


!> @brief Scales MD velocities according to Berendsen thermostat
subroutine scale_vels(atoms, x1, xT, heat, deltat_fs, delta, box)

    !> Velocity scaling to on a tip to follow the deposited heat. 
    !> Rewritten by A. Kyritsakis.

    type(HeatData), intent(in)  :: heat
    
    integer, intent(in)     :: atoms(:,:,:) !Atoms on the grid 
    real(dp), intent(inout) :: x1(:) !< Atom velocities (parcas units)
    real(dp), intent(out)   :: xT(:) !< Atom temperature (K) (only for visualization)
    real(dp), intent(in)    :: deltat_fs, & !<!Timestep (fs)
                            delta, &  !< Timestep in PARCAS units
                            box(3) !< Box size (Angstrom)

    real(dp)                :: lambdas, sumlambdas, Tscale
    !Ek: Kinetic energy a each k-slice
    !Ek0: Kinetic energy corresponding to temperature at each slice
    !lambdas: scaling factor for each k-slice
    !sumlambdas: sum of lambdas to print the average over tip
    integer             :: ilambdas ! counter of lambdas calcated for ti
    integer             :: i, j, k, m, Nslice
    !m: atom index on the xq velocities vector of parcas
    !Nslice : No of atoms in each slice
    
    sumlambdas = 0.d0
    ilambdas = 0

    do k = 1, size(atoms,3)
        if (pars%shank) then
            Tscale = min((k-1) / 20.d0, 1.) * heat%tempfinal(k)
        else
            Tscale = heat%tempfinal(k)
        endif
        if (heat%temp_md(k) > 1.d-30 .and. Tscale < pars%temp_cap ) then 
        ! if there were any atoms on the slice and the temperature is less than cap
            lambdas = sqrt(1.d0 + (deltat_fs / pars%tau) * &
                (Tscale / heat%temp_md(k) - 1.d0) )
            do j = 1, size(atoms, 2) !loop over all atoms of the slice
                do i = 1, size(atoms, 1)
                    m = atoms(i, j, k)
                    if (m > 0) then
                        xT(m) = Tscale
                        m = (m - 1)*3
                        x1(m + 1 : m + 3) = x1(m + 1 : m + 3) * lambdas
                    endif
                    !scale velocities
                enddo
            enddo
            sumlambdas = sumlambdas + lambdas !Add sumlambdas to keep track
            ilambdas = ilambdas + 1
        end if
    enddo

    print '(A15,ES13.5)', '<lambdas> =', sumlambdas / ilambdas

end subroutine scale_vels


!> @brief Takes MD velocities and gets their z-axis temperature distribution
subroutine get_temp_md(atoms, x1, heat, delta, box)

    !> Velocity scaling to on a tip to follow the deposited heat. 
    !> Rewritten by A. Kyritsakis.

    type(HeatData), intent(inout)  :: heat
    
    integer, intent(in)     :: atoms(:,:,:) !Atoms on the grid 
    real(dp), intent(in)    :: x1(:) !< Atom velocities (parcas units)
    real(dp), intent(in)    :: delta, &  !< Timestep in PARCAS units
                                box(3) !< Box size (Angstrom)

    real(dp)                :: Ek
    !Ek: Kinetic energy a each k-slice

    integer             :: i, j, k, m, Nslice
    !m: atom index on the xq velocities vector of parcas
    !Nslice : No of atoms in each slice

    do k = 1, size(atoms,3)
        Ek = 0.d0 !put initial kinetic to zero for each slice
        Nslice = 0
        do j = 1, size(atoms,2)
            do i = 1, size(atoms,1)
                m = atoms(i, j, k)
                if (m > 0) then !if grid point has atoms
                    m = (m - 1)*3
                    Ek = Ek + ( x1(m + 1)**2 * box(1)**2 + &
                        x1(m + 2)**2 * box(2)**2 + &
                        x1(m + 3)**2 * box(3)**2) / (2.d0 * delta**2)
                    ! add kinetic energy of atom to total slice kinetic energy
                    Nslice = Nslice + 1
                endif
            enddo
        enddo
        
        heat%temp_md(k) = Ek / (kBoltz * Nslice * 1.5d0)
    enddo

end subroutine get_temp_md


!> @brief Reads global parameters from file. All arguments are optional.
subroutine set_heatparams(filename, debug, workfunc, tau, compmode, Flimratio, &
                        Jlimratio, Nsavedata, printheat, EDmethod)
     
    !> If argument exists, the value is taken from it. Otherwise the file named 
    !> "filename" is tried. If nothing, then default values are used.
    
    real(dp), intent(in), optional  :: Flimratio, Jlimratio, tau, workfunc
    integer, intent(in), optional   :: compmode, Nsavedata, EDmethod, debug
    logical, intent(in), optional   :: printheat
    character(len=*), intent(in), optional  :: filename 
    
    type(femocs)            :: fem
    integer                 :: flag, temp
    

    if (present(tau)) pars%tau = tau
    if (present(workfunc)) pars%workfunc = workfunc
    if (present(debug)) pars%debug = debug
    if (present(compmode)) pars%compmode = compmode
    if (present(Flimratio)) pars%Flimratio = Flimratio
    if (present(Jlimratio)) pars%Jlimratio = Jlimratio
    if (present(Nsavedata)) pars%Nsavedata = Nsavedata
    if (present(printheat)) pars%printheat = printheat
    if (present(EDmethod)) pars%EDmethod = EDmethod
    
    if (present(filename)) then
        fem = femocs(trim(filename))
        call fem%parse_double(flag, 'vscale_tau', pars%tau)
        call fem%parse_double(flag, 'workfunc', pars%workfunc)
        call fem%parse_double(flag, 'flimratio', pars%Flimratio)
        call fem%parse_double(flag, 'jlimratio', pars%Jlimratio)
        call fem%parse_double(flag, 'ilimratio', pars%Ilimratio)
        call fem%parse_double(flag, 'V_applied', pars%Vappl)
        call fem%parse_double(flag, 'temp_cap', pars%temp_cap)
        call fem%parse_double(flag, 'lorentz', pars%L_wf) 
        call fem%parse_double(flag, 'maxerr_SC', pars%maxerr_SC)
        call fem%parse_double(flag, 'heateq_dt', pars%heateq_dt)
                
        call fem%parse_int(flag, 'heat_comparison_mode', pars%compmode)
        call fem%parse_int(flag, 'getelec_approx', pars%getelec_approx)
        
        call fem%parse_int(flag, 'shank', temp)
        if (flag == 0) pars%shank = temp /= 0
        call fem%parse_int(flag, 'space_charge', temp)
        if (flag == 0) pars%space_charge = temp /= 0
        call fem%parse_int(flag, 'printheat', temp)
        if (flag == 0) pars%printheat = temp /= 0
        call fem%parse_int(flag, 'fse', temp)
        if (flag == 0) pars%fse = temp /= 0
        
        call fem%delete()
    endif
    
    print *, 'read the following parameters:'
    call print_heatparams()

end subroutine set_heatparams


!> @brief Prints the heat parameters
subroutine print_heatparams()

    print *, 'tau [fs]', pars%tau
    print *, 'workfunc [eV]', pars%workfunc
    print *, 'compmode', pars%compmode
    print *, 'flimratio', pars%Flimratio
    print *, 'Jlimratio', pars%Jlimratio
    print *, 'Ilimratio', pars%Ilimratio
    print *, 'printheat', pars%printheat
    print *, 'Nsavedata', pars%Nsavedata
    print *, 'EDmethod', pars%EDmethod
    print *, 'debug', pars%debug
    print *, 'shank', pars%shank
    print *, 'space_charge', pars%space_charge
    print *, 'Vappl [Volts]', pars%Vappl
    print *, 'getelec_approx', pars%getelec_approx
    print *, 'Lorentz WF', pars%L_wf
    print *, 'maxerr_SC', pars%maxerr_SC
    print *, 'heateq_dt', pars%heateq_dt
    print *,
    
end subroutine print_heatparams


!> @brief Plots temperature distributions
subroutine plot_temp(heat, fsuffix, draft)

    !> Plots 3 temperature distributions. Before and after heat equation, and
    !> and with the one calculated by the MD velocities.
    use pyplot_mod, only: pyplot
    

    type(HeatData), intent(in)  :: heat
    character(len=*), intent(in), optional :: fsuffix !Suffix to be added to the
                                                        !File names
    logical, intent(in), optional   :: draft !< In case a fast plot is needed and shown
    
    integer, parameter          :: font = 32
    type(pyplot)                :: plt
    character(len = 50)         :: fname
    integer                     :: i
    
    if (present(draft) .and. draft) then
        print *, 'plotting draft temperature'
        call plt%initialize(grid=.true.,xlabel='$z[nm]$',ylabel='$T[K]$', &
            figsize=[20,15], font_size=font, title='Temperature distribution', &
            legend=.true.,axis_equal=.false., legend_fontsize=font, &
            xtick_labelsize=font,ytick_labelsize=font,axes_labelsize=font)
        call plt%add_plot([(heat%dx*i, i=heat%tipbounds(0), heat%tipbounds(2))], &
                heat%tempinit(heat%tipbounds(0): heat%tipbounds(2)), &
                label='init', linestyle='b-', linewidth=2)
        call plt%add_plot([(heat%dx*i, i=heat%tipbounds(0), heat%tipbounds(2))], &
                heat%tempfinal(heat%tipbounds(0): heat%tipbounds(2)), &
                label='final', linestyle='r-', linewidth=2)
        call plt%savefig('draft.png', pyfile="draft.py", show = .true.)
        return
    endif
          
    call plt%initialize(grid=.true.,xlabel='$z[nm]$',ylabel='$T[K]$', &
            figsize=[20,15], font_size=font, title='Temperature distribution', &
            legend=.true.,axis_equal=.false., legend_fontsize=font, &
            xtick_labelsize=font,ytick_labelsize=font,axes_labelsize=font)
    call plt%add_plot([(heat%dx*i, i=heat%tipbounds(0), heat%tipbounds(2))], &
            heat%tempinit(heat%tipbounds(0): heat%tipbounds(2)), &
            label='init', linestyle='b-', linewidth=2)
    call plt%add_plot([(heat%dx*i, i=heat%tipbounds(0), heat%tipbounds(2))], &
            heat%tempfinal(heat%tipbounds(0): heat%tipbounds(2)), &
            label='final', linestyle='r-', linewidth=2)
    call plt%add_plot([(heat%dx*i, i=heat%tipbounds(1), heat%tipbounds(2))], &
            heat%temp_md(heat%tipbounds(1) : heat%tipbounds(2)), &
            label='From MD', linestyle='k-', linewidth=2)
            
    write(fname, '("out/temp_",i5.5)') Nsteps
    if (present(fsuffix)) fname = trim(fname) // trim(fsuffix)
    call plt%savefig(trim(fname)//'.png', pyfile=trim(fname)//'.py', show = .false.)
    print *, 'Temperature distribution plot saved succesfully.'
    
    call plt%initialize(grid=.true.,xlabel='$z[nm]$',ylabel='$Heat[W/nm^3]$', &
        figsize=[20,15], font_size=font, title='Heat distribution', &
        legend=.true.,axis_equal=.false., legend_fontsize=font, &
        xtick_labelsize=font,ytick_labelsize=font,axes_labelsize=font)
         
    call plt%add_plot([(heat%dx*i, i=heat%tipbounds(0), heat%tipbounds(2))], &
                    heat%hpower(heat%tipbounds(0):heat%tipbounds(2)), &
                    linestyle='k*', markersize = 12, label = 'Heat', yscale='log')
    if (present(fsuffix)) fname = trim(fname) // trim(fsuffix)
    write(fname, '("out/power_",i5.5)') Nsteps
    call plt%savefig(trim(fname)//'.png', pyfile=trim(fname)//'.py', show = .false.)
    
    print *, 'Heat distribution plot saved succesfully.' 
end subroutine plot_temp


!> @brief Prints all heat data in data file 
subroutine print_heat(heat, fsuffix)

    type(HeatData), intent(in)  :: heat
    character(len=*), intent(in), optional :: fsuffix !Suffix to be added to the
                                                        !File names
    
    integer, parameter          :: fid = 874651
    character(len=50)         :: fname
    integer                     :: i
    
    
    write(fname, '("out/heatdata_",i5.5)') Nsteps
    if (present(fsuffix)) fname = trim(fname) // trim(fsuffix)
    fname = trim(fname) // ".dat"
    
    open(fid,file=trim(fname), action='write')
    write(fid,*) 'Tipbounds = ', heat%tipbounds
    write (fid,'(A3,A11, 12A15)') 'i' , 'z', 'Pjoule', 'Pnot' , 'Pbbr', 'Pthomson', &
        'Icur', 'fluxArea', 'area', 'hpower', 'rho', 'Kcond', 'T_init', 'T_final'
    do i = heat%tipbounds(0), heat%tipbounds(2)
        write (fid,'(i8, 13ES15.7)') i, i * heat%dx, heat%Pjoule(i), &
                heat%Pnot(i), heat%Pbbr(i), heat%Pthomson(i), heat%Icur(i), &
                heat%fluxArea(i), heat%Area(i), heat%hpower(i),  &
                heat%rho(i),  heat%Kcond(i), heat%tempinit(i), heat%tempfinal(i)
    enddo
    close(fid)

end subroutine print_heat
    
end module heating
