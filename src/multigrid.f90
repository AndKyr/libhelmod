!> @file multigrid.f90
!> @brief Home of the multigrid module

!> @author Flyura Djurabekova, University of Helsinki
!> @author Kai Nordlund, University of Helsinki
!> @author Stefan Parviainen, University of Helsinki
!> @author Mikko Lyly, CSC
!
!> Module containing routines for solving the Laplace equation using multigrid methods
!> @todo Get rid of passing size of arrays as parameters to functions since it's not necessary
module multigridmod
integer, parameter :: SOLVE_FULL = 0 !< Solve a full system
integer, parameter :: SOLVE_PARTIAL = 1 !< Solve a subvolume of a full system
private
public :: multigrid

contains

!> @brief Solve Laplace equation using the multigrid method
!>
!> Solves the Laplace equation using the given boundary conditions and applied electric field
SUBROUTINE multigrid(phi, philimit, E0, a_grid, verbose, boundarytype)
  !
  ! Solves the Poisson equation for Parcas as defined in laplace3dsolver
  !
  IMPLICIT NONE
  
  DOUBLE PRECISION, intent(inout) :: phi(:, :, :) !< Electrostatic potential. In: initial guess, out: actual value
  INTEGER, intent(in) :: philimit(:, :, :) !< Boundary conditions
  DOUBLE PRECISION, intent(in) :: e0 !< Applied electric field (V/Å)
  DOUBLE PRECISION, intent(in) :: a_grid(3) !< Grid spacing (Å)
  LOGICAL, intent(in) :: verbose !< Print debug stuff
  integer, intent(in) :: boundarytype !< 0 = full solution, 1 = partial (const. boundaries)

  INTEGER          :: levels
  INTEGER, POINTER :: ni(:), nj(:), nk(:)
  REAL :: tstart, tstop ! Time before and after iteration, for information

  INTEGER :: m1, m2, m3 ! size of the array phi(:,:,:)
  INTEGER :: n1, n2, n3 ! size of the array philimit(:,:,:)

  m1 = size(phi, 1)
  m2 = size(phi, 2)
  m3 = size(phi, 3)

  n1 = size(philimit, 1)
  n2 = size(philimit, 2)
  n3 = size(philimit, 3)

  CALL CPU_TIME(tstart)

  ! Check grid size, density, and compatibility:
  !---------------------------------------------

  ! Calculate the number of levels and grid sizes for MG:
  !------------------------------------------------------
  CALL gridlevels(m1, m2, m3, levels, ni, nj, nk,verbose)

  IF(verbose) THEN
    PRINT *, 'MG: h:', a_grid(1), a_grid(2), a_grid(3)
    PRINT *, 'MG: grid size(i):', ni
    PRINT *, 'MG: grid size(j):', nj
    PRINT *, 'MG: grid size(k):', nk
  END IF

  IF(verbose) THEN
    PRINT *, 'MG: before smoothing: |phi|=', SQRT(SUM(phi*phi))
  END IF

  CALL mg(m1, m2, m3, n3, phi, philimit, e0, a_grid, levels, ni,nj,nk, verbose, boundarytype)

  IF(verbose) THEN
    PRINT *, 'MG: after smoothing: |phi|=', SQRT(SUM(phi*phi))
  END IF
  CALL CPU_TIME(tstop)
  !write (*,*) 'Solvertime: ',tstop-tstart,'s'

CONTAINS

  ! Figure out how many times the box can be divided in half
  SUBROUTINE gridlevels(m1, m2, m3, levels, ni, nj, nk, verbose)
  !
  ! Determines grid sizes for multigrid
  !
    IMPLICIT NONE
    
    ! Number of grid cells (for phi) in each direction
    INTEGER, intent(in) :: m1, m2 ,m3

    ! Number of "subcells" in each grid
    ! index gives "coarsness"
    INTEGER, POINTER :: ni(:), nj(:), nk(:)

    LOGICAL, intent(in) :: verbose

    ! The number of differently sized grids
    ! Also gives the length of the ni,nj,nk-vectors
    INTEGER, intent(out) :: levels

    INTEGER :: i, j, k, n

    n = m1
    ! n = a*2^(i-1), find i, i.e how many times can be divided in half +1
    i = 1
    DO WHILE(MOD(n, 2) == 0)
      i = i + 1
      n = n / 2
    END DO
    
    n = m2
    j = 1
    DO WHILE(MOD(n, 2) == 0)
      j = j + 1
      n = n / 2
    END DO
    
    n = m3
    k = 1
    DO WHILE(MOD(n, 2) == 0)
      k = k + 1
      n = n / 2
    END DO
    
    levels = MIN(i, j)
    levels = MIN(levels, k)

    !levels = 3
    
    IF(verbose) THEN
      PRINT *, 'MG: levels:', levels
    END IF
    
    ! Allocate memory for coarser grids
    ALLOCATE(ni(levels), nj(levels), nk(levels))
    
    ! Example:
    ! Assume we have m1=12, m1<min(m2,m3)
    ! Then levels = 3 (see above)
    ! ni(3-1+1=3) = 12+1 = 13 -> Finest grid has 13 points
    ! ni(3-2+1=2) = 12/2+1 = 7 -> Second grid has 7 points
    ! ni(3-3+1=1) = 12/2/2+1 = 4 -> Coarsest grid has 4 points
    ! So: ni(1)=4, ni(2)=7, ni(3)=13
    ! ni(1) is always even, should be as small as possible

    i = m1
    j = m2
    k = m3
    DO n = 1, levels
      ! Number of points = number of cells + 1
      ni(levels - n + 1) = i + 1
      nj(levels - n + 1) = j + 1
      nk(levels - n + 1) = k + 1
      i = i / 2
      j = j / 2
      k = k / 2
    END DO

  END SUBROUTINE gridlevels
  
END SUBROUTINE multigrid


!============================================================================

SUBROUTINE mg(m1, m2, m3,n3,phi, philimit, e0, a_grid, levels, &
              gsize_k,gsize_j,gsize_i, verbose, boundarytype) ! sic. gsize_k comes first
  !
  !          Solves the Poisson equation in the unit cube
  !           by finite difference and multigrid methods
  !
  !             Written by: Mikko Lyly, 23. June 2008
  !
  IMPLICIT NONE

  INTEGER          :: m1, m2, m3,n3
  DOUBLE PRECISION,intent(inout) :: phi(m1, m2, m3)
  INTEGER          :: philimit(m1, m2, n3)  
  DOUBLE PRECISION :: a_grid(3)
  DOUBLE PRECISION :: e0
  INTEGER          :: levels
  INTEGER          :: gsize_i(levels),gsize_j(levels),gsize_k(levels)! grid sizes ( ni in multigrid() )
  LOGICAL          :: verbose
  integer          :: boundarytype
  !
  !                      Control parameters:
  !                     ---------------------
  !
  LOGICAL :: multigrid          = .true. !FALSE means only Gauss-Seidel
!  LOGICAL, PARAMETER :: multigrid          = .false. !FALSE means only Gauss-Seidel

  ! "Magic" numbers, TODO: Benchmark to find the optimal values
  INTEGER, PARAMETER :: presmoothing       = 1 ! Numer of Gauss-Seidel iterations
  INTEGER, PARAMETER :: cycles             = 2 ! 1 = V-cycle, 2 = W-cycle
  INTEGER, PARAMETER :: postsmoothing      = 1 ! 1 seems to be the best value

  ! This has to be large enough because it is used to solve the coarsest grid
  ! If it is too small a good solution is not found for the coarse grid
  ! and the algorithm fails (error keeps growing)
  ! But the algorithm gets slower if this number is increased
  ! A value of -1 means to use a good "guesstimate"
  INTEGER :: directsmoothing               = 17

  INTEGER, PARAMETER :: maxiter            = 500 ! Ensure the algorithm stops
                                                 ! Usually <50 is enough

  !DOUBLE PRECISION, PARAMETER :: tolerance = 1.0d-9
  !DOUBLE PRECISION, PARAMETER :: tolerance = 1.0d-6
  DOUBLE PRECISION, PARAMETER :: tolerance = 1.0d-20 ! Stop when residual
                                                     ! gets smaller then this ...

  ! NOTE: Setting dphiconv in clic.f90 does not affect the value here
  ! dphiconv must be set separately in clic.f90 and multigrid.f90
  !DOUBLE PRECISION, PARAMETER :: dphiconv = 1.0d-9 ! ... or stop when dphi gets
  !DOUBLE PRECISION, PARAMETER :: dphiconv = 1.0d-4 ! ... or stop when dphi gets
  DOUBLE PRECISION, PARAMETER :: dphiconv = 1.0d-9 ! ... or stop when dphi gets
                                                    ! smaller then this (same as in
                                                    ! jacobi method.

  LOGICAL, PARAMETER :: setphilimit        = .TRUE.

  ! Types and variables:
  !---------------------
  TYPE multigrid_t
    DOUBLE PRECISION, POINTER :: DATA(:,:,:)
  END TYPE multigrid_t

  TYPE(multigrid_t), POINTER :: xgrid(:) ! multigrid for potential. Q: The order of
                                         ! the indices are different then in phi, is
                                         ! there any special reason for this?
  TYPE(multigrid_t), POINTER :: fgrid(:) ! multigrid for source
  TYPE(multigrid_t), POINTER :: ggrid(:) ! multigrid for flux
  TYPE(multigrid_t), POINTER :: rgrid(:) ! multigrid for residual
  TYPE(multigrid_t), POINTER :: zgrid(:) ! multigrid for philimit,Q: Why not int
                                         ! or logical since it seems only >0 is
                                         ! checked. Would save 7*N bytes of
                                         ! memory, could be >100MB!

  DOUBLE PRECISION, POINTER :: x(:,:,:)  ! ptr to potential data
  DOUBLE PRECISION, POINTER :: f(:,:,:)  ! ptr to source data
  DOUBLE PRECISION, POINTER :: g(:,:,:)  ! ptr to flux data
  DOUBLE PRECISION, POINTER :: r(:,:,:)  ! ptr to residual data
  DOUBLE PRECISION, POINTER :: z(:,:,:)  ! ptr to philimit data

  DOUBLE PRECISION, POINTER :: x2(:,:,:) ! For convergance test
  double precision dphisqsum,prevdphisqsum ! For convergance test

  DOUBLE PRECISION :: currentresidual

  INTEGER :: i, j, k, n_i,n_j,n_k, level, iter
  INTEGER :: count ! Used for debugging and informational purposes
  real :: n


  LOGICAL :: FirstTime = .TRUE. ! First time in this function
  
  SAVE FirstTime, xgrid, fgrid, ggrid, rgrid, zgrid, x2
  ! Size of each cell at different levels and the same squared
  DOUBLE PRECISION :: h_i(levels), hsq_i(levels)
  DOUBLE PRECISION :: h_j(levels), hsq_j(levels)
  DOUBLE PRECISION :: h_k(levels), hsq_k(levels)

  if(levels == 0) then
    print *, "MG: WARNING: System size not divisible by 2. Using plain iteration"
    multigrid = .false.
  else
    multigrid = .true.
  end if

  n = gsize_i(1) * gsize_j(1) * gsize_k(1)
  IF(verbose) PRINT *, 'MG: Coarsest', gsize_i(1),gsize_j(1),gsize_k(1)
  IF(verbose) PRINT *, 'MG: Points in coarsest grid', n
  if(any([gsize_i(1), gsize_j(1), gsize_k(1)] > 7)) then
    print *, "WARNING: Grid seems to be pretty coarse. Expect bad performance"
    ! Repeat the above in case verbose was false
    PRINT *, 'MG: Coarsest', gsize_i(1),gsize_j(1),gsize_k(1)
    PRINT *, 'MG: Points in coarsest grid', n
  end if
!   print *, 

  if(directsmoothing<0) then
    n=n**(1.0/3.0)
    ! Fitted to "best" found values during development
    directsmoothing=0.748579*n**2-0.97208*n+3.29423
  end if
  !directsmoothing = 3*directsmoothing


  IF(verbose) PRINT *, 'MG: directsmoothing', directsmoothing

  ! Precompute step sizes
  DO level = 1, levels
    h_i(level) = a_grid(3) * DBLE(2**(levels - level)) ! Note the index!
    hsq_i(level) = h_i(level)**2

    h_j(level) = a_grid(2) * DBLE(2**(levels - level))
    hsq_j(level) = h_j(level)**2

    h_k(level) = a_grid(1) * DBLE(2**(levels - level))
    hsq_k(level) = h_k(level)**2
  end do

  ! Allocate memory for MG-arrays (first time only):
  !-------------------------------------------------
  !IF(FirstTime) THEN
  IF(.true.) THEN ! Size of grid may change from first time (e.g. partial solution)
    IF(verbose) THEN
      PRINT *, '***************************************'
      PRINT *, '*****  Allocating memory for MG  ******'
      PRINT *, '***************************************'
    END IF

    ! Allocate memory & initialize:
    !------------------------------
    ALLOCATE(xgrid(levels))
    ALLOCATE(fgrid(levels))
    ALLOCATE(ggrid(levels))
    ALLOCATE(zgrid(levels))
    ALLOCATE(rgrid(levels))
     
    ALLOCATE(x2(gsize_i(levels), gsize_j(levels), gsize_k(levels)))

    DO level = 1, levels
      n_i = gsize_i(level)
      n_j = gsize_j(level)
      n_k = gsize_k(level)
        
      ALLOCATE(xgrid(level) % DATA(n_i, n_j, n_k))
      ALLOCATE(fgrid(level) % DATA(n_i, n_j, n_k))
      ALLOCATE(ggrid(level) % DATA(n_i, n_j, n_k))
      ALLOCATE(zgrid(level) % DATA(n_i, n_j, n_k))
      ALLOCATE(rgrid(level) % DATA(n_i, n_j, n_k))

      xgrid(level) % DATA = 0.0d0
      rgrid(level) % DATA = 0.0d0
      fgrid(level) % DATA = 0.0d0
      ggrid(level) % DATA = 0.0d0
      zgrid(level) % DATA = 0.0d0
        
    END DO


  END IF

  ! Set philimit:
  !--------------
  IF(setphilimit) THEN
    count = 0

    n_i = gsize_i(levels)
    n_j = gsize_j(levels)
    n_k = gsize_k(levels)
     
    z => zgrid(levels) % DATA
    z = 0.0d0

    DO k = 1, n_k - 1
      DO j = 1, n_j - 1
        DO i = 1, n_i - 1
          ! Note the order of the indices!
          ! multigrid uses the indices in a different order
          IF((philimit(k, j, i) == -1) .OR. & ! CLEANUP: Should be just < 0 
            (philimit(k, j, i) < -2)) THEN ! Actually ignore the boundary, we set it separately anyway

            count = count + 1
            z(i, j, k) = 1.0d0
                 
          END IF
        END DO
      END DO
    END DO

    ! Restrict philimit to coarse grids:
    !-----------------------------------
    ! Speed/memory tradeoff, doesn't use that much memory so probably worth it
    do level = levels, 2, -1
      n_i = gsize_i(level-1)
      n_j = gsize_j(level-1)
      n_k = gsize_k(level-1)

      z => zgrid(level-1) % DATA
      f => zgrid(level) % DATA ! tmp ptr here
     
      do k = 1, n_k
        do j = 1, n_j
          do i = 1, n_i
            z(i, j, k) = f(2*i-1, 2*j-1, 2*k-1)
          end do
        end do
      end do
    end do

  END IF

  IF(verbose) PRINT *, 'MG: philimit set for', count, 'grid points'

  ! Set initial guess:
  !-------------------
  n_i = gsize_i(levels)
  n_j = gsize_j(levels)
  n_k = gsize_k(levels)

  x => xgrid(levels) % DATA
  z => zgrid(levels) % DATA


  DO k = 1, n_k - 1
    DO j = 1, n_j - 1
      DO i = 1, n_i - 1
        x(i, j, k) = phi(k, j, i) ! Note the order of the indices!
        IF(z(i, j, k) > 0.5d0) THEN
          x(i, j, k) = 0.0d0
        END IF
      END DO
    END DO
  END DO


  ! set constant source:
  !---------------------
  f => fgrid(levels) % DATA
  f = 0.0d0 ! Laplace

  ! set constant normal derivative on the top:
  !--------------------------------------------
  !IF(verbose) PRINT *,'MG: E0:', e0

  g => ggrid(levels) % DATA

  g(n_i,:,:) = -e0

  n_i = gsize_i(levels)
  n_j = gsize_j(levels)
  n_k = gsize_k(levels)

  dphisqsum = 1e30

  CALL boundaryconditions(levels)

  ! Set gradient boundary at top
  i = n_i

  DO k = 1, n_k - 1
    DO j = 1, n_j - 1
      x(i,j,k) = x(i-1,j,k)+g(i,j,k)*h_i(levels)
    END DO
  END DO

  CALL computeresidual(levels)
  currentresidual = SQRT(SUM(r*r)) / SQRT(SUM(f*f) + SQRT(SUM(g*g)) * (2**levels))
  if(verbose) PRINT *, 'MG: iter', 0, "currentresidual",currentresidual, "dphisqsum", dphisqsum, maxval(abs(r))

  ! Solve:
  !--------
  IF(verbose) PRINT *,'MG: grid:', n_i, 'x', n_j, 'x', n_k

  dphisqsum = 1e30
  IF(multigrid) THEN
    if(verbose) PRINT *, 'MG:', 'iter,', 'currentresidual,', 'dphisqsum'
    DO iter = 1, maxiter
      CALL computeresidual(levels)

      f => fgrid(levels) % DATA
      g => ggrid(levels) % DATA
      r => rgrid(levels) % DATA

      currentresidual = SQRT(SUM(r*r)) &
           / SQRT(SUM(f*f) + SQRT(SUM(g*g)) * (2**levels))
      if(verbose) PRINT *, 'MG: iter', iter, "currentresidual",currentresidual, &
"dphisqsum", dphisqsum, maxval(abs(r))

      IF(currentresidual < tolerance .or. dphisqsum < dphiconv) EXIT
      x2=x
      CALL solve(levels)

      ! dphisqsum for easier comparison with Jacobi method used in clic.f90
      ! TODO: This is an unneccessary step, and it slows down the algorithm
      ! Then again, it is not called very often, so there is a low overhead
      dphisqsum=sum((x-x2)**2);

      ! Check to see if convergance is happening
      if(iter==1) prevdphisqsum=dphisqsum
      if(mod(iter,15)==0) then
        if(dphisqsum/prevdphisqsum>1.0) then
          directsmoothing=directsmoothing*2
          print *, "MG: Doesn't seem to converge, directsmoothing is now", &
            directsmoothing
          print *, "Larger values usually means slower performance"
          print *, "You probably want to check the size of your grid and make sure it is close to 2^n"
          if(directsmoothing>10000) then
            print *, "Directsmoothing > 100000"
            stop
          end if
        end if
        prevdphisqsum=dphisqsum
      end if
      !if(verbose) PRINT *, 'MG: iter', iter, "currentresidual",currentresidual, &
!"dphisqsum", dphisqsum, maxval(abs(r))

    END DO

    if (currentresidual>=tolerance .and. dphisqsum>=dphiconv) PRINT *, "WARNING: DID NOT FIND SOLUTION, REACHED MAXITER"

  ELSE ! Pure Gauss-Seidel, mostly for debugging purposes
    PRINT *, 'MG: method: Gauss-Seidel'
    x2(:,:,:)=x(:,:,:)
    DO iter = 1, maxiter * 100
      DO i = 1, 200
        CALL gaussseidel(levels)
      END DO

      CALL computeresidual(levels)

      f => fgrid(levels) % DATA
      g => ggrid(levels) % DATA
      r => rgrid(levels) % DATA

      currentresidual = SQRT(SUM(r*r)) &
           / (SQRT(SUM(f*f)) + SQRT(SUM(g*g)) * (2**levels))
      if(verbose) print *, sum(r*r), sum(f*f), sum(g*g), 2**levels

      ! dphisqsum for easier comparison with Jacobi method used in clic.f90
      ! TODO: This is an unneccessary step, and it slows down the algorithm
      ! Then again, it is not called very often, so there is a low overhead

      dphisqsum=sum((x-x2)**2);
      x2=x
      if(verbose) PRINT *, 'Gauss-Seidel:', 200 * iter, currentresidual

      IF(currentresidual < tolerance .or. dphisqsum < dphiconv) EXIT
    END DO

  END IF


  !PRINT *, 'MG: time:', tstop - tstart, 'CPU seconds'

  ! Copy solution into parcas' structures (change i and k):
  !--------------------------------------------------------
  x => xgrid(levels) % DATA
  n_i = gsize_i(levels)
  n_j = gsize_j(levels)
  n_k = gsize_k(levels)

  call boundaryconditions(levels)

  DO k = 1, n_k - 1
    DO j = 1, n_j - 1
      DO i = 1, n_i - 1
        phi(k, j, i) = x(i, j, k) ! Note the order of the indices
      END DO
    END DO
  END DO

  ! Clean up:
  !----------
FirstTime = .FALSE.
if(.true.) then ! Cleanup now has to be done every time since next call might be for a partial solution
  DO level = levels, 1, -1
    DEALLOCATE(xgrid(level) % DATA)
    DEALLOCATE(rgrid(level) % DATA)
    DEALLOCATE(fgrid(level) % DATA)
    DEALLOCATE(ggrid(level) % DATA)
    DEALLOCATE(zgrid(level) % DATA)
  END DO

  DEALLOCATE(xgrid)
  DEALLOCATE(rgrid)
  DEALLOCATE(fgrid)
  DEALLOCATE(ggrid)
  DEALLOCATE(zgrid)
  DEALLOCATE(x2)
end if

CONTAINS

  !============================================================================
  RECURSIVE SUBROUTINE solve(level)
  implicit none
    !
    !                             MG-solver
    !                            -----------
    !
    !                         Solves the equation
    !
    !                               A x = f
    !
    !      where A is the difference approximation of the Laplace
    !     operator in the unit cube with a) Neumann conditions on
    !     the top b) Dirichlet conditions on the bottom c) periodic
    !       conditions on the sides and d) additional restrictions
    !             (philimit) inside the computational domain.
    !
    ! The function is recursive because correct() calls solve(level-1)
    INTEGER :: level

    ! Coarsest grid reached, can't use MG anymore, end of recursion
    IF(level == 1) THEN 
      CALL smooth(level, directsmoothing)
      RETURN
    END IF
    
    CALL smooth(level, presmoothing)
    CALL correct(level, cycles)
    CALL smooth(level, postsmoothing)
        
  END SUBROUTINE solve

  !============================================================================
  RECURSIVE SUBROUTINE correct(level, cycles)
  implicit none
    ! 
    !                        Coarse grid correction
    !                       -------------------------
    !
    !       Computes the coarse grid corrected solution x(level) as
    !
    !               x(level) <- x(level) + P y(level-1)
    !
    ! where P is the bilinear prolongation and y(level-1) is the minimizer of
    !
    !          || f(level) - A[x(level) + P y(level-1)] ||_{inv(A)}
    !
    ! 
    !            This equals to solving the algebraic equation
    !
    !                   (P'AP) y(level-1) = P' r(level)            (*)
    !
    !                                with
    !
    !                  r(level) = f(level) - A x(level)
    !
    !  The subroutine solves (*) recursively by calling the MG-solver it self.
    !
    INTEGER :: level, cycles
    
    INTEGER :: iter


    CALL computeresidual(level)

    CALL restrict(level)

    DO iter = 1, cycles
      CALL solve(level-1) ! level > 1 here always (checked in solve())
    END DO

    CALL prolongate(level)
    
  END SUBROUTINE correct
  
  !============================================================================
  SUBROUTINE prolongate(level)
  implicit none
    !
    !                       Bilinear prolongation
    !                      -----------------------
    !
    !                             Computes
    !
    !                x(level) <- x(level) + P r(level-1)
    !
    ! Coarse-to-fine, like described in NR, except in 3D
    INTEGER :: level
    
    INTEGER :: i, j, k, u, i2, j2, k2, v, w, n_i,n_j,n_k, n2i,n2j,n2k
    ! TODO: Write this into a matrix so we don't recalculate it every time
    ! The 27-point stencil
    ! s(i,j,k) = v(i)*v(j)*v(k), where v=(0.5,1.0,0.5), -1<= i,j,k <=1
    DOUBLE PRECISION,PARAMETER :: s(-1:1,-1:1,-1:1) = reshape( &
         (/0.125,0.250,0.125, 0.250,0.500,0.250, 0.125,0.250,0.125, &
           0.250,0.500,0.250, 0.500,1.000,0.500, 0.250,0.500,0.250, &
           0.125,0.250,0.125, 0.250,0.500,0.250, 0.125,0.250,0.125/), (/3,3,3/))

    n_i = gsize_i(level-1) ! Number of points in coarser grid
    n_j = gsize_j(level-1) ! Number of points in coarser grid
    n_k = gsize_k(level-1) ! Number of points in coarser grid

    n2i = gsize_i(level) ! Number of points in finer grid
    n2j = gsize_j(level) ! Number of points in finer grid
    n2k = gsize_k(level) ! Number of points in finer grid

    x => xgrid(level-1) % DATA ! Potential in coarser grid
    r => rgrid(level) % DATA ! Residual? in this grid

    r = 0.0d0
    
    DO k = 1, n_k ! Go through every point in the coarse grid line
      DO k2 = -1, 1
        w = 2 * k - 1 + k2 ! Corresponding points in finer grid
        IF((w < 1) .OR. (w > n2k)) CYCLE ! w<1 for first point

        DO j = 1, n_j ! Repeat in the other directions
          DO j2 = -1, 1
            v = 2 * j - 1 + j2
            IF((v < 1) .OR. (v > n2j)) CYCLE
                
            DO i = 1, n_i
              DO i2 = -1, 1
                u = 2 * i - 1 + i2
                IF((u < 1) .OR. (u > n2i)) CYCLE
                      
                r(u, v, w) = r(u, v, w) &
                  !+ s(i2) * s(j2) * s(k2) * x(i, j, k)
                  + s(i2,j2,k2)*x(i,j,k)

              END DO
            END DO
          END DO
        END DO
      END DO
    END DO

    x => xgrid(level) % DATA
    ! Moving this into the loop does not speed up anything
    x = x + r

  END SUBROUTINE prolongate

  !============================================================================
  SUBROUTINE restrict(level)
  implicit none
    !
    !                    Bilinear restriction
    !                   -----------------------
    !
    !                           Computes
    !
    !                  f(level-1) <- P' r(level)
    !
    ! Fine-to-coarse
    INTEGER :: level
    
    INTEGER :: i, j, k, u, v, w, n_i,n_j,n_k, n2i,n2j,n2k, i2, j2, k2
    ! The 27-point stencil divided by 8.0
    ! s(i,j,k) = v(i)*v(j)*v(k)/8.0, where v=(0.5,1.0,0.5), -1<= i,j,k <=1
    DOUBLE PRECISION,PARAMETER :: s(-1:1,-1:1,-1:1) = reshape( &
         (/0.015625,0.031250,0.015625, 0.031250,0.062500,0.031250, 0.015625,0.031250,0.015625, &
           0.031250,0.062500,0.031250, 0.062500,0.125000,0.062500, 0.031250,0.062500,0.031250, &
           0.015625,0.031250,0.015625, 0.031250,0.062500,0.031250, 0.015625,0.031250,0.015625/), (/3,3,3/))
    
    n_i = gsize_i(level-1) ! Number of grid points in coarser grid
    n_j = gsize_j(level-1) ! Number of grid points in coarser grid
    n_k = gsize_k(level-1) ! Number of grid points in coarser grid
    n2i = gsize_i(level) ! Number of grid points in this grid
    n2j = gsize_j(level) ! Number of grid points in this grid
    n2k = gsize_k(level) ! Number of grid points in this grid

    r => rgrid(level) % DATA 
    f => fgrid(level-1) % DATA
    
    f = 0.0d0

    DO k = 1, n_k
      DO k2 = -1, 1
        w = 2 * k - 1 + k2
        IF((w < 1) .OR. (w > n2k)) CYCLE
          
        DO j = 1, n_j
          DO j2 = -1, 1
            v = 2 * j - 1 + j2
            IF((v < 1) .OR. (v > n2j)) CYCLE
                
            DO i = 1, n_i
              DO i2 = -1, 1
                u = 2 * i - 1 + i2
                IF((u < 1) .OR. (u > n2i)) CYCLE
                      
                f(i, j, k) = f(i, j, k) &
                  !+ s(i2) * s(j2) * s(k2) * r(u, v, w) / 8.0d0
                  + s(i2,j2,k2)*r(u,v,w)
              END DO
            END DO
          END DO
        END DO
      END DO
    END DO
    
  END SUBROUTINE restrict
  
  !============================================================================
  SUBROUTINE smooth(level, rounds)
  implicit none
    !
    !                        Relaxation step
    !                       -----------------
    !
    !                    Computes several times
    !
    !               x(level) <- x(level) + M r(level)
    !
    !                             with
    !
    !                 r(level) = f(level) - A x(level)
    !
    !              Here M is some approximation to inv(A).
    !
    INTEGER :: level, rounds

    INTEGER :: iter

    DO iter = 1, rounds
      CALL gaussseidel(level)
    END DO
    
  END SUBROUTINE smooth
  
  !============================================================================
  SUBROUTINE computeresidual(level)
  implicit none
    !
    !                      Residual computation
    !                     ----------------------
    !
    !                            Computes
    !
    !                r(level) = f(level) - A x(level)
    !
    INTEGER :: level

    INTEGER :: i, j, k, n_i,n_j,n_k

    INTEGER :: iprevious, inext
    INTEGER :: jprevious, jnext
    INTEGER :: kprevious, knext

    DOUBLE PRECISION :: hi,hsqi,hsqj,hsqk

    hi = h_i(level)
    hsqi = 1d0/hsq_i(level)
    hsqj = 1d0/hsq_j(level)
    hsqk = 1d0/hsq_k(level)

    x => xgrid(level) % DATA
    f => fgrid(level) % DATA
    g => ggrid(level) % DATA
    r => rgrid(level) % DATA
    z => zgrid(level) % DATA

    n_i = gsize_i(level)
    n_j = gsize_j(level)
    n_k = gsize_k(level)

    
    r = 0d0

    ! interior points:
    !-----------------
    DO k = 1, n_k - 1
      kprevious = k - 1; IF(kprevious < 1) kprevious = n_k - 1
      knext = k + 1; IF(knext > (n_k - 1)) knext = 1
       
      DO j = 1, n_j - 1
        jprevious = j - 1; IF(jprevious < 1) jprevious = n_j - 1
        jnext = j + 1; IF(jnext > (n_j - 1)) jnext = 1
          
        DO i = 2, n_i - 1
          iprevious = i - 1
          inext = i + 1

          IF(z(i, j, k) > 0.5d0) THEN
            r(i, j, k) = 0.0d0
            CYCLE
          END IF
          r(i, j, k) = f(i, j, k) + & 
            ! Central difference equations
            (x(iprevious,j,k)+x(inext,j,k)-2*x(i,j,k))*hsqi + &
            (x(i,jprevious,k)+x(i,jnext,k)-2*x(i,j,k))*hsqj + &
            (x(i,j,kprevious)+x(i,j,knext)-2*x(i,j,k))*hsqk
        END DO
      END DO
    END DO
    
    if(boundarytype==SOLVE_FULL) then
      ! Neumann condition:
      !--------------------
      i = n_i
      iprevious = i - 1
        
      DO k = 1, n_k - 1
        kprevious = k - 1; IF(kprevious < 1) kprevious = n_k - 1
        knext = k + 1; IF(knext > (n_k - 1)) knext = 1

        DO j = 1, n_j - 1
          jprevious = j - 1; IF(jprevious < 1) jprevious = n_j - 1
          jnext = j + 1; IF(jnext > (n_j - 1)) jnext = 1
           
          r(i, j, k) = f(i, j, k) + &
            (x(iprevious,j,k)+x(iprevious,j,k)-2*x(i,j,k)+2*hi*g(i,j,k))*hsqi + &
            (x(i,jprevious,k)+x(i,jnext,k)-2*x(i,j,k))*hsqj + &
            (x(i,j,kprevious)+x(i,j,knext)-2*x(i,j,k))*hsqk
        END DO
      END DO
    else
      ! We have exact solution at the border
      r(1,:,:) = 0d0
      r(:,1,:) = 0d0
      r(:,:,1) = 0d0

      r(n_i,:,:) = 0d0
      r(:,n_j,:) = 0d0
      r(:,:,n_k) = 0d0
    end if
  END SUBROUTINE computeresidual
  
  !============================================================================
  SUBROUTINE gaussseidel(level)
  implicit none
    !
    !                    Gauss-Seidel relaxation
    !                   -------------------------
    !
    !                           Computes
    !
    !             x(level) <- x(level) + M r(level)
    !
    !                             with
    !
    !                r(level) = f(level) - A x(level)
    !
    !       Here M is the Gauss-Seidel approximation to inv(A)
    !
    ! TODO: Red-Black Gauss-Seidel seems like a popular choice, maybe test with
    ! that also? (NR: "it is usually best to use red-black ordering")
    INTEGER :: level

    INTEGER :: i, j, k, n_i,n_j,n_k

    INTEGER :: iprevious, inext
    INTEGER :: jprevious, jnext
    INTEGER :: kprevious, knext

    DOUBLE PRECISION :: hi,hsqi,hsqj,hsqk,hsqijk
    DOUBLE PRECISION :: delta, delta_x, delta_y, delta_z

    integer :: add(3,2)
    add = 0
    add(3,1) = 1
    if(boundarytype == SOLVE_PARTIAL) then
      add = 1
    end if
    
    x => xgrid(level) % DATA
    f => fgrid(level) % DATA
    g => ggrid(level) % DATA
    r => rgrid(level) % DATA
    z => zgrid(level) % DATA

    n_i = gsize_i(level)
    n_j = gsize_j(level)
    n_k = gsize_k(level)

    hi = h_i(level)
    hsqi = hsq_i(level)
    hsqj = hsq_j(level)
    hsqk = hsq_k(level)

    ! TODO: These could also be precomputed
    delta_x = hsqj*hsqk
    delta_y = hsqi*hsqk
    delta_z = hsqi*hsqj
    delta = 1d0/(2.0*(delta_x + delta_y + delta_z))
    hsqijk = hsqi*hsqj*hsqk


    ! internal points:
    !------------------
    ! Eliminating the if's inside the loop does not seem to help much, so let them be
    DO k = 1+add(1,1), n_k - 1 -add(1,2)
      kprevious = k - 1; IF(kprevious < 1) kprevious = n_k - 1 ! Periodic
      knext = k + 1; IF(knext > (n_k - 0)) knext = 1

      DO j = 1+add(2,1), n_j - 1 -add(2,2)
        jprevious = j - 1; IF(jprevious < 1) jprevious = n_j - 1
        jnext = j + 1; IF(jnext > (n_j - 0)) jnext = 1

        DO i = 1+add(3,1), n_i - 1 -add(3,2)! i=1 is surface for sure
          iprevious = i - 1
          inext = i + 1
             
          ! Check if it's an "abnormal" point (philimit -1 or -2), can't compare directly
          ! with floats
            IF(z(i, j, k) > 0.5d0) THEN
              x(i, j, k) = 0.0d0
              CYCLE
            END IF

            ! Math: f(i,j,k) = dphi^2/dx^2+dphi^2/dy^2+dphi^2/dz^2
            ! d^2phi/dx^2=-2*phi(i,j,k)+phi(i-1,j,k)+phi(i+1,j,k)
            ! Same in different directions, simplify and solve for phi and you get:
            x(i,j,k) = (x(iprevious,j,k)+x(inext,j,k))*delta_x + &
                       (x(i,jprevious,k)+x(i,jnext,k))*delta_y + &
                       (x(i,j,kprevious)+x(i,j,knext))*delta_z + &
                       f(i,j,k)*hsqijk
            x(i,j,k) = x(i,j,k)*delta ! The compiler seems to be smart enough to hoist this out of the loop

        END DO
      END DO
    END DO

    if(boundarytype == SOLVE_FULL) then
      ! Neumann condition on the top:
      !------------------------------
      i = n_i
      iprevious = i - 1

      DO k = 1, n_k - 1
        kprevious = k - 1; IF(kprevious < 1) kprevious = n_k - 1
        knext = k + 1; IF(knext > (n_k - 1)) knext = 1
         
        DO j = 1, n_j - 1
          jprevious = j - 1; IF(jprevious < 1) jprevious = n_j - 1
          jnext = j + 1; IF(jnext > (n_j - 1)) jnext = 1       
            
          x(i,j,k) = (x(iprevious,j,k)+x(iprevious,j,k)+2.0*hi*g(i,j,k))*delta_x + &
                     (x(i,jprevious,k)+x(i,jnext,k))*delta_y + &
                     (x(i,j,kprevious)+x(i,j,knext))*delta_z + &
                     f(i,j,k)*hsqijk
          x(i,j,k) = x(i,j,k)*delta

        END DO
      END DO
    end if

    ! boundary conditions & philimit:
    !--------------------------------
    CALL boundaryconditions(level)

    
  END SUBROUTINE gaussseidel

  
  !============================================================================
  SUBROUTINE boundaryconditions(level)
    implicit none
    !
    !     Sets boundary conditions for the solution and residual
    !
    INTEGER :: level
    
    INTEGER :: n_i, n_j,n_k

    n_i = gsize_i(level)
    n_j = gsize_j(level)
    n_k = gsize_k(level)

    x => xgrid(level) % DATA
    r => rgrid(level) % DATA
    z => zgrid(level) % DATA
    g => ggrid(level) % DATA

    if(boundarytype == SOLVE_FULL) then
      ! Dirichlet: (i.e constant value at the bottom)
      !-----------
      x(1,:,:) = 0.0d0
      r(1,:,:) = 0.0d0

      ! Periodic in y:
      !----------
      x(:,n_j,:) = x(:,1,:)
      r(:,n_j,:) = r(:,1,:)
      
      ! Periodic in z:
      !----------
      x(:,:,n_k) = x(:,:,1)
      r(:,:,n_k) = r(:,:,1)
    else
      ! Let x be 
      ! We have exact solution at the border
      r(1,:,:) = 0d0
      r(:,1,:) = 0d0
      r(:,:,1) = 0d0

      r(n_i,:,:) = 0d0
      r(:,n_j,:) = 0d0
      r(:,:,n_k) = 0d0
    end if

    ! zgrid (philimit):
    !------------------

    !WHERE (z>0.5d0)
    !   x=0.0d0
    !   r=0.0d0
    !END WHERE

  END SUBROUTINE boundaryconditions

END SUBROUTINE mg
end module multigridmod

! Standard Fortran requires spaces instead of tabs
! vim: expandtab:tabstop=2:shiftwidth=2
! kate: indent-width 2; space-indent on; replace-tabs
