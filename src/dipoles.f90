module dipoles

implicit none

integer, parameter      :: dp = 8

real(dp), parameter     :: polarizability = 0.55d0

contains

function dipole_potential(Efield) result(Upol)

    real(dp), intent(in)    :: Efield(:,:,:,4)
    real(dp)                :: Upol(size(Efield,1), size(Efield,2), size(Efield,3))
    
    Upol = 0.5 * polarizability * Efield(:,:,:,4) * Efield(:,:,:,4)
     
end function dipole_potential


    
    

    
