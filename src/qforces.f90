!> @file qforces.f90
!> @brief Home of the qforces module

!> @brief Module containing subrotuines for calculating the Coloumb force between atoms
module qforces_mod
contains

!> @brief Add forces due to Coloumb interaction and Lorentz force to atoms
!>
!> The subroutine adds the Coulomb force between atoms based on the charge in the "first column" in @p xq.  The Lorentz force is
!> calculated elsehwere, but the force itself is added to the total force here. The Lorentz force components are also stored in @p xq. 
subroutine Qforces(x0, xnp, box, pbc, Epair, Vpair, Vqq, xq, qrcut, qscreen, natoms, dbg)

IMPLICIT NONE

! ------------------------------------------------------------------------
!     Variables passed in and out
      
REAL*8, intent(in) :: x0(:) !< Atom positions (parcas units)
REAL*8, intent(out) :: xnp(:) !< Forces on atoms (parcas units)

REAL*8, intent(in) :: box(3) !< Simulation box size (Å)
REAL*8, intent(in) :: pbc(3) !< Periodic boundaries

REAL*8, intent(inout) :: Epair(:) !< Potential energy per atom
REAL*8, intent(inout) :: Vpair !< Total potential energy of atoms. Pot. due to Coloumb forces are added here. NOTE: Lorentz is missing!

integer, intent(in) :: natoms !< Number of atoms. CLEANUP: Not really needed, could use size of x0 instead

real*8, intent(out) :: Vqq  !< Potnetial energy due to coloumb interaction

real*8, intent(in) :: xq(:) !< Charges on atoms (unit charges) and Lorentz force components
real*8, intent(in) :: qrcut !< Cut-off for Coloumb force
real*8, intent(in) :: qscreen !< Screening factor for Coulomb force
logical, intent(in) :: dbg !< Show debugging information
  
! ------------------------------------------------------------------------
!     Local variables

logical, save :: firsttime = .true.

integer t,t3,t4
    
integer i,i3,i4
integer imx,imy,imz
integer, save :: imxmax,imymax,imzmax
real*8 xi,yi,zi
real*8 xtim,ytim,ztim
real*8 dx,dy,dz,r,rsq,V,dVdr,kCoulomb_q1q2,qscreenfact
  
real*8, save :: rmax,qrcutsq
  
real :: t0,t1,t2
integer :: chi,cht
integer :: charged(size(x0)/3) ! List of atoms with charge
integer :: ncharged ! Number of charged atoms

integer*4, save, allocatable :: neigh_cells(:,:,:,:) ! Divide system into cells to ease force calc. First number in 4th col is number of atoms
integer, save :: ncell(3) ! How many cells we have

integer, parameter :: update_neighbours_every = 10
integer, save :: times_called = -1

real* 8,save :: neigh_cell_size(3)
integer,save :: max_in_cell
integer :: atoms_in_cell
integer :: ineix, ineiy, ineiz, cell(3), nei(3)

real*8 :: cellc(3) ! Center-point of cell
  
! ------------------------------------------------------------------------

call cpu_time(t0)

! Initializations
ncharged = 0
do i = 1,natoms
  i3=i*3-3
  i4=i*4

  !Transform any outer forces from internal units into eV/A for force routines
  xnp(i3+1)=xnp(i3+1)*box(1)
  xnp(i3+2)=xnp(i3+2)*box(2)
  xnp(i3+3)=xnp(i3+3)*box(3)
     
  ! Make a list of charged atoms. Later we only need to loop over these
  if(abs(xq(i4)) > 0d0) then
    ncharged = ncharged + 1
    charged(ncharged) = i
  end if
enddo

! Figure out size of image cells and neighbour cells
if(firsttime) then
  rmax=qrcut
  qrcutsq=qrcut*qrcut
  
  imxmax=ceiling(rmax/box(1))
  if (pbc(1)==0d0) imxmax=0
  imymax=ceiling(rmax/box(2))
  if (pbc(2)==0d0) imymax=0
  imzmax=ceiling(rmax/box(3))
  if (pbc(3)==0d0) imzmax=0

  neigh_cell_size = 2.01d0*qrcut
  ! Assume max 50% higher atomic density than in crystal
  max_in_cell = ceiling(1.5*0.85*neigh_cell_size(1)*neigh_cell_size(2)*neigh_cell_size(3))

  ncell(1) = ceiling(box(1)/neigh_cell_size(1))
  ncell(2) = ceiling(box(2)/neigh_cell_size(2))
  ncell(3) = ceiling(box(3)/neigh_cell_size(3))

  if(dbg) print *,'qforces initial range of image cells',imxmax,imymax,imzmax
  if(dbg) print *, "neigh_cells", ncell(1), ncell(2), ncell(3), max_in_cell
  allocate(neigh_cells(ncell(1),ncell(2),ncell(3),0:max_in_cell))
endif
  
Vqq = 0.0d0

times_called = times_called + 1

call cpu_time(t1)
! Divide system into cells, update which atoms belong where
if(mod(times_called,update_neighbours_every) == 0) then
  neigh_cells = -1
  do i=1,ncharged
    i3=charged(i)*3-3
    i4=charged(i)*4-4

    cell(1)=ceiling((x0(i3+1)*box(1) + box(1)/2d0)/neigh_cell_size(1))
    cell(2)=ceiling((x0(i3+2)*box(2) + box(2)/2d0)/neigh_cell_size(2))
    cell(3)=ceiling((x0(i3+3)*box(3) + box(3)/2d0)/neigh_cell_size(3))
    if (cell(1) > ncell(1)) cell(1) = ncell(1)
    if (cell(2) > ncell(2)) cell(2) = ncell(2)
    if (cell(3) > ncell(3)) cell(3) = ncell(3)

    ! Can happen if atom is exactly at border?
    if(cell(1) < 1) cell(1) = 1
    if(cell(2) < 1) cell(2) = 1
    if(cell(3) < 1) cell(3) = 1

    atoms_in_cell = neigh_cells(cell(1),cell(2),cell(3),0)
    if (atoms_in_cell >= max_in_cell) then
      print *, "Qforces: too many atoms in cell", atoms_in_cell,max_in_cell
      stop
    end if 

    if (atoms_in_cell < 0) then
      atoms_in_cell = 0
    end if

    atoms_in_cell = atoms_in_cell + 1
    neigh_cells(cell(1),cell(2),cell(3),0) = atoms_in_cell
    neigh_cells(cell(1),cell(2),cell(3),atoms_in_cell) = i
  end do ! i
  call cpu_time(t2)
  if(dbg) print *, 'Built qforces neighbour cell list in',t2-t1
end if

  !call cpu_time(t1)
  ! Do triple loop over image cells
do imx=-imxmax,imxmax
do imy=-imymax,imymax
do imz=-imzmax,imzmax

  ! Determine whether we should include atoms in this image
  ! cell (idea similar to fig. 5.7 in Allen-Tildesley)
  ! by checking whether cell _center_ is farther than qrcut
  ! from the central cell

  rsq=imx*imx*box(1)*box(1)+imy*imy*box(2)*box(2)+ &
    imz*imz*box(3)*box(3)

  if (rsq > qrcutsq) cycle

  ! Once we have chosen to handle one image cell, take
  ! care of all atoms in there
  
  !$OMP PARALLEL DO DEFAULT(SHARED), &
  !$OMP PRIVATE(i,i3,i4,xi,yi,zi,t,t3,t4,& 
  !$OMP xtim,ytim,ztim,dx,dy,dz,rsq,r,kCoulomb_q1q2,qscreenfact,V,dVdr), &
  !$OMP REDUCTION(+:Vqq), REDUCTION(+:Vpair)
  do chi=1,ncharged
    i=charged(chi)
    i3=i*3-3
    i4=i*4-4

    xi=x0(i3+1)*box(1)
    yi=x0(i3+2)*box(2)
    zi=x0(i3+3)*box(3)

    cell(1)=ceiling((x0(i3+1)*box(1) + box(1)/2d0)/neigh_cell_size(1))
    cell(2)=ceiling((x0(i3+2)*box(2) + box(2)/2d0)/neigh_cell_size(2))
    cell(3)=ceiling((x0(i3+3)*box(3) + box(3)/2d0)/neigh_cell_size(3))

    cellc(1) = cell(1)*neigh_cell_size(1)-neigh_cell_size(1)/2
    cellc(2) = cell(2)*neigh_cell_size(2)-neigh_cell_size(2)/2
    cellc(3) = cell(3)*neigh_cell_size(3)-neigh_cell_size(3)/2

    if (cell(1) > ncell(1)) cell(1) = ncell(1)
    if (cell(2) > ncell(2)) cell(2) = ncell(2)
    if (cell(3) > ncell(3)) cell(3) = ncell(3)

    do ineiz=-1,1
      if (zi > cellc(3) .and. ineiz == -1) cycle ! Past midpoint, don't look the other way
      if (zi < cellc(3) .and. ineiz == 1) cycle
      nei(3) = cell(3)+ineiz

      if (nei(3) > ncell(3)) cycle
      if (nei(3) < 1) cycle
      do ineiy=-1,1
        if (yi > cellc(2) .and. ineiy == -1) cycle ! Past midpoint, don't look the other way
        if (yi < cellc(2) .and. ineiy == 1) cycle
        nei(2) = cell(2)+ineiy
        if (nei(2) > ncell(2)) nei(2) = 1
        if (nei(2) < 1) nei(2) = ncell(2)
        do ineix=-1,1
          if (xi > cellc(1) .and. ineix == -1) cycle ! Past midpoint, don't look the other way
          if (xi < cellc(1) .and. ineix == 1) cycle

          nei(1) = cell(1)+ineix
          ! Periodic boundaries
          if (nei(1) > ncell(1)) nei(1)= 1
          if (nei(1) < 1) nei(1) = ncell(1)

          do cht=1,neigh_cells(nei(1),nei(2),nei(3),0)
            t = neigh_cells(nei(1),nei(2),nei(3),cht)
            t4=t*4-4
            t3=t*3-3
            if(xq(t4+1) == 0) cycle
   
            ! Form atom t coordinate in image cell
            xtim=(x0(t3+1)+1.0d0*imx)*box(1)
            ytim=(x0(t3+2)+1.0d0*imy)*box(2)
            ztim=(x0(t3+3)+1.0d0*imz)*box(3)

            ! Skip self-interaction
            if (i==t .and. imx==0 .and. imy==0 .and. imz==0) cycle
          
            ! Find distance. Because image cells are used,
            ! periodics are not needed.
            dx=xtim-xi
            dy=ytim-yi
            dz=ztim-zi
            rsq=dx*dx+dy*dy+dz*dz
            r=sqrt(rsq)

            ! With cutoff 20.0 Vqq differs by 0.0002% from w.o. cutoff
            !if (r > 20.0) cycle
            if (r > qrcut) cycle
            ! Coulomb term
            kCoulomb_q1q2=14.399758*xq(i4+1)*xq(t4+1)

            qscreenfact=1d0;
            if (qscreen /= 0d0) then
              qscreenfact=exp(-qscreen*r)
            endif
            V=kCoulomb_q1q2/r*qscreenfact
 
            Epair(i)=Epair(i)+0.5d0*V
            Vqq = Vqq+0.5d0*V
            Vpair=Vpair+0.5d0*V
         
            dVdr=-V/rsq
            ! rsq because one 1/r from derivative, another from
            ! vector projection
         
            xnp(i3+1)=xnp(i3+1) + dVdr*dx
            xnp(i3+2)=xnp(i3+2) + dVdr*dy
            xnp(i3+3)=xnp(i3+3) + dVdr*dz
          enddo ! end of loop over atoms s which are neighbours of i
        end do ! ineix
      end do ! ineiy
    end do ! ineiz
  end do ! chi
  !$OMP END PARALLEL DO
end do; end do; end do

! Do unit transformation from eV-A system to parcas units
! Should not be needed, so commented out
!wxx = wxx/(box(1)*box(1))
!wyy = wyy/(box(2)*box(2))
!wzz = wzz/(box(3)*box(3))

!$OMP PARALLEL DO PRIVATE(i3,i4)
do i=1,natoms
  i3=i*3-3
  i4=i*4-4

  xnp(i3+1) = xnp(i3+1)/box(1) + xq(i4+2)/box(1)
  xnp(i3+2) = xnp(i3+2)/box(2) + xq(i4+3)/box(2)
  xnp(i3+3) = xnp(i3+3)/box(3) + xq(i4+4)/box(3)
enddo
!$OMP END PARALLEL DO

close(12)

firsttime=.false.

call cpu_time(t2)

RETURN
  
END SUBROUTINE Qforces
end module
