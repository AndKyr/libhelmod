!> @file evaporation.f90
!> @brief Home of the evaporation module

!> @brief Module to handle field evaporation of atom in Atom Probe simulations
!>
!> @todo Add a debug flag to control what is output and when
module evaporation
implicit none
private
private :: coordinate_to_gridpoint ! Seems to be a bug in Doxygen which forces the documentation to be produced for this one
public :: evaporate_atom

contains

!> @brief Get the field at a specifc point in space
double precision function field_at_point(r ,rc, delta, E, nmax)
implicit none
double precision, intent(in) :: r(3) !< The point at which to get the electric field (parcas units)
double precision, intent(in) :: rc(3) !< The origin of the electric field grid (parcas units)
double precision, intent(in) :: delta(3) !< Grid spacing (parcas internal units)
double precision, intent(in) :: E(:, :, :) !< The electric field
integer, intent(out), optional :: nmax(3) !< The point around the coordinate where the maximum electric field was found
integer :: n(3) !< Coordinate on grid corresponding to the coordinate in space

integer dx, dy, dz, dx2, dy2, dz2

call coordinate_to_gridpoint(r ,n, rc, delta)

! If the point is outside the box, just assume same field as at infinity
if(any(n<1) .or. n(1) > size(E, 1) .or. n(2) > size(E, 2) .or. n(3) > size(E, 3)) then
  field_at_point = E(1, 1, size(E, 3)-2) ! Assume same value as at infinity
  if(present(nmax)) then
    nmax = n
  end if
  return
end if


field_at_point=0d0
if(present(nmax)) then
  nmax = n
end if

! If we try to get the field at an atom, it will be zero (no field inside the material)
! Instead try to find the point around it where the field is biggest
if (E(n(1), n(2), n(3)) < 1e-30) then ! No field in surface, scan around
  if(present(nmax)) then
    nmax = -1
  end if

  do dy=-1, 1; do dx=-1, 1; do dz=-1, 1
    dx2 = dx
    dy2 = dy
    dz2 = dz

    ! Don't go outside the box
    if (n(1) + dx > size(E, 1) .or. n(1) + dx < 1) dx2=0
    if (n(2) + dy > size(E, 2) .or. n(2) + dy < 1) dy2=0
    if (n(3) + dz > size(E, 3) .or. n(3) + dz < 1) dz2=0

    if(E(n(1) + dx2, n(2) + dy2, n(3) + dz2) > 0.0) then
      if(E(n(1) + dx2, n(2) + dy2, n(3) + dz2) > field_at_point) then
        field_at_point = E(n(1) + dx2, n(2) + dy2, n(3) + dz2)
        if(present(nmax)) nmax = [n(1) + dx2, n(2) + dy2, n(3) + dz2]
      end if
    end if
  end do; end do; end do
else
  field_at_point = E(n(1), n(2), n(3))
end if

! Look a bit further if nothing was found on the first try
if (field_at_point < 1e-30) then ! No field in surface, scan around
  do dy=-2, 2; do dx=-2, 2; do dz=-2, 2
    dx2 = dx
    dy2 = dy
    dz2 = dz

    if (n(1) + dx >size(E, 1) .or. n(1) + dx<1) dx2=0
    if (n(2) + dy >size(E, 2) .or. n(2) + dy<1) dy2=0
    if (n(3) + dz >size(E, 3) .or. n(3) + dz<1) dz2=0

    if(E(n(1) + dx2, n(2) + dy2, n(3) + dz2) > 0.0) then
      if(E(n(1) + dx2, n(2) + dy2, n(3) + dz2) > field_at_point) then
        field_at_point = E(n(1) + dx2, n(2) + dy2, n(3) + dz2)
        if(present(nmax)) nmax = [n(1) + dx2, n(2) + dy2, n(3) + dz2]
      end if
    end if
  end do; end do; end do
end if

!if(present(nmax)) then
!  if (nmax(1) == -1 .and. nmax(2) == -1 .and. nmax(3) == -1) then
!    print *, "No field found at", r, n, rc, delta
!    stop
!  end if
!end if

if(field_at_point == 0d0) then
  print *, "No field found at", r, n, rc, delta
  stop
end if

end function

!> Integrate ion trajectory
subroutine SPF_verlet(r0, m, q, zmax, E, rc, delta, box, atype)
implicit none

! r0 is in relative units
! m is in kg
! q is in C
! zmax is in absolute
! rc is in relative
! delta is in relative (boxes per point)

double precision, intent(in) :: r0(3), m, q, zmax, rc(3), delta(3), box(3)
double precision, intent(in) :: E(:, :, :, :)
integer, intent(in) :: atype !< Atom types
double precision :: xout, yout, t
integer :: outside

double precision :: rold(3), r(3), rnew(3) ! In internal coordinates!
double precision :: a(3), dt, dt2
integer :: i, n(3), nmax(3)
double precision :: Emax
double precision :: vprev(3)

integer, parameter :: ndumptrajectory = 10 ! Write out coordinates of ion every N steps

rold = r0
rnew = 0.0d0
r = rold
a = 0.0d0 !acceleration

dt = 1 ! Choose a sensible dt, depends on units. 1fs
dt2 = dt*dt
n = 0

i = 0
open(112, file='tracks.txt', position='append')
write(112, "(F14.4, 3F8.2)") i*dt, r*box
! Verlet loop
outside = 0 ! For warning if particle is outside field
print *, "DBG: tracing until", zmax
do while ((abs(r(3)*box(3)) < zmax)) ! stops when atom reaches detector
  call coordinate_to_gridpoint(r, n, rc, delta) ! Get point of particle
  Emax = field_at_point(r, rc, delta, E(:, :, :, 4), nmax) ! Get point where field is highest
  if (n(3) > size(E, 3)-2) then
    nmax = [1, 1, size(E, 3)-2]
  endif
  if (nmax(1) > size(E, 1)) then ! Flying outside screen
    nmax(1) = size(E, 1) ! A bit dangerous!
    if (outside == 0) outside = 1
  end if
  if (nmax(1) < 1) then ! Flying outside screen
    nmax(1) = 1 ! A bit dangerous!
    if (outside == 0) outside = 1
  end if
  if (nmax(2) > size(E, 2)) then ! Flying outside screen
    nmax(2) = size(E, 2) ! A bit dangerous!
    if(outside == 0) outside = 1
  end if
  if (nmax(2) < 1) then ! Flying outside screen
    nmax(2) = 1 ! A bit dangerous!
    if (outside == 0) outside = 1
  end if
  if (nmax(3) < 1) then
    print *, "ERROR: atom flew down through the bottom"
    stop
    nmax(3) = 1
  end if

  if (outside == 1) then
    print *, 'WARNING: Particle outside field', n(1), n(2), n(3), E(nmax(1), nmax(2), nmax(3), 1:3)
    outside = 2
  end if

  ! Here we temporarily use the a varialbe to hold the electric field, instead of the acceleration!
  a(1) = E(nmax(1), nmax(2), nmax(3), 1) ! Field in V/Å
  a(2) = E(nmax(1), nmax(2), nmax(3), 2)
  a(3) = E(nmax(1), nmax(2), nmax(3), 3)

  if (r(1) == rold(1) .and. r(2) == rold(2) .and. r(3) == rold(3) .and. a(1) == 0 .and. a(2) == 0 .and. a(3) == 0) then
    print *, 'WARNING: Got stuck in integration'
    stop
  endif

  ! Scale units
  ! Some of the steps could be combined, but for clarity they are separately here
  a = a*1d10 ! Field -> V/m
  a = q*a/m ! Now in m/s^2 (All SI)! d2r/dt2 = (q/m)*e_field
  a = a*1d10 ! -> Å/s^2
  a = a*1d-30 ! -> Å/ fs^2
  a = a/box ! ->  boxes/fs^2

  rnew = 2.0d0*r - rold + dt2*a
  vprev = (rnew - rold)/(2.0*dt)
  rold = r
  r = rnew
  i = i+1
  ! Doing integration with small steps is still fast, but outputting everything leads to huge file
  if(mod(i, ndumptrajectory)==0) then
    write(112, "(F0.4, 3F7.1, 3F12.5)") i*dt, r*box, vprev*box
  endif
end do
write(112, "(F0.4, 3F7.1, 3F12.5)") i*dt, r*box, vprev*box ! Always write last
close(112)

! last point if we're printing
call coordinate_to_gridpoint(rnew, n, rc, delta)
print *, "Hit at ", rnew, n

xout = rnew(1)*box(1)
yout = rnew(2)*box(2)
t = i*dt

open (111, file='xytdata.dat', position='append')
write (111, '(F0.3, " ", F0.3, " ", F0.3, " ", I0, " ", F0.4, " ", F0.4, " ", F0.4, " ", F0.5, " ", &
  &F0.5, " ", F0.5, " ", F0.5, " ", F0.5, " ", F0.5)') xout, yout, t, atype, vprev*box, a*box, r0*box
close(111, status='keep')

return
end subroutine SPF_verlet

!> @brief Conver a coordinate in parcas units to a gridpoint
subroutine coordinate_to_gridpoint(r, n, rc, delta)
implicit none
double precision, intent(in) :: r(3) !< Position (parcas units)
double precision, intent(in) :: rc(3) !< Origin of electric field (parcas units)
double precision, intent(in) :: delta(3) !< Grid spacing (parcas units)
integer, intent(out) :: n(3) !< Position on the grid
n = ceiling((r - rc) / delta)
end subroutine

!> @brief Calculate evaporation barrier according to Forbes 1995
!> http://dx.doi.org/10.1016/0169-4332(94)00526-5
function evaporation_barrier(typ, lambda, F, ionization_potential, work_function)
!use TypeParam
implicit none
integer, intent(in) :: typ
double precision, intent(in) :: lambda
double precision, intent(in) :: F
double precision, intent(in) :: ionization_potential(:)
double precision, intent(in) :: work_function(:)

double precision :: evaporation_barrier
double precision :: delta, K, Fev

if(ionization_potential(typ) == 0d0) then
  print *, "Please set the ionization potential for type", typ, "in md.in"
  stop
end if
if(work_function(typ) == 0d0) then
  print *, "Please set the work function for type", typ, "in md.in"
  stop
end if

K = -lambda + ionization_potential(typ) - work_function(typ)
Fev = 0.694*K**2.0*0.1 ! Calculated evaporation field in V/Å
delta = 1-F/Fev

if (delta < 0) then
  evaporation_barrier = 0
else
  evaporation_barrier = (delta**0.5+0.5*(1-delta)*log((1-delta**0.5)/(1+delta**0.5)))*K
end if

if (evaporation_barrier == 0) then
  print *, "WARNING: evaporation_barrier = 0. Something is probably wrong"
  print *, delta, F, Fev, K, lambda, ionization_potential(typ), work_function(typ), typ
end if

return
end function

!> @brief Pick a random atom for evaporation and calculate its trajectory
subroutine evaporate_atom(xq, x0, x1, atype, box, E_field, rcorner, mctemp, myatoms, Fp, Epair, isize, evpatype, zmax, &
  mass, ionization_potential, work_function)
use random
implicit none

double precision, intent(in) :: x0(:) !< Positions of atoms (parcas units)
double precision, intent(in) :: box(3) !< Size of simulation box (Å)
double precision, intent(in) :: E_field(:, :, :, :) !< Electric field (V/Å)
double precision, intent(in) :: rcorner(3) !< Origin of electric field grid in real-space (Å)
double precision, intent(in) :: mctemp !< Monte-carlo temperature for APT simulation (K)
integer, intent(in) :: myatoms !< Number of atoms in system
double precision, intent(in) :: Fp(:, :) !< Potential energy
double precision, intent(in) :: Epair(:) !< More potential energy
integer, intent(in) :: isize(3) !< Size of the atomic system. Used to determine the grid spacing in parcas units
integer, intent(in) :: evpatype !< Atom type to give to evaporated atoms
double precision, intent(in) :: zmax !< Let atoms fly this high up (Å)

double precision, intent(inout) :: xq(:) !< Charge of atoms. Charge of evaporated atom will be put to zero (e)
double precision, intent(inout) :: x1(:) !< Velocity of atoms. Only used to zero velocity of evaporated atoms (parcas units)
integer, intent(inout) :: atype(:) !< Atom types

double precision :: delta(3)
integer nsurface_atoms
integer surface_atoms(size(x0)/3)
double precision picked_pot, picked_barrier, picked_field
integer picked_atom
double precision atom_potential_energy, atom_field
double precision min_barrier, max_field
double precision atom_evaporation_barrier
double precision :: revap
double precision :: rates(0:size(x0)/3)

integer :: i

double precision, parameter :: kBeV = 8.61734215d-5 ! Boltzmann constant, eV
double precision, parameter :: u = 1.66054873d-27 ! Atomic mass unit       

double precision, intent(in) :: mass(:)
double precision, intent(in) :: ionization_potential(:)
double precision, intent(in) :: work_function(:)

delta = 1d0/isize

picked_atom = -1
nsurface_atoms = 0
surface_atoms = -1
rates = 0d0
min_barrier = 1e30
max_field = 0

if(mctemp <= 0) then
  print *, "mctemp should be > 0"
  stop
end if

! Calculate rates and collect some information about max and min values encountered on the way
do i=1, myatoms
  if(atype(i) /= evpatype .and. atype(i) > 0 .and. abs(xq(i*4)) > 1d-30) then ! Surface atom
    nsurface_atoms = nsurface_atoms + 1
    surface_atoms(nsurface_atoms) = i
    atom_potential_energy = Epair(i) + Fp(i, 1)
    atom_field = field_at_point(x0(i*3-2:i*3), rcorner, delta, E_field(:, :, :, 4))
    atom_evaporation_barrier = evaporation_barrier(atype(i), atom_potential_energy, atom_field, ionization_potential, work_function)
    min_barrier = min(min_barrier, atom_evaporation_barrier)
    max_field = max(max_field, atom_field)
    rates(nsurface_atoms) = exp(-atom_evaporation_barrier/(kBeV*mctemp))
  end if
end do

if(sum(rates) == 0d0) then
  print *, "Zero probability for any evaporation to occur. This may be because the temperature (mctemp) or local field is too low."
  print *, "mctemp is ", mctemp, "and the  maximum field is ", max_field, ". The lowest barrier is ", min_barrier
  stop
end if

rates = rates/(1d0*sum(rates)) ! Normalize
! Calculate cumulative sum (aka prefix sum)

do i=1, nsurface_atoms
  rates(i) = rates(i) + rates(i-1)
end do

revap = MyRanf(-1) ! Generate random number [0,1[

do i=1, nsurface_atoms
  if (rates(i-1) <= revap .and. rates(i) > revap) then
    picked_atom = surface_atoms(i)
    print *, "DBG: Picked for evaporation", revap, rates(i-1), rates(i), i, surface_atoms(i)
    exit
  end if
end do

if (picked_atom == -1) then
  print *, revap
  print *, nsurface_atoms
  print *, rates(0:nsurface_atoms)
  print *, rates(nsurface_atoms)
  print *, "Failed to pick atom to evaporate. This should not happen."
  stop
end if

if (abs(xq(picked_atom*4)) <= 1d-30) then
  print *, "Error: picked non-surface atom", picked_atom
  stop
end if

if(atype(picked_atom)<0) then
  print *, "Tried to pick fixed atom", picked_atom, atype(picked_atom)
  stop
end if

picked_pot = Epair(picked_atom) + Fp(picked_atom, 1)
picked_field = field_at_point(x0(picked_atom*3-2:picked_atom*3), rcorner, delta, E_field(:, :, :, 4))
picked_barrier = evaporation_barrier(atype(picked_atom), picked_pot, picked_field, ionization_potential, work_function)

print *, "picked_pot", picked_pot, "picked_field", picked_field, "picked_barrier", picked_barrier, "picked_atom", &
picked_atom, "picked type", atype(picked_atom), "nsurface_atoms", nsurface_atoms
print *, "isize", isize, "rcorner", rcorner

call SPF_verlet(x0(picked_atom*3-2:picked_atom*3), mass(atype(picked_atom))*u, -1.60217646d-19, zmax, E_field, &
  rcorner, 1d0/isize, box, atype(picked_atom))

x1(picked_atom*3-2) = 0.0
x1(picked_atom*3-1) = 0.0
x1(picked_atom*3) = 0.0
xq(picked_atom*4-3) = 0.0
xq(picked_atom*4-2) = 0.0
xq(picked_atom*4-1) = 0.0
xq(picked_atom*4) = 0.0
atype(picked_atom) = evpatype
end subroutine
end module

! Standard Fortran requires spaces instead of tabs
! vim: expandtab:tabstop=2:shiftwidth=2
! kate: indent-width 2; space-indent on; replace-tabs
