If you use libhelmod, please cite the following paper

@article{PhysRevE.83.026704,

  title = {Atomistic modeling of metal surfaces under electric fields: Direct coupling of electric fields to a molecular dynamics algorithm},

  author = {Djurabekova, F. and Parviainen, S. and Pohjonen, A. and Nordlund, K.},

  journal = {Phys. Rev. E},

  volume = {83},

  issue = {2},

  pages = {026704},

  numpages = {11},

  year = {2011},

  month = {Feb},

  publisher = {American Physical Society},

  doi = {10.1103/PhysRevE.83.026704},

  url = {https://link.aps.org/doi/10.1103/PhysRevE.83.026704}

}